package madcat.studio.setting;

import madcat.studio.constant.Constants;
import madcat.studio.dialog.BarThemeListDialog;
import madcat.studio.dialog.FontThemeListDialog;
import madcat.studio.dialog.ModifyBirthDayTypeDialog;
import madcat.studio.dialog.ModifyBirthdayDialog;
import madcat.studio.dialog.ModifyGenderDialog;
import madcat.studio.dialog.ModifyNameDialog;
import madcat.studio.friends.FriendsList;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.Window;
import android.widget.Toast;

public class Settings extends PreferenceActivity {
	private static final String TAG										=	"Settings";
	
	private CheckBoxPreference mModifyCheckBox							=	null;
	private Preference mModifyName										=	null;
	private Preference mModifyGender									=	null;
	private Preference mModifyBirthDayType								=	null;
	private Preference mModifyBirthday									=	null;
	private Preference mModifyFontSize									=	null;
	private Preference mModifyFriends									=	null;
	private Preference mModifyWidgetTheme								=	null;
	private Preference mModifyFontTheme									=	null;
	private Preference mSelectLanguage									=	null;
	
	private String mName;
	private int mGender;
//	private int mBirthDayType;
	private FortuneDate mBirthDay;
	
	private int mFontSize;
	private int mTempFontSize;

//	private int mSelectedYear;
	
	private int mSelectedThemeType;
	private int mTempSelectedThemeType;
	
//	private Spinner mSetting_SpinYear, mSetting_SpinMonth, mSetting_SpinDay;
//	private Button mSetting_btnOk, mSetting_btnCancle;
//	private ArrayAdapter<CharSequence> mAdapterYear, mAdapterMonth, mAdapterDay;
    
    private Context mContext;
    
    private SharedPreferences mUserPref, mConfigPref;
    
    // 언어 설정 관련 변수
    private int mSelectedLanguageIndex, mTempLanguageIndex;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
		
		// 기본 전역 변수 초기화
		mContext = this;
		mModifyCheckBox = (CheckBoxPreference)findPreference("preference_modify_check");
		
		mModifyName = (Preference)findPreference("preference_modify_name");
		mModifyGender = (Preference)findPreference("preference_modify_gender");
		mModifyBirthDayType = (Preference)findPreference("preference_modify_birthday_type");
		mModifyBirthday = (Preference)findPreference("preference_modify_birthday");
		mModifyFontSize = (Preference)findPreference("preference_modify_font_size");
		mModifyFriends = (Preference)findPreference("preference_modify_friends");
		mModifyWidgetTheme = (Preference)findPreference("preference_modify_widget_theme");
		mModifyFontTheme = (Preference)findPreference("preference_modify_font_theme");
		mSelectLanguage = (Preference)findPreference("preference_select_language");

		mUserPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);

		mModifyCheckBox.setChecked(false);
		
		// 사용자 설정 값 로드
		mName = mUserPref.getString(Constants.USER_NAME, "");
		mGender = mUserPref.getInt(Constants.USER_GENDER, 0);
		mModifyName.setSummary(mName);
		
		switch (mGender) {
			case 1:
				mModifyGender.setSummary(getString(R.string.setting_man));
				break;
			case 2:
				mModifyGender.setSummary(getString(R.string.setting_woman));
				break;
		}
		
//		mBirthDayType = mUserPref.getInt(Constants.USER_BIRTH_TYPE, 0);
		
		switch (mUserPref.getInt(Constants.USER_BIRTH_TYPE, 0)) {
		case 1://Solar
			mBirthDay = new FortuneDate(mUserPref.getInt(Constants.USER_SOLAR_YEAR, 0), mUserPref.getInt(Constants.USER_SOLAR_MONTH, 0), mUserPref.getInt(Constants.USER_SOLAR_DAY, 0));
			mModifyBirthDayType.setSummary(getString(R.string.setting_solar));
			mModifyBirthday.setSummary(mBirthDay.getYear() + "/" + mBirthDay.getMonth() + "/" + mBirthDay.getDay());
			break;
		case 2://Lunar
			mBirthDay = new FortuneDate(mUserPref.getInt(Constants.USER_LUNAR_YEAR, 0), mUserPref.getInt(Constants.USER_LUNAR_MONTH, 0), mUserPref.getInt(Constants.USER_LUNAR_DAY, 0));
			mModifyBirthDayType.setSummary(getString(R.string.setting_lunar));
			mModifyBirthday.setSummary(mBirthDay.getYear() + "/" + mBirthDay.getMonth() + "/" + mBirthDay.getDay());
			break;
		}
		
//		mSelectedYear = mBirthDay.getYear();
		mFontSize = mConfigPref.getInt(Constants.CONFIG_FONT_SIZE, 3);
		
		switch (mFontSize) {
			case 1:
				mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_small));
				break;
			case 2:
				mModifyFontSize.setSummary(getString(R.string.setting_summary_font_small));
				break;
			case 3:
				mModifyFontSize.setSummary(getString(R.string.setting_summary_font_normal));
				break;
			case 4:
				mModifyFontSize.setSummary(getString(R.string.setting_summary_font_big));
				break;
			case 5:
				mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_big));
				break;
		}
		
		// 언어 변경 초기 설정
		String currentLanguage = null;
		mSelectedLanguageIndex = mConfigPref.getInt(Constants.CONFIG_SELECT_LANGUAGE, 0);
		mTempLanguageIndex = mSelectedLanguageIndex;
		
		switch(mSelectedLanguageIndex) {
			case 0:	//	Korean
				currentLanguage = getString(R.string.menu_item_language_korean);
				break;
			case 1:	//	English
				currentLanguage = getString(R.string.menu_item_language_english);
				break;
		}
		
		mSelectLanguage.setSummary(getString(R.string.prefix_language) + currentLanguage);
		
		mSelectedThemeType = mConfigPref.getInt(Constants.CONFIG_WIDGET_STYLE_TYPE, 0);
		
		// 이벤트 핸들러 호출
		SettingsEventHandler settingsEventHandler = new SettingsEventHandler();

		mModifyCheckBox.setOnPreferenceClickListener(settingsEventHandler);
		mModifyName.setOnPreferenceClickListener(settingsEventHandler);
		mModifyGender.setOnPreferenceClickListener(settingsEventHandler);
		mModifyBirthDayType.setOnPreferenceClickListener(settingsEventHandler);
		mModifyBirthday.setOnPreferenceClickListener(settingsEventHandler);
		mModifyFontSize.setOnPreferenceClickListener(settingsEventHandler);
		mModifyFriends.setOnPreferenceClickListener(settingsEventHandler);
		mModifyWidgetTheme.setOnPreferenceClickListener(settingsEventHandler);
		mModifyFontTheme.setOnPreferenceClickListener(settingsEventHandler);
		mSelectLanguage.setOnPreferenceClickListener(settingsEventHandler);
	}
	
	@Override
	protected void onStop() {
//		Log.d(TAG, "onStop 호출");
		super.onStop();
		
		mModifyCheckBox.setChecked(false);
		
		mModifyName.setEnabled(false);
		mModifyGender.setEnabled(false);
		mModifyBirthDayType.setEnabled(false);
		mModifyBirthday.setEnabled(false);
		
		mModifyName.setSelectable(false);
		mModifyGender.setSelectable(false);
		mModifyBirthDayType.setSelectable(false);
		mModifyBirthday.setSelectable(false);
	}
	
	private class SettingsEventHandler implements OnPreferenceClickListener {
		public boolean onPreferenceClick(Preference preference) {
			if (preference.getKey().equals("preference_modify_check")) {
				if (mModifyCheckBox.isChecked()) {
					mModifyName.setEnabled(true);
					mModifyGender.setEnabled(true);
					mModifyBirthDayType.setEnabled(true);
					mModifyBirthday.setEnabled(true);
					
					mModifyName.setSelectable(true);
					mModifyGender.setSelectable(true);
					mModifyBirthDayType.setSelectable(true);
					mModifyBirthday.setSelectable(true);
				} else {
					mModifyName.setEnabled(false);
					mModifyGender.setEnabled(false);
					mModifyBirthDayType.setEnabled(false);
					mModifyBirthday.setEnabled(false);
					
					mModifyName.setSelectable(false);
					mModifyGender.setSelectable(false);
					mModifyBirthDayType.setSelectable(false);
					mModifyBirthday.setSelectable(false);
				}
				return true;
			} else if (preference.getKey().equals("preference_modify_name")) {
				final ModifyNameDialog modifyNameDialog = new ModifyNameDialog(mContext);
				modifyNameDialog.show();
				
				modifyNameDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyNameDialog.getDialogFlag()) {
							mModifyName.setSummary(mUserPref.getString(Constants.USER_NAME, ""));
						}
					}
				});
				
				return true;
			} else if (preference.getKey().equals("preference_modify_gender")) {
				final ModifyGenderDialog modifyGenderDialog = new ModifyGenderDialog(mContext);
				modifyGenderDialog.show();
				
				modifyGenderDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyGenderDialog.getDialogFlag()) {
							switch(mUserPref.getInt(Constants.USER_GENDER, ModifyGenderDialog.RADIO_MAN_SELECT)) {
							case 1:
								mModifyGender.setSummary(getString(R.string.setting_man));
								break;
							case 2:
								mModifyGender.setSummary(getString(R.string.setting_woman));
								break;
							default:
								break;
							}
						}
					}
				});
				
				return true;
			} else if (preference.getKey().equals("preference_modify_birthday_type")) {
				final ModifyBirthDayTypeDialog modifyBirthDayTypeDialog = new ModifyBirthDayTypeDialog(mContext);
				modifyBirthDayTypeDialog.show();
				
				modifyBirthDayTypeDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyBirthDayTypeDialog.getDialogFlag()) {
							FortuneDate solarDate = null;
							FortuneDate lunarDate = null;
							
							switch(modifyBirthDayTypeDialog.getModifyFlag()) {
								case ModifyBirthDayTypeDialog.RADIO_SOLAR_CHECK:				//	음력에서 양력으로 변환
									mModifyBirthDayType.setSummary(getString(R.string.setting_solar));
									
									lunarDate = mBirthDay;
									mBirthDay = Util.getSolarDate(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
									solarDate = mBirthDay;
									
									Util.setUserBirthDay(mContext, solarDate, lunarDate);
									mModifyBirthday.setSummary(mBirthDay.getYear() + "/" + mBirthDay.getMonth() + "/" + mBirthDay.getDay());
									
									break;
								case ModifyBirthDayTypeDialog.RADIO_LUNAR_CHECK:				//	양력에서 음력으로 변환
									mModifyBirthDayType.setSummary(getString(R.string.setting_lunar));
									
									solarDate = mBirthDay;
									mBirthDay = Util.getLunarDate(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay());
									lunarDate = mBirthDay;
									
									Util.setUserBirthDay(mContext, solarDate, lunarDate);
									mModifyBirthday.setSummary(mBirthDay.getYear() + "/" + mBirthDay.getMonth() + "/" + mBirthDay.getDay());
									
									break;
								default:
									break;
							}
						}
					}
				});
				
				return true;
			} else if (preference.getKey().equals("preference_modify_birthday")) {
				final ModifyBirthdayDialog modifyBirthDayDialog = new ModifyBirthdayDialog(mContext, mBirthDay);
				modifyBirthDayDialog.show();
				
				modifyBirthDayDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(modifyBirthDayDialog.getDialogFlag()) {
							FortuneDate birthDay = modifyBirthDayDialog.getBirthDay();
							mModifyBirthday.setSummary(birthDay.getYear() + "/" + birthDay.getMonth() + "/" + birthDay.getDay());
						}
					}
				});
				
				return true;
			} else if (preference.getKey().equals("preference_modify_font_size")) {
				showModifyFontDialog();
				return true;
			} else if (preference.getKey().equals("preference_modify_friends")) {
				Intent intent = new Intent(Settings.this, FriendsList.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				return true;
			} else if (preference.getKey().equals("preference_modify_widget_theme")) {
				showModifyThemeDialog();
			} else if (preference.getKey().equals("preference_modify_font_theme")) {
				FontThemeListDialog fontThemeListDialog = new FontThemeListDialog(mContext);
				fontThemeListDialog.show();
			} else if (preference.getKey().equals("preference_select_language")) {
				showSelectLanguageDialog();
			}
			
			return false;
		}
	}
	
	private void showSelectLanguageDialog() {
		final String[] items = {getString(R.string.menu_item_language_korean), getString(R.string.menu_item_language_english)};
		
		AlertDialog.Builder builder = new Builder(mContext);
		builder.setTitle(getString(R.string.dialog_title_select_language));
		builder.setSingleChoiceItems(items, mSelectedLanguageIndex, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int index) {
				mTempLanguageIndex = index;
			}
		}).setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				if(mSelectedLanguageIndex != mTempLanguageIndex) {
					// 어플을 재시작하라고 알림
					Toast.makeText(mContext, getString(R.string.toast_notice_changed_language), Toast.LENGTH_SHORT).show();
					
					mSelectedLanguageIndex = mTempLanguageIndex;
					
					// 언어 Summary 설정
					mSelectLanguage.setSummary(getString(R.string.prefix_language) + items[mSelectedLanguageIndex]);
					
					// 환경 설정 값에 현재 설정된 언어 인덱스를 저장
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putInt(Constants.CONFIG_SELECT_LANGUAGE, mSelectedLanguageIndex);
					editor.commit();
					
					// 어플리케이션 언어 설정을 변경
					String changeLanguage = null;
					switch(mSelectedLanguageIndex) {
						case 0:
							changeLanguage = "ko";
							break;
						case 1:
							changeLanguage = "en";
							break;
					}
					
					Util.setLocale(mContext, changeLanguage);
					
					// 위젯을 업데이트하여 언어를 변경
					Util.sendBroadCasting(mContext, Constants.ACTION_CHANGE_LANGUAGE);
				}
			}
		}).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				
			}
		});
		
		builder.show();
	}
	
	private void showModifyThemeDialog() {
		int tempThemeIndex = mConfigPref.getInt(Constants.CONFIG_WIDGET_BAR_THEME_ITEM, 0);
		
		final BarThemeListDialog barThemeListDialog = new BarThemeListDialog(mContext);
		barThemeListDialog.show();
	}
	
	private void showModifyFontDialog() {
		final SharedPreferences pref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);

		mTempFontSize = mFontSize;
		
		final String items[] = {getString(R.string.setting_font_very_small), getString(R.string.setting_font_small), 
				getString(R.string.setting_font_normal), getString(R.string.setting_font_big), getString(R.string.setting_font_very_big)};
		AlertDialog.Builder ab = new AlertDialog.Builder(Settings.this);
		ab.setTitle(getString(R.string.setting_choice_font));
		ab.setSingleChoiceItems(items, (mFontSize-1), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				switch(whichButton) {
					case 0:
						mTempFontSize = 1;
						mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_small));
						break;
					case 1:
						mTempFontSize = 2;
						mModifyFontSize.setSummary(getString(R.string.setting_summary_font_small));
						break;
					case 2:
						mTempFontSize = 3;
						mModifyFontSize.setSummary(getString(R.string.setting_summary_font_normal));
						break;
					case 3:
						mTempFontSize = 4;
						mModifyFontSize.setSummary(getString(R.string.setting_summary_font_big));
						break;
					case 4:
						mTempFontSize = 5;
						mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_big));
						break;
					default:
						Toast.makeText(mContext, getString(R.string.toast_caution_not_modify_font), Toast.LENGTH_SHORT).show();
						break;
				}
			}
		}).setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				mFontSize = mTempFontSize;
				
				SharedPreferences.Editor prefEditor = pref.edit();
				prefEditor.putInt(Constants.CONFIG_FONT_SIZE, mFontSize);
				prefEditor.commit();
			}
		}).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				mTempFontSize = mFontSize;
				switch(mTempFontSize) {
				case 1:
					mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_small));
					break;
				case 2:
					mModifyFontSize.setSummary(getString(R.string.setting_summary_font_small));
					break;
				case 3:
					mModifyFontSize.setSummary(getString(R.string.setting_summary_font_normal));
					break;
				case 4:
					mModifyFontSize.setSummary(getString(R.string.setting_summary_font_big));
					break;
				case 5:
					mModifyFontSize.setSummary(getString(R.string.setting_summary_font_very_big));
					break;
				default:
					break;
				}
			}
		});
		
		ab.show();
		
	}
	 
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}