package madcat.studio.utils;

public class FortuneAllDate {
	private int mSolarYear, mSolarMonth, mSolarDay, mLunarYear, mLunarMonth, mLunarDay;

	public int getSolarYear() {
		return mSolarYear;
	}

	public void setSolarYear(int solarYear) {
		this.mSolarYear = solarYear;
	}

	public int getSolarMonth() {
		return mSolarMonth;
	}

	public void setSolarMonth(int solarMonth) {
		this.mSolarMonth = solarMonth;
	}

	public int getSolarDay() {
		return mSolarDay;
	}

	public void setSolarDay(int solarDay) {
		this.mSolarDay = solarDay;
	}

	public int getLunarYear() {
		return mLunarYear;
	}

	public void setLunarYear(int lunarYear) {
		this.mLunarYear = lunarYear;
	}

	public int getLunarMonth() {
		return mLunarMonth;
	}

	public void setLunarMonth(int lunarMonth) {
		this.mLunarMonth = lunarMonth;
	}

	public int getLunarDay() {
		return mLunarDay;
	}

	public void setLunarDay(int lunarDay) {
		this.mLunarDay = lunarDay;
	}
}
