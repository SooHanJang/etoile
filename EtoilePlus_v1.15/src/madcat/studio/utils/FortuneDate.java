package madcat.studio.utils;

public class FortuneDate {
	private int mYear = 0;
	private int mMonth = 0;
	private int mDay = 0;
	
	public FortuneDate() {
		// TODO Auto-generated constructor stub
		mYear = mMonth = mDay = 0;
	}
	
	public FortuneDate(int year, int month, int day) {
		// TODO Auto-generated constructor stub
		mYear = year;
		mMonth = month;
		mDay = day;
	}
	
	public void setYear(int year) { mYear = year; }
	public void setMonth(int month) { mMonth = month; }
	public void setDay(int day) { mDay = day; }
	
	public int getYear() { return mYear; }
	public int getMonth() { return mMonth; }
	public int getDay() { return mDay; }
}
