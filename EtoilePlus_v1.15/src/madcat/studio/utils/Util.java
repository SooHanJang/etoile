package madcat.studio.utils;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import madcat.studio.constant.Constants;
import madcat.studio.friends.Friends;
import madcat.studio.plus.R;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Dane
 * !!DO NOT CHANGE THIS FILE!!
 */
public class Util {
//	private static final String TAG = "UTIL";
	
	private static final int PERIOD = 27;
	/**
	 * 입력창에 입력된 정보들을 받아, 유효성 검사를 수행함.
	 * 유효성 검사 알고리즘에 따라 결과 코드를 반환한다.
	 * @param name
	 * @param year
	 * @param month
	 * @param day
	 * @return resultCode
	 */
	public static int isCorrect(String name, String year, String month, String day) {
		GregorianCalendar gregori = new GregorianCalendar();
		int temp_year, temp_month, temp_day;
		
		temp_year = temp_month = temp_day = 0;
		
		if(name.length() == 0) {		// 1 : 이름 미입력 시 에러
			return 1;
		} 
		
		if(year.length() == 0) {		// 2 : 년도 미입력 시 에러
			return 2;
		} else {
			temp_year = Integer.parseInt(year);		// 3 : 년도 범위 벗어났을 시 에러
			if(temp_year < 1900 || temp_year > 2100) {
				return 3;
			}
		}
		
		if(month.length() == 0) {		// 4 : 태어난 달 미입력 에러
			return 4;
		} else if(day.length() == 0) {	// 5: 태어난 일 미입력 에러
			return 5;
		} else {
			temp_month = Integer.parseInt(month);
			temp_day = Integer.parseInt(day);
			
			if (temp_month >= 1 && temp_month <= 12) {
				if(temp_month==2) {
					if(gregori.isLeapYear(temp_year)) {	// 7: 태어난 일 범위 초과 에러
						if(temp_day < 1 || temp_day > 29) {
							return 7;
						}
					} else {		
						if(temp_day < 1 || temp_day > 28) {
							return 7;
						}
					}
				} else if(temp_month==1 || temp_month==3 || temp_month==5 || temp_month==7 || temp_month==8 || temp_month==10 || temp_month==12) {
					if(temp_day < 1 || temp_day > 31) {
						return 7;
					}
				} else {
					if(temp_day < 1 || temp_day > 30) {
						return 7;
					}
				}
			} else {
				return 6;	// 6: 태어난 달 범위 초과 에러
			}
		}
		
		return 0;
	}
	
	/**
	 * year, month,day에 해당하는 양력 날짜를 반환한다.
	 * @param year
	 * @param month
	 * @param day
	 * @return solarDate
	 */
	public static FortuneDate getSolarDate(int year, int month, int day) {
		FortuneDate solarDate = null;
		
//		Log.d(TAG, "DB 접근, 양력 변환");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
//		Log.d(TAG, year + "/" + month + "/" + day);
		
		String matchLunarDate = "select * from " + Constants.TABLE_CALENDAR + 
							   " where " + Constants.ATTRIBUTE_LUNAR_YEAR + "=" + year +
							   " and " + Constants.ATTRIBUTE_LUNAR_MONTH + "=" + month +
							   " and " + Constants.ATTRIBUTE_LUNAR_DAY + "=" + day;
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);
		cursor.moveToFirst();
		
		solarDate = new FortuneDate(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
		
		cursor.close();
		sdb.close();
		
//		Log.d(TAG, solarDate.getYear() + "/" + solarDate.getMonth() + "/" + solarDate.getDay());
		
		return solarDate;
	}
	
	/**
	 * 해당 년도와 달을 중심으로, -1, +1 달에 해당하는 음력일을 전부 가져온다.
	 * 
	 * @param context
	 * @param year
	 * @param month
	 * @return
	 */
	
	public static ArrayList<FortuneAllDate> getRangeAllDate(Context context, int year, int month) {
		ArrayList<FortuneAllDate> rangeAllDateOrders = new ArrayList<FortuneAllDate>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		int prevYear, nextYear;
		int prevMonth, nextMonth;
		
		if(month == 1) {
			prevYear = year - 1;
			prevMonth = 12;
		} else {
			prevYear = year;
			prevMonth = month - 1;
		}
		
		if(month == 12) {
			nextYear = year + 1;
			nextMonth = 1;
		} else {
			nextYear = year;
			nextMonth = month + 1;
		}
		
		String matchRangeMonthDate = "SELECT * FROM " + Constants.TABLE_CALENDAR +
									" WHERE (" + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + prevYear +
									" AND " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + prevMonth + ")" +
									" OR (" + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + year +
									" AND " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + month + ")" +
									" OR (" + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + nextYear + 
									" AND " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + nextMonth + ");";
		
		Cursor cursor = sdb.rawQuery(matchRangeMonthDate, null);
		cursor.getCount();
		
		while(cursor.moveToNext()) {
			FortuneAllDate allDate = new FortuneAllDate();
			allDate.setSolarYear(cursor.getInt(1));
			allDate.setSolarMonth(cursor.getInt(2));
			allDate.setSolarDay(cursor.getInt(3));
			allDate.setLunarYear(cursor.getInt(4));
			allDate.setLunarMonth(cursor.getInt(5));
			allDate.setLunarDay(cursor.getInt(6));
			
			rangeAllDateOrders.add(allDate);
		}
		
		cursor.close();
		sdb.close();

		return rangeAllDateOrders;
	}
	
	/**
	 * year, month 에 해당하는 음력 날짜를 전부 반환한다.
	 * 
	 * @param year
	 * @param month
	 * @return
	 */
	
	public static ArrayList<FortuneDate> getLunarMonthlyDate(Context context, int year, int month) {
		ArrayList<FortuneDate> lunarDateOrders = new ArrayList<FortuneDate>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String matchLunarMonthDate = "SELECT * FROM " + Constants.TABLE_CALENDAR +
									" WHERE " + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + year +
									" AND " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + month;
		
		Cursor cursor = sdb.rawQuery(matchLunarMonthDate, null);
		cursor.getCount();
		
		while(cursor.moveToNext()) {
			FortuneDate lunarDate = new FortuneDate(cursor.getInt(4), cursor.getInt(5), cursor.getInt(6));
			lunarDateOrders.add(lunarDate);
		}
		
		cursor.close();
		sdb.close();

		return lunarDateOrders;
	}
	
	
	/**
	 * 년, 월에 해당되는 운세 타입을 String[] 형태로 반환한다.
	 * 
	 * @param context
	 * @param year
	 * @param month
	 * @return
	 */
	public static String[] getMonthlyFortuneType(Context context, int year, int month) {
		ArrayList<FortuneDate> lunarDateOrders = Util.getLunarMonthlyDate(context, year, month);
		SharedPreferences pref = context.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		String[] fortuneType = new String[lunarDateOrders.size()];
		
		for(int i=0; i < lunarDateOrders.size(); i++) {
			fortuneType[i] = Util.getEastTodayFortune(pref.getInt(Constants.USER_EAST_STAR_POSITION, -1), 
	    			Util.getEastStarPosition(lunarDateOrders.get(i).getMonth(), lunarDateOrders.get(i).getDay()));
		}
		
		return fortuneType;
	}
	
	/**
	 * year, month, day에 해당하는 음력 날짜를 반환한다.
	 * @param year
	 * @param month
	 * @param day
	 * @return lunarDate
	 */
	public static FortuneDate getLunarDate(int year, int month, int day) {
		FortuneDate lunarDate = null;
		
//		Log.d(TAG, "DB 접근, 음력 변환.");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
//		Log.d(TAG, year + "/" + month + "/" + day);
		
		String matchLunarDate = "select * from " + Constants.TABLE_CALENDAR + 
							   " where " + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + year +
							   " and " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + month +
							   " and " + Constants.ATTRIBUTE_SOLAR_DAY + "=" + day;
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);
		cursor.moveToFirst();
		
		lunarDate = new FortuneDate(cursor.getInt(4), cursor.getInt(5), cursor.getInt(6));
		
		cursor.close();
		sdb.close();
		
//		Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
		
		return lunarDate;
	}
	
	public static final int TODAY = 0;
	public static final int TOMORROW = 1;
	
	/**
	 * TYPE에 해당되는 음력 날짜를 반환한다.
	 * @param TYPE : TODAY or TOMORROW
	 * @return lunarDate
	 */
	public static FortuneDate getLunarDate(int TYPE) {
		FortuneDate lunarDate = null;
		
//		Log.d(TAG, "DB 접근, 음력 변환.");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		Calendar calendar = null;
		
		switch (TYPE) {
		case TODAY:
			calendar = Calendar.getInstance();
			break;
		case TOMORROW:
			calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 1);
			break;
		default:
			calendar = Calendar.getInstance();
		}
		
//		Log.d(TAG, calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH));
		
		String matchLunarDate = "select * from " + Constants.TABLE_CALENDAR + 
							   " where " + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + calendar.get(Calendar.YEAR) +
							   " and " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + (calendar.get(Calendar.MONTH) + 1) +
							   " and " + Constants.ATTRIBUTE_SOLAR_DAY + "=" + calendar.get(Calendar.DAY_OF_MONTH);
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);
		cursor.moveToFirst();
		
		lunarDate = new FortuneDate(cursor.getInt(4), cursor.getInt(5), cursor.getInt(6));
		
		cursor.close();
		sdb.close();
		
//		Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
		
		return lunarDate;
	}
	
	/**
	 * 월, 일을 입력받아 서양식 별자리를 판단하여 반환한다.
	 * @param month
	 * @param day
	 * @return westStarPosition
	 */
	public static int getWestStarPosition(int month, int day) {
    	if(((month == 12) && (day >= 22)) || ((month == 1) && (day <= 19))) {
    		return 0;		// 염소자리
    	} else if(((month == 1) && (day >= 20)) || ((month == 2) && (day <= 18))) {
    		return 1;		// 물병자리
    	} else if(((month == 2) && (day >= 19)) || ((month == 3) && (day <= 20))) {
    		return 2;		// 물고기자리
    	} else if(((month == 3) && (day >= 21)) || ((month == 4) && (day <= 20))) {
    		return 3;		// 양자리
    	} else if(((month == 4) && (day >= 21)) || ((month == 5) && (day <= 20))) {
    		return 4;		// 황소자리
    	} else if(((month == 5) && (day >= 21)) || ((month == 6) && (day <= 21))) {
    		return 5;		// 쌍둥이자리
    	} else if(((month == 6) && (day >= 22)) || ((month == 7) && (day <= 22))) {
    		return 6;		// 게자리
    	} else if(((month == 7) && (day >= 23)) || ((month == 8) && (day <= 22))) {
    		return 7;		// 사자자리
    	} else if(((month == 8) && (day >= 23)) || ((month == 9) && (day <= 22))) {
    		return 8;		// 처녀자리
    	} else if(((month == 9) && (day >= 23)) || ((month == 10) && (day <= 21))) {
    		return 9;		// 천칭자리
    	} else if(((month == 10) && (day >= 22)) || ((month == 11) && (day <= 21))) {
    		return 10;		// 전갈자리
    	} else if(((month == 11) && (day >= 22)) || ((month == 12) && (day <= 21))) {
    		return 11;		// 사수자리
    	} else {
    		return -1;
    	}
    }
	
	/**
	 * 음력 월, 일을 입력받아 동양식 별자리를 판단하여 반환한다.
	 * @param month
	 * @param day
	 * @return eastStarPosition
	 */
	public static int getEastStarPosition(int month, int day) {
		int eastStarPosistion = 0;
		
		switch (month) {
		case 7:
			day = day + 1;
			break;
		case 8:
		case 9:
		case 10:
			day = day + 2;
			break;
		case 11:
		case 12:
			day = day + 3;
			break;
		default:
			break;
		}
		
		eastStarPosistion = ((month - 1) * 2 + day) % PERIOD;
		
		if (eastStarPosistion != 0) {
			return eastStarPosistion;
		} else {
			return PERIOD;
		}
	}
	
	/**
	 * 동양식 별자리를 Integer에서 String으로 변환한다.
	 * @param eastStarPosition (integer)
	 * @return eastStarPosition (String)
	 */
	public static String toStringEastStarPosition(int eastStarPosition) {
		if (eastStarPosition > 0 && eastStarPosition < 28) {
			switch (eastStarPosition) {
			case 1:
				return "실";
			case 2:
				return "벽";
			case 3:
				return "규";
			case 4:
				return "루";
			case 5:
				return "위";
			case 6:
				return "묘";
			case 7:
				return "필";
			case 8:
				return "자";
			case 9:
				return "참";
			case 10:
				return "정";
			case 11:
				return "귀";
			case 12:
				return "류";
			case 13:
				return "성";
			case 14:
				return "장";
			case 15:
				return "익";
			case 16:
				return "진";
			case 17:
				return "각";
			case 18:
				return "항";
			case 19:
				return "저";
			case 20:
				return "방";
			case 21:
				return "심";
			case 22:
				return "미";
			case 23:
				return "기";
			case 24:
				return "두";
			case 25:
				return "여";
			case 26:
				return "허";
			case 27:
				return "윈";
			default:
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * 동양식 별자리를 String에서 Integer로 변환한다.
	 * @param eastStarPosition (String)
	 * @return eastStarPosition (Integer)
	 */
	public static int toIntegerEastStarPosition(String eastStarPosition) {
		if (eastStarPosition.equals("실")) {
			return 1;
		} else if (eastStarPosition.equals("벽")) {
			return 2;
		} else if (eastStarPosition.equals("규")) {
			return 3;
		} else if (eastStarPosition.equals("루")) {
			return 4;
		} else if (eastStarPosition.equals("위")) {
			return 5;
		} else if (eastStarPosition.equals("묘")) {
			return 6;
		} else if (eastStarPosition.equals("필")) {
			return 7;
		} else if (eastStarPosition.equals("자")) {
			return 8;
		} else if (eastStarPosition.equals("참")) {
			return 9;
		} else if (eastStarPosition.equals("정")) {
			return 10;
		} else if (eastStarPosition.equals("귀")) {
			return 11;
		} else if (eastStarPosition.equals("류")) {
			return 12;
		} else if (eastStarPosition.equals("성")) {
			return 13;
		} else if (eastStarPosition.equals("장")) {
			return 14;
		} else if (eastStarPosition.equals("익")) {
			return 15;
		} else if (eastStarPosition.equals("진")) {
			return 16;
		} else if (eastStarPosition.equals("각")) {
			return 17;
		} else if (eastStarPosition.equals("항")) {
			return 18;
		} else if (eastStarPosition.equals("저")) {
			return 19;
		} else if (eastStarPosition.equals("방")) {
			return 20;
		} else if (eastStarPosition.equals("심")) {
			return 21;
		} else if (eastStarPosition.equals("미")) {
			return 22;
		} else if (eastStarPosition.equals("기")) {
			return 23;
		} else if (eastStarPosition.equals("두")) {
			return 24;
		} else if (eastStarPosition.equals("여")) {
			return 25;
		} else if (eastStarPosition.equals("허")) {
			return 26;
		} else if (eastStarPosition.equals("윈")) {
			return 27;
		} else {
			return -1;
		}
	}
	
	/**
	 * 두 개의 동양식 별자리를 입력받아 동양식 상성을 계산하여 반환한다.
	 * @param iStar [자신의 별자리]
	 * @param uStar [상대방의 별자리]
	 * @return eastStarHarmony
	 */
	public static String getEastStarHarmony(int iStar, int uStar) {
		int harmony = iStar - uStar;
		
		if (harmony < 0)
			harmony = harmony + PERIOD;
		
		switch (harmony % PERIOD) {
		case 0:
			return "동";
		case 9:
			return "과";
		case 18:
			return "인";
		case 1:
		case 10:
		case 19:
			return "친";
		case 2:
		case 11:
		case 20:
			return "양";
		case 3:
		case 12:
		case 21:
			return "멸";
		case 4:
		case 13:
		case 22:
			return "성";
		case 5:
		case 14:
		case 23:
			return "험";
		case 6:
		case 15:
		case 24:
			return "안";
		case 7:
		case 16:
		case 25:
			return "쇠";
		case 8:
		case 17:
		case 26:
			return "복";
		default:
			return null;
		}
	}
	
	/**
	 * 자신과 오늘의 별자리를 입력받아 동양식 오늘의 운세를 계산하여 반환한다.
	 * @param iStar_string [자신의 별자리]
	 * @param dayStar_string [오늘의 별자리]
	 * @return eastTodayFortune
	 */
	public static String getEastTodayFortune(int iStar, int dayStar) {
		int temp = iStar - dayStar;
		
		if (temp < 0)
			temp = temp + PERIOD;
		
		switch (temp % PERIOD) {
		case 0:
			return "자신";
		case 1:
			return "동인";
		case 2:
			return "양광";
		case 3:
			return "소멸";
		case 4:
			return "성공";
		case 5:
			return "위험";
		case 6:
			return "안주";
		case 7:
			return "음약";
		case 8:
			return "다복";
		case 9:
			return "인연";
		case 10:
			return "친구";
		case 11:
			return "양명";
		case 12:
			return "멸망";
		case 13:
			return "성취";
		case 14:
			return "위태";
		case 15:
			return "안정";
		case 16:
			return "쇠약";
		case 17:
			return "행복";
		case 18:
			return "응보";
		case 19:
			return "상서";
		case 20:
			return "원양";
		case 21:
			return "자멸";
		case 22:
			return "입신";
		case 23:
			return "위급";
		case 24:
			return "편안";
		case 25:
			return "나약";
		case 26:
			return "복덕";
		default:
			return null;
		}
	}
	
	/**
	 * TextView 의 설정된 Width 를 기준으로 \n 을 추가하여 글자 길이를 TextView 의 Width 에 맞춘다.
	 * @param text_view
	 * @param text_view_width
	 * @param content
	 */
	
	public static void appendToTextViewByNonWordBreak(TextView text_view, int text_view_width, String content) {
		String split_text = content;
		float textviewwidth = text_view_width;
		Paint paint = text_view.getPaint();
		int break_idx = 0;
		
		while(split_text.length() > 0) {
			break_idx = paint.breakText(split_text, true, textviewwidth, null);
			
			if(split_text.length() <= break_idx) {
				text_view.append(split_text);
				break;
			}
			
			text_view.append(split_text.substring(0, break_idx));
			split_text = split_text.substring(break_idx);

			text_view.append("\n");
		}
	}
	
	/**
	 * 부모 View를 기준으로 자식 View를 재귀 호출하면서 할당된 이미지를 해제한다.
	 * @param root
	 */
	
	public static void recursiveRecycle(View root) {
		if(root == null) {
			return;
		}
		
		if(root instanceof ViewGroup) {
			ViewGroup group = (ViewGroup)root;
			int count = group.getChildCount();
			
			for(int i=0; i < count; i++) {
				recursiveRecycle(group.getChildAt(i));
			}
			
			if(!(root instanceof AdapterView)) {
				group.removeAllViews();
			}
		}
		
		if(root instanceof ImageView) {
			((ImageView)root).setImageDrawable(null);
		}
		
		root.setBackgroundDrawable(null);
		
		root = null;
		
		return;
	}
	
	/**
	 * Friends Table 에 저장된 친구 정보를 조회한 뒤 결과 값을 ArrayList<Friends>로 반환한다.
	 * @return friendsOrders;
	 */
	
	public static ArrayList<Friends> getFriendsList() {
		Friends friends;
		ArrayList<Friends> friendsOrders = new ArrayList<Friends>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String matchLunarDate = "select * from " + Constants.TABLE_FRIEND;
		
		Cursor cursor = sdb.rawQuery(matchLunarDate, null);

		while(cursor.moveToNext()) {
			friends = new Friends(cursor.getString(1), cursor.getInt(2), cursor.getInt(3),cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), 
					cursor.getInt(7), cursor.getInt(8), cursor.getInt(9), ""); 
			friendsOrders.add(friends);
		}
		
		cursor.close();
		sdb.close();
		
//		Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
		
		return friendsOrders;
	}
	
	/**
	 * Friends Table에 해당하는 사용자 정보를 Insert 한다.
	 * @param name	[이름]
	 * @param gender	[성별]
	 * @param birthType	[양력,음력]
	 * @param sy	[양력 년]
	 * @param sm	[양력 월]
	 * @param sd	[양력 일]
	 * @param ly	[음력 년]
	 * @param lm	[음력 월]
	 * @param ld	[음력 일]
	 */
	
	public static void insertDbFriends(String name, int gender, int birthType, int sy, int sm, int sd, int ly, int lm, int ld) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		
		values.put(Constants.ATTRIBUTE_FRIEND_NAME, name);
		values.put(Constants.ATTRIBUTE_FRIEND_GENDER, gender);
		values.put(Constants.ATTRIBUTE_FRIEND_BIRTH_TYPE, birthType);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_YEAR, sy);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_MONTH, sm);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_DAY, sd);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_YEAR, ly);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_MONTH, lm);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_DAY, ld);
		
		sdb.insert(Constants.TABLE_FRIEND, null, values);
		sdb.close();
	}
	
	public static void deleteDbFriends(String name, int gender, int birthType, int sy, int sm, int sd) {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		sdb.delete(Constants.TABLE_FRIEND, Constants.ATTRIBUTE_FRIEND_NAME + "='" + name + "' and " +
										   Constants.ATTRIBUTE_FRIEND_GENDER + "=" + gender + " and " +
										   Constants.ATTRIBUTE_FRIEND_BIRTH_TYPE + "=" + birthType + " and " +
										   Constants.ATTRIBUTE_FRIEND_SOLAR_YEAR + "=" + sy + " and " +
										   Constants.ATTRIBUTE_FRIEND_SOLAR_MONTH + "=" + sm + " and " +
										   Constants.ATTRIBUTE_FRIEND_SOLAR_DAY + "=" + sd, null);
		
		sdb.close();
		
	}
	
	public static boolean emptyLunarDateCheck(int year, int month, int day) {
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String emptyLunarDateCheck = "select * from " + Constants.TABLE_CALENDAR + 
			   " where " + Constants.ATTRIBUTE_LUNAR_YEAR + "=" + year +
			   " and " + Constants.ATTRIBUTE_LUNAR_MONTH + "=" + month +
			   " and " + Constants.ATTRIBUTE_LUNAR_DAY + "=" + day;

		Cursor cursor = sdb.rawQuery(emptyLunarDateCheck, null);
		cursor.moveToFirst();
		
		if(cursor.getCount() != 0) {		// DB 에 데이터가 있다면 true
			
			cursor.close();
			sdb.close();
			return true;
		} 
		
		cursor.close();
		sdb.close();
		return false;
		
	}
	
	public static void updateDbFriends(Friends friend, String name, int gender, int birthType, int sy, int sm, int sd, int ly, int lm, int ld) {
//		Log.d(TAG, "updateDbFriends 가 호출");
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		ContentValues values = new ContentValues();
		
		values.put(Constants.ATTRIBUTE_FRIEND_NAME, name);
		values.put(Constants.ATTRIBUTE_FRIEND_GENDER, gender);
		values.put(Constants.ATTRIBUTE_FRIEND_BIRTH_TYPE, birthType);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_YEAR, sy);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_MONTH, sm);
		values.put(Constants.ATTRIBUTE_FRIEND_SOLAR_DAY, sd);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_YEAR, ly);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_MONTH, lm);
		values.put(Constants.ATTRIBUTE_FRIEND_LUNAR_DAY, ld);
		
		sdb.update(Constants.TABLE_FRIEND, values, Constants.ATTRIBUTE_FRIEND_NAME + "='" + friend.getName() + "' and " +
				   								   Constants.ATTRIBUTE_FRIEND_GENDER + "=" + friend.getGender() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_BIRTH_TYPE + "=" + friend.getBirthType() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_SOLAR_YEAR + "=" + friend.getSolarYear() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_SOLAR_MONTH + "=" + friend.getSolarMonth() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_SOLAR_DAY + "=" + friend.getSolarDay() + " and " + 
				   								   Constants.ATTRIBUTE_FRIEND_LUNAR_YEAR + "=" + friend.getLunarYear() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_LUNAR_MONTH + "=" + friend.getLunarMonth() + " and " +
				   								   Constants.ATTRIBUTE_FRIEND_LUNAR_DAY + "=" + friend.getLunarDay(), null);
		
		sdb.close();
	}
	
	public static void getVibratorService(Context context, long millSeconds) {
		Vibrator vibe = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		vibe.vibrate(millSeconds);
	}
	
//	public static Cursor getURI(Context context, String name) {
//    	Uri people = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
//    	
//    	String[] projection = new String[] { ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER };
//    	String[] params = new String[] {name};
//    	
//    	return context.getContentResolver().query(people, projection, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + "=?", params, null);
//    }
	
	public static boolean isWifiAvailable(Context context) {
		ConnectivityManager manager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connect = false;
		
		try {
			if(manager == null) {
				return false;
			}
			
			NetworkInfo info = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			connect = (info.isAvailable() && info.isConnected());
		} catch(Exception e) {
			return false;
		}
		
		return connect;
	}
	
	public static boolean is3GAvailable(Context context) {
		ConnectivityManager manager= (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		boolean connect = false;
		try {
			if( manager == null ) {
				return false;
			}
			NetworkInfo info = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			connect = (info.isAvailable() && info.isConnected());
		} catch(Exception e) {
			return false;
		}

		return connect;
	}
	
	//아래의 메소드로 Activity 와 drawable의 id를 입력받아 InputStream처리후 drawable를 리턴해서
	//(class명은 backgroundMgr이라 가정)
	 public static BitmapDrawable getDrawable(Activity activity, int resid) {
		  
		 InputStream is = activity.getApplicationContext().getResources().openRawResource(resid);
		 BitmapDrawable bd = new BitmapDrawable(is);
		  
		 return bd;
	 }
	 
	 public static String[] getLifeText(Context context, int result) {
		 String[] lifeText = null;

		 switch(result) {
		 case 1:
			 lifeText = context.getResources().getStringArray(R.array.life_1);
			 break;
		 case 2:
			 lifeText = context.getResources().getStringArray(R.array.life_2);
			 break;
		 case 3:
			 lifeText = context.getResources().getStringArray(R.array.life_3);
			 break;
		 case 4:
			 lifeText = context.getResources().getStringArray(R.array.life_4);
			 break;
		 case 5:
			 lifeText = context.getResources().getStringArray(R.array.life_5);
			 break;
		 case 6:
			 lifeText = context.getResources().getStringArray(R.array.life_6);
			 break;
		 case 7:
			 lifeText = context.getResources().getStringArray(R.array.life_7);
			 break;
		 case 8:
			 lifeText = context.getResources().getStringArray(R.array.life_8);
			 break;
		 case 9:
			 lifeText = context.getResources().getStringArray(R.array.life_9);
			 break;
		 case 10:
			 lifeText = context.getResources().getStringArray(R.array.life_10);
			 break;
		 case 11:
			 lifeText = context.getResources().getStringArray(R.array.life_11);
			 break;
		 case 12:
			 lifeText = context.getResources().getStringArray(R.array.life_12);
			 break;
		 case 13:
			 lifeText = context.getResources().getStringArray(R.array.life_13);
			 break;
		 case 14:
			 lifeText = context.getResources().getStringArray(R.array.life_14);
			 break;
		 case 15:
			 lifeText = context.getResources().getStringArray(R.array.life_15);
			 break;
		 case 16:
			 lifeText = context.getResources().getStringArray(R.array.life_16);
			 break;
		 case 17:
			 lifeText = context.getResources().getStringArray(R.array.life_17);
			 break;
		 case 18:
			 lifeText = context.getResources().getStringArray(R.array.life_18);
			 break;
		 case 19:
			 lifeText = context.getResources().getStringArray(R.array.life_19);
			 break;
		 case 20:
			 lifeText = context.getResources().getStringArray(R.array.life_20);
			 break;
		 case 21:
			 lifeText = context.getResources().getStringArray(R.array.life_21);
			 break;
		 case 22:
			 lifeText = context.getResources().getStringArray(R.array.life_22);
			 break;
		 case 23:
			 lifeText = context.getResources().getStringArray(R.array.life_23);
			 break;
		 case 24:
			 lifeText = context.getResources().getStringArray(R.array.life_24);
			 break;
		 case 25:
			 lifeText = context.getResources().getStringArray(R.array.life_25);
			 break;
		 case 26:
			 lifeText = context.getResources().getStringArray(R.array.life_26);
			 break;
		 case 27:
			 lifeText = context.getResources().getStringArray(R.array.life_27);
			 break;
	     default:
	    	 break;
		 }
		 
		 return lifeText;
	 }
	 
	 public static String[] getLoveText(Context context, int result) {
		 String[] loveText = null;

		 switch(result) {
		 case 1:
			 loveText = context.getResources().getStringArray(R.array.love_1);
			 break;
		 case 2:
			 loveText = context.getResources().getStringArray(R.array.love_2);
			 break;
		 case 3:
			 loveText = context.getResources().getStringArray(R.array.love_3);
			 break;
		 case 4:
			 loveText = context.getResources().getStringArray(R.array.love_4);
			 break;
		 case 5:
			 loveText = context.getResources().getStringArray(R.array.love_5);
			 break;
		 case 6:
			 loveText = context.getResources().getStringArray(R.array.love_6);
			 break;
		 case 7:
			 loveText = context.getResources().getStringArray(R.array.love_7);
			 break;
		 case 8:
			 loveText = context.getResources().getStringArray(R.array.love_8);
			 break;
		 case 9:
			 loveText = context.getResources().getStringArray(R.array.love_9);
			 break;
		 case 10:
			 loveText = context.getResources().getStringArray(R.array.love_10);
			 break;
		 case 11:
			 loveText = context.getResources().getStringArray(R.array.love_11);
			 break;
		 case 12:
			 loveText = context.getResources().getStringArray(R.array.love_12);
			 break;
		 case 13:
			 loveText = context.getResources().getStringArray(R.array.love_13);
			 break;
		 case 14:
			 loveText = context.getResources().getStringArray(R.array.love_14);
			 break;
		 case 15:
			 loveText = context.getResources().getStringArray(R.array.love_15);
			 break;
		 case 16:
			 loveText = context.getResources().getStringArray(R.array.love_16);
			 break;
		 case 17:
			 loveText = context.getResources().getStringArray(R.array.love_17);
			 break;
		 case 18:
			 loveText = context.getResources().getStringArray(R.array.love_18);
			 break;
		 case 19:
			 loveText = context.getResources().getStringArray(R.array.love_19);
			 break;
		 case 20:
			 loveText = context.getResources().getStringArray(R.array.love_20);
			 break;
		 case 21:
			 loveText = context.getResources().getStringArray(R.array.love_21);
			 break;
		 case 22:
			 loveText = context.getResources().getStringArray(R.array.love_22);
			 break;
		 case 23:
			 loveText = context.getResources().getStringArray(R.array.love_23);
			 break;
		 case 24:
			 loveText = context.getResources().getStringArray(R.array.love_24);
			 break;
		 case 25:
			 loveText = context.getResources().getStringArray(R.array.love_25);
			 break;
		 case 26:
			 loveText = context.getResources().getStringArray(R.array.love_26);
			 break;
		 case 27:
			 loveText = context.getResources().getStringArray(R.array.love_27);
			 break;
	     default:
	    	 break;
		 }
		 
		 return loveText;
	 }
	 
	 public static String[] getJobText(Context context, int result) {
		 String[] jobText = null;

		 switch(result) {
		 case 1:
			 jobText = context.getResources().getStringArray(R.array.job_1);
			 break;
		 case 2:
			 jobText = context.getResources().getStringArray(R.array.job_2);
			 break;
		 case 3:
			 jobText = context.getResources().getStringArray(R.array.job_3);
			 break;
		 case 4:
			 jobText = context.getResources().getStringArray(R.array.job_4);
			 break;
		 case 5:
			 jobText = context.getResources().getStringArray(R.array.job_5);
			 break;
		 case 6:
			 jobText = context.getResources().getStringArray(R.array.job_6);
			 break;
		 case 7:
			 jobText = context.getResources().getStringArray(R.array.job_7);
			 break;
		 case 8:
			 jobText = context.getResources().getStringArray(R.array.job_8);
			 break;
		 case 9:
			 jobText = context.getResources().getStringArray(R.array.job_9);
			 break;
		 case 10:
			 jobText = context.getResources().getStringArray(R.array.job_10);
			 break;
		 case 11:
			 jobText = context.getResources().getStringArray(R.array.job_11);
			 break;
		 case 12:
			 jobText = context.getResources().getStringArray(R.array.job_12);
			 break;
		 case 13:
			 jobText = context.getResources().getStringArray(R.array.job_13);
			 break;
		 case 14:
			 jobText = context.getResources().getStringArray(R.array.job_14);
			 break;
		 case 15:
			 jobText = context.getResources().getStringArray(R.array.job_15);
			 break;
		 case 16:
			 jobText = context.getResources().getStringArray(R.array.job_16);
			 break;
		 case 17:
			 jobText = context.getResources().getStringArray(R.array.job_17);
			 break;
		 case 18:
			 jobText = context.getResources().getStringArray(R.array.job_18);
			 break;
		 case 19:
			 jobText = context.getResources().getStringArray(R.array.job_19);
			 break;
		 case 20:
			 jobText = context.getResources().getStringArray(R.array.job_20);
			 break;
		 case 21:
			 jobText = context.getResources().getStringArray(R.array.job_21);
			 break;
		 case 22:
			 jobText = context.getResources().getStringArray(R.array.job_22);
			 break;
		 case 23:
			 jobText = context.getResources().getStringArray(R.array.job_23);
			 break;
		 case 24:
			 jobText = context.getResources().getStringArray(R.array.job_24);
			 break;
		 case 25:
			 jobText = context.getResources().getStringArray(R.array.job_25);
			 break;
		 case 26:
			 jobText = context.getResources().getStringArray(R.array.job_26);
			 break;
		 case 27:
			 jobText = context.getResources().getStringArray(R.array.job_27);
			 break;
	     default:
	    	 break;
		 }
		 
		 return jobText;
	 }
	 
	 public static String[] getPersonalityText(Context context, int result) {
		 String[] personalityText = null;

		 switch(result) {
		 case 1:
			 personalityText = context.getResources().getStringArray(R.array.personality_1);
			 break;
		 case 2:
			 personalityText = context.getResources().getStringArray(R.array.personality_2);
			 break;
		 case 3:
			 personalityText = context.getResources().getStringArray(R.array.personality_3);
			 break;
		 case 4:
			 personalityText = context.getResources().getStringArray(R.array.personality_4);
			 break;
		 case 5:
			 personalityText = context.getResources().getStringArray(R.array.personality_5);
			 break;
		 case 6:
			 personalityText = context.getResources().getStringArray(R.array.personality_6);
			 break;
		 case 7:
			 personalityText = context.getResources().getStringArray(R.array.personality_7);
			 break;
		 case 8:
			 personalityText = context.getResources().getStringArray(R.array.personality_8);
			 break;
		 case 9:
			 personalityText = context.getResources().getStringArray(R.array.personality_9);
			 break;
		 case 10:
			 personalityText = context.getResources().getStringArray(R.array.personality_10);
			 break;
		 case 11:
			 personalityText = context.getResources().getStringArray(R.array.personality_11);
			 break;
		 case 12:
			 personalityText = context.getResources().getStringArray(R.array.personality_12);
			 break;
		 case 13:
			 personalityText = context.getResources().getStringArray(R.array.personality_13);
			 break;
		 case 14:
			 personalityText = context.getResources().getStringArray(R.array.personality_14);
			 break;
		 case 15:
			 personalityText = context.getResources().getStringArray(R.array.personality_15);
			 break;
		 case 16:
			 personalityText = context.getResources().getStringArray(R.array.personality_16);
			 break;
		 case 17:
			 personalityText = context.getResources().getStringArray(R.array.personality_17);
			 break;
		 case 18:
			 personalityText = context.getResources().getStringArray(R.array.personality_18);
			 break;
		 case 19:
			 personalityText = context.getResources().getStringArray(R.array.personality_19);
			 break;
		 case 20:
			 personalityText = context.getResources().getStringArray(R.array.personality_20);
			 break;
		 case 21:
			 personalityText = context.getResources().getStringArray(R.array.personality_21);
			 break;
		 case 22:
			 personalityText = context.getResources().getStringArray(R.array.personality_22);
			 break;
		 case 23:
			 personalityText = context.getResources().getStringArray(R.array.personality_23);
			 break;
		 case 24:
			 personalityText = context.getResources().getStringArray(R.array.personality_24);
			 break;
		 case 25:
			 personalityText = context.getResources().getStringArray(R.array.personality_25);
			 break;
		 case 26:
			 personalityText = context.getResources().getStringArray(R.array.personality_26);
			 break;
		 case 27:
			 personalityText = context.getResources().getStringArray(R.array.personality_27);
			 break;
	     default:
	    	 break;
		 }
		 
		 return personalityText;
	 }
	 
	 public static String[] getFortuneText(Context context, String result) {
		 String[] fortuneText = null;
		 
		 if(result.equals("자신")) {
			 fortuneText = context.getResources().getStringArray(R.array.today_1);
			} else if(result.equals("동인")) {
				fortuneText = context.getResources().getStringArray(R.array.today_2);
			} else if(result.equals("양광")) {
				fortuneText = context.getResources().getStringArray(R.array.today_3);
			} else if(result.equals("소멸")) {
				fortuneText = context.getResources().getStringArray(R.array.today_4);
			} else if(result.equals("성공")) {
				fortuneText = context.getResources().getStringArray(R.array.today_5);
			} else if(result.equals("위험")) {
				fortuneText = context.getResources().getStringArray(R.array.today_6);
			} else if(result.equals("안주")) {
				fortuneText = context.getResources().getStringArray(R.array.today_7);
			} else if(result.equals("음약")) {
				fortuneText = context.getResources().getStringArray(R.array.today_8);
			} else if(result.equals("다복")) {
				fortuneText = context.getResources().getStringArray(R.array.today_9);
			} else if(result.equals("인연")) {
				fortuneText = context.getResources().getStringArray(R.array.today_10);
			} else if(result.equals("친구")) {
				fortuneText = context.getResources().getStringArray(R.array.today_11);
			} else if(result.equals("양명")) {
				fortuneText = context.getResources().getStringArray(R.array.today_12);
			} else if(result.equals("멸망")) {
				fortuneText = context.getResources().getStringArray(R.array.today_13);
			} else if(result.equals("성취")) {
				fortuneText = context.getResources().getStringArray(R.array.today_14);
			} else if(result.equals("위태")) {
				fortuneText = context.getResources().getStringArray(R.array.today_15);
			} else if(result.endsWith("안정")) {
				fortuneText = context.getResources().getStringArray(R.array.today_16);
			} else if(result.equals("쇠약")) {
				fortuneText = context.getResources().getStringArray(R.array.today_17);
			} else if(result.equals("행복")) {
				fortuneText = context.getResources().getStringArray(R.array.today_18);
			} else if(result.equals("응보")) {
				fortuneText = context.getResources().getStringArray(R.array.today_19);
			} else if(result.equals("상서")) {
				fortuneText = context.getResources().getStringArray(R.array.today_20);
			} else if(result.equals("원양")) {
				fortuneText = context.getResources().getStringArray(R.array.today_21);
			} else if(result.equals("자멸")) {
				fortuneText = context.getResources().getStringArray(R.array.today_22);
			} else if(result.equals("입신")) {
				fortuneText = context.getResources().getStringArray(R.array.today_23);
			} else if(result.equals("위급")) {
				fortuneText = context.getResources().getStringArray(R.array.today_24);
			} else if(result.equals("편안")) {
				fortuneText = context.getResources().getStringArray(R.array.today_25);
			} else if(result.equals("나약")) {
				fortuneText = context.getResources().getStringArray(R.array.today_26);
			} else if(result.equals("복덕")) {
				fortuneText = context.getResources().getStringArray(R.array.today_27);
			} else {
				//ERR
			}
		 
		 return fortuneText;
	 }
	 
	 public static String[] getSendText(Context context, String result) {
		 String[] sendText = null;
		 
		 if(result.equals("자신")) {
			 sendText = context.getResources().getStringArray(R.array.widget_today_fortune_1);
			} else if(result.equals("동인")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_2);
			} else if(result.equals("양광")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_3);
			} else if(result.equals("소멸")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_4);
			} else if(result.equals("성공")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_5);
			} else if(result.equals("위험")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_6);
			} else if(result.equals("안주")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_7);
			} else if(result.equals("음약")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_8);
			} else if(result.equals("다복")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_9);
			} else if(result.equals("인연")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_10);
			} else if(result.equals("친구")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_11);
			} else if(result.equals("양명")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_12);
			} else if(result.equals("멸망")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_13);
			} else if(result.equals("성취")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_14);
			} else if(result.equals("위태")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_15);
			} else if(result.endsWith("안정")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_16);
			} else if(result.equals("쇠약")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_17);
			} else if(result.equals("행복")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_18);
			} else if(result.equals("응보")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_19);
			} else if(result.equals("상서")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_20);
			} else if(result.equals("원양")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_21);
			} else if(result.equals("자멸")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_22);
			} else if(result.equals("입신")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_23);
			} else if(result.equals("위급")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_24);
			} else if(result.equals("편안")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_25);
			} else if(result.equals("나약")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_26);
			} else if(result.equals("복덕")) {
				sendText = context.getResources().getStringArray(R.array.widget_today_fortune_27);
			} else {
				//ERR
			}
		 
		 return sendText;
	 }
	 
	 public static String[] getFortuneNumeric(Context context, String result) {
		 String[] fortuneNumeric = null;
		 
		 if(result.equals("자신")) {
			 fortuneNumeric = context.getResources().getStringArray(R.array.numeric_1);
			} else if(result.equals("동인")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_2);
			} else if(result.equals("양광")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_3);
			} else if(result.equals("소멸")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_4);
			} else if(result.equals("성공")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_5);
			} else if(result.equals("위험")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_6);
			} else if(result.equals("안주")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_7);
			} else if(result.equals("음약")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_8);
			} else if(result.equals("다복")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_9);
			} else if(result.equals("인연")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_10);
			} else if(result.equals("친구")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_11);
			} else if(result.equals("양명")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_12);
			} else if(result.equals("멸망")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_13);
			} else if(result.equals("성취")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_14);
			} else if(result.equals("위태")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_15);
			} else if(result.endsWith("안정")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_16);
			} else if(result.equals("쇠약")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_17);
			} else if(result.equals("행복")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_18);
			} else if(result.equals("응보")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_19);
			} else if(result.equals("상서")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_20);
			} else if(result.equals("원양")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_21);
			} else if(result.equals("자멸")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_22);
			} else if(result.equals("입신")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_23);
			} else if(result.equals("위급")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_24);
			} else if(result.equals("편안")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_25);
			} else if(result.equals("나약")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_26);
			} else if(result.equals("복덕")) {
				fortuneNumeric = context.getResources().getStringArray(R.array.numeric_27);
			} else {
				//ERR
			}
		 
		 return fortuneNumeric;
	 }
	 
	 public static String[] getHarmonyResultText(Context context, String result) {
		 String[] harmonyResultText = null;
		 
		 if(result.equals("동")) {
			 harmonyResultText = context.getResources().getStringArray(R.array.match_1);
			} else if(result.equals("복")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_2);
			} else if(result.equals("쇠")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_3);
			} else if(result.equals("안")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_4);
			} else if(result.equals("험")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_5);
			} else if(result.equals("성")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_6);
			} else if(result.equals("멸")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_7);
			} else if(result.equals("양")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_8);
			} else if(result.equals("친")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_9);
			} else if(result.equals("인")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_10);
			} else if(result.equals("과")) {
				harmonyResultText = context.getResources().getStringArray(R.array.match_11);
			} else {
				
			}
		 
		 return harmonyResultText;
	 }
	 
	 public static String getHarmonySummaryText(Context context, String result) {
		 String harmonySummaryText = null;
		 
		 if(result.equals("동")) {
			 harmonySummaryText = context.getResources().getString(R.string.match_summary_1);
		 } else if(result.equals("복")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_2);
		 } else if(result.equals("쇠")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_3);
		 } else if(result.equals("안")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_4);
		 } else if(result.equals("험")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_5);
		 } else if(result.equals("성")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_6);
		 } else if(result.equals("멸")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_7);
		 } else if(result.equals("양")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_8);
		 } else if(result.equals("친")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_9);
		 } else if(result.equals("인")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_10);
		 } else if(result.equals("과")) {
			harmonySummaryText = context.getResources().getString(R.string.match_summary_11);
		 } else {
			
		 }
		 
		 return harmonySummaryText;
	 }
	 
	 public static int getDisplayCenterPixelHeight(Activity activity) {
		 DisplayMetrics displayMetrics = new DisplayMetrics();
		 activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		 
		 int deviceHeight = displayMetrics.heightPixels / 2;
		 
		 return deviceHeight;
	 }
	 
	 public static String getTodayOfMonth() {
		 Date date = new Date();
		 SimpleDateFormat format = new SimpleDateFormat("dd");
		 
		 return format.format(date);
	 }
	 
	 public static String getMonthOfYear() {
		 Date date = new Date();
		 SimpleDateFormat format = new SimpleDateFormat("MM");
		 
		 return format.format(date);
	 }
	 
	 public static String getYear() {
		 Date date = new Date();
		 SimpleDateFormat format = new SimpleDateFormat("yyyy");
		 
		 return format.format(date);
	 }
	 
	 /**
		 * dip 를 pixel 로 변환합니다.
		 * 
		 * @param context
		 * @param dip
		 * @return int형의 pixel 값
		 */
		
	public static int pixelFromDip(Context context, int dip) {
		Resources resources = context.getResources();
		int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
		return pixel;
	}
	
	/**
	 * 핸드폰 화면의 가로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 가로 크기 dip
	 */
	
	public static int getScreenDipWidthSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);
		
		return dipWidth;
	}

	/**
	 * 핸드폰 화면의 세로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 세로 크기 dip
	 */
	
	public static int getScreenDipHeightSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipHeight = (int) (displayMetrics.heightPixels / displayMetrics.density);
		
		return dipHeight;
	}
	
	/**
	 * 사용자의 양력, 음력 생년월일을 재설정하여, SharePreference 에 저장합니다.
	 * 저장이 끝나면, 위젯을 업데이트 하는 인텐트를 브로드 캐스팅 합니다.
	 * 
	 * @param context
	 * @param solarDate
	 * @param lunarDate
	 */
	
	public static void setUserBirthDay(Context context, FortuneDate solarDate, FortuneDate lunarDate) {
		SharedPreferences.Editor prefEditor = context.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE).edit();
		prefEditor.putInt(Constants.USER_SOLAR_YEAR, solarDate.getYear());
		prefEditor.putInt(Constants.USER_SOLAR_MONTH, solarDate.getMonth());
		prefEditor.putInt(Constants.USER_SOLAR_DAY, solarDate.getDay());
		
		prefEditor.putInt(Constants.USER_LUNAR_YEAR, lunarDate.getYear());
		prefEditor.putInt(Constants.USER_LUNAR_MONTH, lunarDate.getMonth());
		prefEditor.putInt(Constants.USER_LUNAR_DAY, lunarDate.getDay());
		
		prefEditor.putInt(Constants.USER_WEST_STAR_POSITION, Util.getWestStarPosition(solarDate.getMonth(), solarDate.getDay()));
		prefEditor.putInt(Constants.USER_EAST_STAR_POSITION, Util.getEastStarPosition(lunarDate.getMonth(), lunarDate.getDay()));
		
		prefEditor.commit();
		
		sendBroadCasting(context, Constants.ACTION_WIDGET_UPDATE);
	}
	
	/**
	 * 파라미터로 넘어온 action 을 BroadCasting 한다.
	 * 
	 * @param context
	 * @param action
	 */
	
	public static void sendBroadCasting(Context context, String action) {
		Intent intent = new Intent(action);
		context.sendBroadcast(intent);
	}
	
	public static int getWidgetFortuneWeather(Context context, String todayFortune) {
		int weather = 0;
		
		if(todayFortune.equals("자신")) {
			weather = Constants.WIDGET_WEATHER_SOSO;
		} else if(todayFortune.equals("동인")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("양광")) {
			weather = Constants.WIDGET_WEATHER_GOOD; 
		} else if(todayFortune.equals("소멸")) {
			weather = Constants.WIDGET_WEATHER_WORST;
		} else if(todayFortune.equals("성공")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("위험")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("안주")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("음약")) {
			weather = Constants.WIDGET_WEATHER_SOSO;
		} else if(todayFortune.equals("다복")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("인연")) {
			weather = Constants.WIDGET_WEATHER_SOSO;
		} else if(todayFortune.equals("친구")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("양명")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("멸망")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("성취")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("위태")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("안정")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("쇠약")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("행복")) {
			weather = Constants.WIDGET_WEATHER_SOSO;
		} else if(todayFortune.equals("응보")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("상서")) {
			weather = Constants.WIDGET_WEATHER_SOSO;
		} else if(todayFortune.equals("원양")) {
			weather = Constants.WIDGET_WEATHER_BEST;
		} else if(todayFortune.equals("자멸")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("입신")) {
			weather = Constants.WIDGET_WEATHER_GOOD;
		} else if(todayFortune.equals("위급")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("편안")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("나약")) {
			weather = Constants.WIDGET_WEATHER_BAD;
		} else if(todayFortune.equals("복덕")) {
			weather = Constants.WIDGET_WEATHER_BEST;
		} 
		
		return weather;
	}
	
	public static String[] getWidgetFortuneText(Context context, String todayFortune) {
		String[] textDisplay = null;
		
		if(todayFortune.equals("자신")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_1);
		} else if(todayFortune.equals("동인")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_2);
		} else if(todayFortune.equals("양광")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_3);
		} else if(todayFortune.equals("소멸")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_4);
		} else if(todayFortune.equals("성공")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_5);
		} else if(todayFortune.equals("위험")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_6);
		} else if(todayFortune.equals("안주")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_7);
		} else if(todayFortune.equals("음약")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_8);
		} else if(todayFortune.equals("다복")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_9);
		} else if(todayFortune.equals("인연")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_10);
		} else if(todayFortune.equals("친구")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_11);
		} else if(todayFortune.equals("양명")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_12);
		} else if(todayFortune.equals("멸망")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_13);
		} else if(todayFortune.equals("성취")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_14);
		} else if(todayFortune.equals("위태")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_15);
		} else if(todayFortune.equals("안정")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_16);
		} else if(todayFortune.equals("쇠약")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_17);
		} else if(todayFortune.equals("행복")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_18);
		} else if(todayFortune.equals("응보")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_19);
		} else if(todayFortune.equals("상서")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_20);
		} else if(todayFortune.equals("원양")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_21);
		} else if(todayFortune.equals("자멸")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_22);
		} else if(todayFortune.equals("입신")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_23);
		} else if(todayFortune.equals("위급")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_24);
		} else if(todayFortune.equals("편안")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_25);
		} else if(todayFortune.equals("나약")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_26);
		} else if(todayFortune.equals("복덕")) {
			textDisplay = context.getResources().getStringArray(R.array.widget_today_fortune_27);
		} 
		
		return textDisplay;
	}
	
	/**
	 * 주어진 파라미터 값에 따라 폰트 파일명을 리턴한다.
	 * 0 은 디폴트 폰트
	 * 반드시 FontThemeListDialog 의 인덱스와 순서를 맞출것!
	 * 
	 * @param index
	 * @return
	 */
	
	public static String getFontFileName(int index) {
		String fontFileName = null;
		
		switch(index) {
		case 0:
			fontFileName = Constants.FONT_NAME_SANS;
			break;
		case 1:
			fontFileName = Constants.FONT_NAME_THREE_BYCICLE;
			break;
		case 2:
			fontFileName = Constants.FONT_NAME_SEOUL_NAMSAN;
			break;
		case 3:
			fontFileName = Constants.FONT_NAME_DAUM;
			break;
		}
		
		return fontFileName;
	}
	
	/**
	 * 
	 * 
	 * @param context
	 * @param path
	 * @param position
	 * @return
	 */
	
	public static Typeface getFontType(Context context, String path, int position) {
		Typeface fontType = null;

		if(position != 0) {
			fontType = fontType.createFromAsset(context.getAssets(), path);
		} else {
			fontType = fontType.DEFAULT;
		}
		
		return fontType;
	}
	
	/**
	 * 주어진 글꼴과 사이즈, 글꼴 색상에 따른 Paint 객체를 리턴한다.
	 * 
	 * @param typeface
	 * @param fontSize
	 * @param color
	 * @return
	 */
	
	public static Paint getFontPaint(Typeface typeface, int fontSize, String colorRGB) {
		Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.parseColor(colorRGB));
        paint.setTypeface(typeface);
        paint.setTextSize(fontSize);
        
        return paint;
	}
	
	public static String getPickColorRGB(Context context, int pickColorIndex) {
		String colorRGB = null;
		
		switch(pickColorIndex) {
		case 0:		//	Black
			colorRGB = "#000000";
			break;
		case 1:	//	White
			colorRGB = "#ffffff";
			break;
		}
		
		return colorRGB;
	}
	
	/**
	 * 주어진 파라미터 값에 따라 Canvas 에 content 가 drawText 된 bitmap 을 리턴한다.
	 * 기본적으로, 텍스트가 표시될 위젯 사이즈를 따라가며, 상세한 설정은 미리 설정 되어있다.
	 * 
	 * @param context
	 * @param fontTypeFace
	 * @param content
	 * @param fontSize
	 * @return
	 */
	
	public static Bitmap getFontBitmapToSetting(Context context, Typeface fontTypeFace, 
			String content, int fontSize, int widthDip, int heightDip, int fontColor) {
		
		// 기본 변수 설정
		int contentIdx = 0;
		int defaultLineBreakIdx = 0;
		
		int marginWidth = 0;
		int marginHeight = 5;
		
		// Paint 설정
		Paint fontPaint = Util.getFontPaint(fontTypeFace, fontSize, Util.getPickColorRGB(context, fontColor));
        
        Bitmap bitmap = Bitmap.createBitmap(Util.pixelFromDip(context, widthDip), Util.pixelFromDip(context, heightDip), Bitmap.Config.ARGB_4444);
        Canvas canvas = new Canvas(bitmap);
        bitmap.eraseColor(0);

        if(content.length() <= fontPaint.breakText(content, true, Util.pixelFromDip(context, widthDip), null)) {
        	defaultLineBreakIdx = fontPaint.breakText(content, true, Util.pixelFromDip(context, widthDip), null);
        	contentIdx = 1;
        } else {
        	defaultLineBreakIdx = fontPaint.breakText(content, true, Util.pixelFromDip(context, widthDip), null)-2;		// Line Break 길이를 구함

        	content = Util.getDelWhiteSpaceContent(content, 0, defaultLineBreakIdx, defaultLineBreakIdx);
        	
        	if((content.length() % defaultLineBreakIdx) != 0) {
        		contentIdx = (content.length() / defaultLineBreakIdx) + 1;
        	} else {
        		contentIdx = content.length() / defaultLineBreakIdx;
        	}
        }
        
        // 텍스트 위치 조정
        int startIdx = 0;
        int endBreakIdx = defaultLineBreakIdx;
        int tinyStartHeight = 2;						//	미세하게 높이 값을 조정하기 위한 변수 (default : 2)
        
        String[] reContent = new String[contentIdx];
        
        // 가운데 정렬을 위한 코드
        int maxHeight = Util.pixelFromDip(context, heightDip);
        int textHeight = (fontSize * contentIdx) + (marginHeight * (contentIdx-1));
        int startPointHeight = ((maxHeight - textHeight) / 2) - tinyStartHeight;
        
        if(startPointHeight < 0) {
        	startPointHeight = 0;
        }
        
        for(int i=0; i < contentIdx; i++) {
        	if(endBreakIdx > content.length()) {
        		endBreakIdx = content.length();
        	}
        	
        	reContent[i] = content.substring(startIdx, endBreakIdx);
        	startIdx = endBreakIdx;
        	endBreakIdx = endBreakIdx + defaultLineBreakIdx;

    		canvas.drawText(reContent[i], marginWidth, startPointHeight+((fontSize * (i+1)) + (marginHeight * i)), fontPaint);
        }
        
        return bitmap;
	}
	
	/**
	 * content 의 Start 에서 end 사이의 공백을 제거한 뒤, defaultBreak 길이에 따라 content를 한줄 씩 내린 값을 반환합니다.
	 * 
	 * @param content
	 * @param start
	 * @param end
	 * @param defaultBreak
	 * @return
	 */
	
	public static String getDelWhiteSpaceContent(String content, int start, int end, int defaultBreak) {
		StringBuffer strBuf = new StringBuffer();
		
		int startIdx = start;
		int endBreakIdx = end;
		int defaultLineBreakIdx = defaultBreak;
		
		while(true) {
			if(content.substring(startIdx, startIdx+1).equals(" ")) {
        		startIdx += 1;
        		endBreakIdx += 1;
        	}
        	
        	if(endBreakIdx > content.length()) {
        		endBreakIdx = content.length();
        	}
        	
        	strBuf.append(content.subSequence(startIdx, endBreakIdx));
        	
        	startIdx = endBreakIdx;
        	endBreakIdx = endBreakIdx + defaultLineBreakIdx;

    		if(startIdx >= content.length()) {
    			break;
    		}
		}
		
		return strBuf.toString();
	}
	
	/**
	 * 입력받은 voice 텍스트에서 년,월,일에 해당하는 숫자만을 분해한 뒤, FortuneDate 객체로 반환합니다.
	 * 
	 * @param voice
	 * @return
	 */
	
	public static String getSplitVoiceFortune(String voice) {
		char[] charArray = voice.toCharArray();
		
		StringBuffer strBuf = new StringBuffer();
		int year = 0, month = 0, day = 0;
		int tempIndex = 0;
		for(int i=0; i < charArray.length; i++) {
			if(charArray[i] == '년') {
				for(int j=0; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						return null;
					}
					strBuf.append(charArray[j]);
				}
				
				year = Integer.parseInt(strBuf.toString().trim());
				strBuf.setLength(0);
				tempIndex = i+1;
			}
			
			if(charArray[i] == '월') {
				for(int j=tempIndex; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						return null;
					}
					strBuf.append(charArray[j]);
				}
				
				month = Integer.parseInt(strBuf.toString().trim());
				
				strBuf.setLength(0);
				tempIndex = i+1;
			}
			
			if(charArray[i] == '일') {
				for(int j=tempIndex; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						return null;
					}
					strBuf.append(charArray[j]);
				}
				
				day = Integer.parseInt(strBuf.toString().trim());
				strBuf.setLength(0);
				tempIndex = i+1;
			}
		}
		
		if(year != 0 && month != 0 && day != 0) {
			
			if(isCorrect("Test", String.valueOf(year), String.valueOf(month), String.valueOf(day)) == 0) {
				return (year + "," + month + "," + day);
			}
			
			return null;
		}
		
		return null;
	}
	
	 /**
	  * 어플리케이션의 언어 설정을 변경합니다.
	  * 
	  * @param context
	  * @param language
	  */
	 public static void setLocale(Context context, String language) {
		 Locale locale = new Locale(language); 
	   	 Locale.setDefault(locale);
	   	 Configuration config = new Configuration();
	   	 config.locale = locale;
	   	 context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
   	 
	 }
}

















