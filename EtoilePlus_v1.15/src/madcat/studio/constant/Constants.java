package madcat.studio.constant;

import android.appwidget.AppWidgetManager;
import android.provider.BaseColumns;

public class Constants implements BaseColumns {
	
	public static final boolean DEVELOPE_MODE										=	true;
	
	//USER PREFERENCE CONSTANTS.
	public static final String USER_PREFERENCE_NAME									=	"USER_PREPERENCE";
	
	public static final String USER_NAME											=	"NAME";
	public static final String USER_GENDER											=	"GENDER";
	public static final String USER_BIRTH_TYPE										=	"TYPE";
	public static final String USER_SOLAR_YEAR										=	"SY";
	public static final String USER_SOLAR_MONTH										=	"SM";
	public static final String USER_SOLAR_DAY										=	"SD";
	public static final String USER_LUNAR_YEAR										=	"LY";
	public static final String USER_LUNAR_MONTH										=	"LM";
	public static final String USER_LUNAR_DAY										=	"LD";
	public static final String USER_WEST_STAR_POSITION								=	"WEST";
	public static final String USER_EAST_STAR_POSITION								=	"EAST";
	
	//MATE PREFERENCE CONSTANTS.
	public static final String MATE_PREFERENCE_NAME									=	"MATE_PREFERENCE";
	
	public static final String MATE_NAME											=	"NAME";
	public static final String MATE_GENDER											=	"GENDER";
	public static final String MATE_BIRTH_TYPE										=	"TYPE";
	public static final String MATE_SOLAR_YEAR										=	"SY";
	public static final String MATE_SOLAR_MONTH										=	"SM";
	public static final String MATE_SOLAR_DAY										=	"SD";
	public static final String MATE_LUNAR_YEAR										=	"LY";
	public static final String MATE_LUNAR_MONTH										=	"LM";
	public static final String MATE_LUNAR_DAY										=	"LD";
	public static final String MATE_WEST_STAR_POSITION								=	"WEST";
	public static final String MATE_EAST_STAR_POSITION								=	"EAST";
	
	//CONFIG PREFERENCE CONSTANTS.
	public static final String CONFIG_PREFERENCE_NAME								=	"CONFIG_PREFERENCE";
	
	public static final String CONFIG_TODAY_DATE									=	"TODAY_DATE";
	public static final String CONFIG_FONT_SIZE										=	"FONT SIZE";
	public static final String CONFIG_WIDGET_STYLE_TYPE								=	"WIDGET_STYLE_TYPE";
	public static final String CONFIG_WIDGET_BAR_THEME_ITEM							=	"WIDGET_BAR_THEME_ITEM";
	public static final String CONFIG_WIDGET_CHARACTER_THEME_ITEM					=	"WIDGET_CHARACTER_THEME_ITEM";
	public static final String CONFIG_WIDGET_FONT_COLOR_INDEX						=	"WIDGET_FONT_COLOR_INDEX";
	public static final String CONFIG_FONT_THEME_ITEM								=	"FONT_THEME_ITEM";
	public static final String CONFIG_CHECK_FORTUNE_STAR							=	"FORTUNE_STAR_FLAG";
	public static final String CONFIG_FORTUNE_STAR_NUMBER							=	"FORTUNE_STAR_NUMBER";
	public static final String CONFIG_FORTUNE_STAR_PLACE							=	"FORTUNE_STAR_PLACE";
	public static final String CONFIG_FORTUNE_STAR_COLOR							=	"FORTUNE_STAR_COLOR";
	public static final String CONFIG_FORTUNE_STAR_LOTTO							=	"FORTUNE_STAR_LOTTO";
	public static final String CONFIG_SELECT_LANGUAGE								=	"SELECT_LANGUAGE";
	
	//DATABASE CONSTANTS.
	public static final String ROOT_DIR												=	"/data/data/madcat.studio.plus/databases/";
	public static final String FILE_NAME											=	"calendarDB.db";

	public static final String DATABASE_NAME										=	"calendarDB.db";
	public static final int DATABASE_VERSION										=	1;
	
	public static final String TABLE_CALENDAR										=	"CALENDAR";
	public static final String TABLE_FRIEND											=	"FRIENDS";
	
	public static final String ATTRIBUTE_SOLAR_YEAR									=	"cd_sy";
	public static final String ATTRIBUTE_SOLAR_MONTH								=	"cd_sm";
	public static final String ATTRIBUTE_SOLAR_DAY									=	"cd_sd";
	
	public static final String ATTRIBUTE_LUNAR_YEAR									=	"cd_ly";
	public static final String ATTRIBUTE_LUNAR_MONTH								=	"cd_lm";
	public static final String ATTRIBUTE_LUNAR_DAY									=	"cd_ld";
	
	public static final String ATTRIBUTE_FRIEND_NAME								=	"fr_name";
	public static final String ATTRIBUTE_FRIEND_GENDER								=	"fr_gender";
	public static final String ATTRIBUTE_FRIEND_BIRTH_TYPE							=	"fr_birth_type";
	
	public static final String ATTRIBUTE_FRIEND_SOLAR_YEAR							=	"fr_sy";
	public static final String ATTRIBUTE_FRIEND_SOLAR_MONTH							=	"fr_sm";
	public static final String ATTRIBUTE_FRIEND_SOLAR_DAY							=	"fr_sd";
	
	public static final String ATTRIBUTE_FRIEND_LUNAR_YEAR							=	"fr_ly";
	public static final String ATTRIBUTE_FRIEND_LUNAR_MONTH							=	"fr_lm";
	public static final String ATTRIBUTE_FRIEND_LUNAR_DAY							=	"fr_ld";
	
	public static final int SCROLL_VIEW_MARGIN										=	40;
	
	public static final long VIBRATE_MILLSECONDS									=	100; 
	
	public static final int FLAG_FRIENDS_LIST_SEARCH								=	1;
	public static final int FLAG_FRIENDS_TODAYFORTUNE_SEARCH						=	2;
	public static final int FLAG_FRIENDS_PHONE_SEARCH								=	3;

	public static final int MENU_UPLOAD_SNS											=	1;
	public static final int MENU_ADD_FRIENDS										=	2;
	public static final int MENU_SEND_SMS											=	3;
	public static final int MENU_SEND_KAKAO											=	4;
	public static final int MENU_VIEW_LIST											=	5;
	public static final int MENU_VIEW_CALENDAR										=	6;
	
	public static final int ME2DAY_LOGIN_FLAG										=	10;
	public static final String ME2DAY_URL											=	"http://me2day.net/plugins/mobile_post/new?";
	public static final String ME2DAY_BODY_PARAM									=	"new_post[body]=";
	public static final String ME2DAY_TAG_PARAM										=	"new_post[tags]=";
	public static final String ME2DAY_TAG											=	"별자리 앱 Etoile 에서 전송";
	public static final String ME2DAY_SIGN											=	"&";
	
	public static final String FACEBOOK_APP_ID										=	"174268895966223";

	public static final int TWITTER_LOGIN_FLAG										=	20;
	public static final String TWITTER_API_KEY										=	"FQv2sjbVdnppFis4RzSftw";
	public static final String TWITTER_CONSUMER_KEY									=	"FQv2sjbVdnppFis4RzSftw";
	public static final String TWITTER_CONSUMER_SECRET								=	"IKKACU7JyI4iE08imTEw5Ibh1AtFrJUugpDrzkA";
	public static final String TWITTER_PREFERENCE_ACCESS_TOKEN						=	"TWITTER_ACCESS_TOKEN";
	public static final String TWITTER_PREFERENCE_ACCESS_SECRET						=	"TWITTER_ACCESS_SECRET";
	public static final String TWITTER_PREFERENCE_PIN_CODE							=	"TWITTER_PIN_CODE";
	
//	public static final String TODAY_FORTUNE_TITLE									=	"오늘의 운세는 ";
//	public static final String TOMORROW_FORTUNE_TITLE								=	"내일의 운세는 ";
	
//	public static final String TEXT_WHITE_SPACE										=	"&nbsp";
	public static final String TEXT_WHITE_SPACE										=	" ";
	
	// 카카오톡 관련 전역 변수
	public static final String KAKAO_STR_MSG										=	"오늘의 운세 전달 될 사항"; 
	public static final String KAKAO_STR_URL										=	"https://market.android.com/details?id=madcat.studio.main";
	public static final String KAKAO_STR_INSTALL_URL								=	"https://market.android.com/details?id=madcat.studio.main";
	public static final String KAKAO_STR_APPID										=	"madcat.studio.main";
	public static final String KAKAO_STR_APPVER										=	"1.1.4";
	public static final String KAKAO_STR_APPNAME									=	"오늘의 운세 Etoile";
	
	// 인텐트 액션
	public static final String ACTION_WIDGET_UPDATE_THEME							=	"action.Etoile.WIDGET_UPDATE_THEME";
	public static final String ACTION_WIDGET_UPDATE									=	AppWidgetManager.ACTION_APPWIDGET_UPDATE;
	public static final String ACTION_CHANGE_LANGUAGE								=	"action.Etoile.LANGUAGE";
	
	// 위젯 스타일 변수
	public static final int WIDGET_STYLE_BAR										=	0;
	public static final int WIDGET_STYLE_CHARACTER									=	1;
	
	// 위젯 바 날씨 상태
	public static final int WIDGET_WEATHER_BEST										=	0;
	public static final int WIDGET_WEATHER_GOOD										=	1;
	public static final int WIDGET_WEATHER_SOSO										=	2;
	public static final int WIDGET_WEATHER_BAD										=	3;
	public static final int WIDGET_WEATHER_WORST									=	4;
	
	// 폰트 설정
	public static final int FONT_WIDGET_DEFAULT_SIZE								=	20;
	public static final String FONT_FILE_DIR										=	"file:///android_asset/fonts/";
	public static final String FONT_PREFIX_PATH										=	"fonts/";
	public static final String FONT_SUFFIX_TTF										=	".ttf";
	
	public static final String FONT_NAME_SANS										=	"sans";							//	index										=	0
	public static final String FONT_NAME_THREE_BYCICLE								=	"ThreeBycicle";
	public static final String FONT_NAME_SEOUL_NAMSAN								=	"SeoulNamsan";			//	index										=	4
	public static final String FONT_NAME_DAUM										=	"DaumSemiBold";
	
	// 위젯에 표시될 텍스트 영역 사이즈 (DIP)
	public static final int WIDGET_WIDTH_DIP										=	235;
	public static final int WIDGET_HEIGHT_DIP										=	70;
	
	// 캐릭터 위젯에 표시될 텍스트 영역 사이즈 (DIP)
	public static final int WIDGET_CHARACTER_WIDTH_DIP								=	240;
	public static final int WIDGET_CHARACTER_HEIGHT_DIP								=	85;
	
	// 글씨 크기 관련 변수
	public static final int FONT_PLUS_SIZE = 13;
	
}
