package madcat.studio.main;

import java.util.ArrayList;

import madcat.studio.adapter.FriendsAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.fortune.TodayFortune;
import madcat.studio.friends.Friends;
import madcat.studio.friends.FriendsList;
import madcat.studio.friends.FriendsTodayFortune;
import madcat.studio.plus.R;
import madcat.studio.setting.Settings;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;

public class TodayFortuenMenu extends Activity {

//	private static final String TAG = "TodayFortuneMenu";
	
	private ImageButton mUserFortune, mFriendsFortune;
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_today_fortune_submenu);
		
		mContext = this;
		
		mUserFortune = (ImageButton)findViewById(R.id.user_today_fortune);
		mFriendsFortune = (ImageButton)findViewById(R.id.friend_today_fortune);
		
		mUserFortune.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				AsyncLoadingMenu asyncLoadingMenu = new AsyncLoadingMenu();
				asyncLoadingMenu.execute();
			}
		});
		
		mFriendsFortune.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				showFriendsListDialog();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		
		MenuInflater menuInflater = new MenuInflater(getApplicationContext());
		menuInflater.inflate(R.menu.todayfortunemenu, menu);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
		case R.id.manage_my_info:
			Intent intent_setting = new Intent(TodayFortuenMenu.this, Settings.class);
			startActivity(intent_setting);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			break;
		case R.id.manage_friends_list:
			Intent intent_friendList = new Intent(TodayFortuenMenu.this, FriendsList.class);
			startActivity(intent_friendList);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			break;
		}
		
		return result;
	}
	
	private void showFriendsListDialog() {
		final ArrayList<Friends> friendsOrders = Util.getFriendsList();

		if(friendsOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_not_registe_friends), Toast.LENGTH_SHORT).show();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.dialog_title_frineds_list)).setCancelable(true).setAdapter(new FriendsAdapter(getApplicationContext(), 
					R.layout.setting_friends_row, friendsOrders, Constants.FLAG_FRIENDS_TODAYFORTUNE_SEARCH), new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(TodayFortuenMenu.this, FriendsTodayFortune.class);
//						Log.d(TAG, "showFriendsListDialog ȣ�� : " + friendsOrders.get(which).getName() + ", " 
//								+ friendsOrders.get(which).getLunarMonth() + ", "
//								+ friendsOrders.get(which).getLunarDay());
						
						intent.putExtra("FRIENDS_NAME", friendsOrders.get(which).getName());
						intent.putExtra("FRIENDS_LUNAR_MONTH", friendsOrders.get(which).getLunarMonth());
						intent.putExtra("FRIENDS_LUNAR_DAY", friendsOrders.get(which).getLunarDay());
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
						dialog.dismiss();
					}
				});
			
			builder.create().show();
		}
	}
	
	public class AsyncLoadingMenu extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog loading;
		
		@Override
		protected Void doInBackground(Void... arg0) {
			Intent intent = new Intent(TodayFortuenMenu.this, TodayFortune.class);
			startActivity(intent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			return null;
		}
		
		@Override
		protected void onPreExecute() {
			loading = ProgressDialog.show(mContext, "", getString(R.string.progress_loading_my_fortune));
		}
		
		@Override
		protected void onPostExecute(Void result) {
			loading.dismiss();
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
