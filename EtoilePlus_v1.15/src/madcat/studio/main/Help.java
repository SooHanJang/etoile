package madcat.studio.main;

import java.util.Locale;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

public class Help extends Activity {
//	private static final String TAG = "Help";
	
	private LinearLayout mMainLayout;
	
	private WebView mWebView;
	private ScrollView mScrollView;
	private String mLocale;
	
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.help);
		
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mMainLayout = (LinearLayout)findViewById(R.id.help_main_layout);
		mWebView = (WebView)findViewById(R.id.help_text);
		mScrollView = (ScrollView)findViewById(R.id.help_scroll_view);
		
		mWebView.setBackgroundColor(0);
		
//		RelativeLayout.LayoutParams setScrollMargin = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
//		setScrollMargin.setMargins(Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN);
//		mScrollView.setLayoutParams(setScrollMargin);
		
		mLocale = getResources().getConfiguration().locale.getLanguage();
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		String helpText;
		
		int fontType = mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0);
		String fontURL = Constants.FONT_FILE_DIR + Util.getFontFileName(fontType) + Constants.FONT_SUFFIX_TTF;
		String head = "<head><style>@font-face {font-family: '" + 
					Util.getFontFileName(fontType) + "';src: url('" + fontURL + "');}body {font-family: '" + 
					Util.getFontFileName(fontType) + "';}</style></head>";
		
		if(mLocale.equals(Locale.KOREAN.toString())) {
			helpText = 
				"<html>" + head + "<body><p align=\"justify\"><font color=white><big><span style=\"font-weight: bold;\">" +
				"1. Etoile란?" + "</span></big><br><br>" +
				"&nbsp;Etoile란, 프랑스어로 &nbsp;'운명의 별' 이란 뜻으로써 사용자가 입력한 기본 정보를 바탕으로 오늘과 내일의 운세 " +
				"그리고 다른 사용자와의 상성을 확인할 수 있는 점술 어플리케이션입니다." + "<br><br>" +
				"<big style=\"font-weight: bold;\">" + 
				"2. 이용방법" + "</big><br><br>" +
				"&nbsp;Etoile는 크게 오늘의 운세, 상성, 환경설정, 나의 운명, 위젯으로 이루어져 있습니다. " + 
				"오늘의 운세에서는 Etoile 설치 시 입력했던 정보를 이용하여 오늘과 내일의 운세를 보여줍니다. " +
				"또한, 자신의 운세를 트위터, 페이스북, 미투데이에 실시간으로 공유가 가능합니다. " +
				"상성에서는 상대방 정보를 입력하면, &nbsp;사용자와 상대방의 정보를 매칭하여 상성 정보를 보여줍니다. " +
				"나의 운명에서는 나의 인생, 성격, 직업, 사랑에 관한 운세를 보여줍니다. " +
				"환경설정에서는 처음 사용자가 등록하였던 정보를 수정할 수 있습니다. " + 
				"위젯에서는 매일 그 날의 운세를 간략하게 텍스트와 아이콘을  통해 만나볼 수 있습니다." + "<br>" +
				"친구관리에서는 친구들의 정보를 등록하여 손 쉽게 친구들의 운세를 확인해 볼 수 있으며" + "<br>" +
				"친구들의 운세를 문자 메시지로 보낼 수 있는 기능을 제공하고 있습니다." + "<br>" +
				"(점술에 이용되는 사용자 정보는 본인 외에 절대 다른 용도로 제공되지 않습니다.)" + "<br><br>" +
				"<big style=\"font-weight: bold;\">" +
				"3. Etoile Tip</big><br><br>" +
				"&nbsp;다른 사용자와 상성을 볼 시에 상대방과 나와의 상성이 서로의 입장에서 다르니 즐겁게 확인해 주시기 바랍니다.<br><br>" +
				"<big style=\"font-weight: bold;\">" +
				"4. 문의사항" + "</big><br>" +
				"<p align=\"center\">번역 : Chris Kang<br>" +
				"madcat@madcatstudio.co.kr</p>" + 
				"</font></p></body></html>";
		} else {
			helpText = 
				"<html>" + head + "<body><p align=\"justify\"><font color=white><big><span style=\"font-weight: bold;\">" +
				"1. What is Etoile?" + "</span></big><br><br>" +
				"&nbsp;Etoile means ‘The star of the destiny’ in French. It is a fortune-telling application which is capable of checking your fortune for today " +
				"and tomorrow according to basic personal information you input. And it also can balance your fortune with somebody else." + "<br><br>" +
				"<big style=\"font-weight: bold;\">" + 
				"2. How to use" + "</big><br><br>" +
				"&nbsp;Etoile consists of today's fortune, Harmony, My destiny, configuration and widget. " + 
				"It shows the fortune of today and tomorrow’s according to the basic information you input. " +
				"Also you can share your fortune with your friends by Twitter, Facebook or Me2day in real time. " +
				"In harmony section, you can check out the information of harmony between you and your opponent. " +
				"In my destiny, my life, personality, career, love horoscope shows on. " +
				"You can edit your basic personal information in configuration section. " + 
				"You can briefly meet your fortune by short text and little icon everyday through activating this widget." + "<br>" +
				"Etoile offers the functions which can help registering your friend’s information so you can check your friend’s fortune " +
				"easily and also can text the friend’s fortune to your buddy in ‘Managing friends’ section." + "<br>" +
				"(We never abuse every user’s personal information which you input to check your fortune-telling in any circumstances.)" + "<br><br>" + 
				"<big style=\"font-weight: bold;\">" +
				"3. Etoile Tip</big><br><br>" +
				"&nbsp;Hope you enjoy the aspects of little different data of the harmony between ‘me inside you’ and ‘you inside me.<br><br>" +
				"<big style=\"font-weight: bold;\">" +
				"4. Question or Suggestion" + "</big><br>" +
				"<p align=\"center\">Translation : Chris Kang<br>" +
				"madcat@madcatstudio.co.kr</p>" + "</font></p></body></html>";
		}
		
		mWebView.loadDataWithBaseURL("http://madcatstudio.co.kr", helpText, "text/html", "UTF-8", null);
	}
	
	@Override
	protected void onDestroy() {
		mMainLayout.requestFocus();
		
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
