package madcat.studio.main;

import madcat.studio.constant.Constants;
import madcat.studio.dialog.FortuneStarDialog;
import madcat.studio.harmony.HarmonyInput;
import madcat.studio.plus.R;
import madcat.studio.setting.Settings;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainMenu extends Activity {
//	private static final String TAG = "MainMenu";

	private Context mContext;
	
	private RelativeLayout mMainLayout;
	private ImageButton mTodayFortune, mHarmony, mConfigration, mHelp, mDestiny, mFortunStar;
	
	private ImageView mShootingStar1;
	private ImageView mShootingStar2;
	private ImageView mShootingStar3;
	
	private BitmapDrawable mBitmapDrawable;
	
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main_menu);
		
		mContext = this;
		
		// Init sharedPreference
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        
		mMainLayout = (RelativeLayout)findViewById(R.id.menu_layout);
        mTodayFortune = (ImageButton)findViewById(R.id.menu_todayfortune);
        mHarmony = (ImageButton)findViewById(R.id.menu_match);
        mHelp = (ImageButton)findViewById(R.id.menu_help);
        mConfigration = (ImageButton)findViewById(R.id.menu_config);
        mDestiny = (ImageButton)findViewById(R.id.menu_destiny);
        mFortunStar = (ImageButton)findViewById(R.id.shooting_star);
        mShootingStar1 = (ImageView)findViewById(R.id.shooting_star1);
        mShootingStar2 = (ImageView)findViewById(R.id.shooting_star2);
        mShootingStar3 = (ImageView)findViewById(R.id.shooting_star3);
        
        mBitmapDrawable = Util.getDrawable(this, R.drawable.menu_help);
        mHelp.setBackgroundDrawable(mBitmapDrawable);
        
        mShootingStar1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star1));
        mShootingStar2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star2));
        mShootingStar3.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star3));
        
        mTodayFortune.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "Go To Today Fortune.");
				Intent intent = new Intent(MainMenu.this, TodayFortuenMenu.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
        
        mHarmony.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "Go To Harmony.");
				Intent intent = new Intent(MainMenu.this, HarmonyInput.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
        
        mConfigration.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "Go To Preference.");
				Intent intent = new Intent(MainMenu.this, Settings.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
        
        mHelp.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, "Go to Help.");
				Intent intent = new Intent(MainMenu.this, Help.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
        
        mDestiny.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(MainMenu.this, Destiny.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
        
        mFortunStar.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(!mConfigPref.getBoolean(Constants.CONFIG_CHECK_FORTUNE_STAR, false)) {		//	포춘 스타를 처음 실행한다면
					Intent intent = new Intent(MainMenu.this, FortuneStar.class);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
				} else {
					FortuneStarDialog fortuneStarDialog = new FortuneStarDialog(mContext);		//	포춘 스타를 이미 하루에 한번 실행하였다면
					fortuneStarDialog.show();
				}
			}
		});
	}        
	
	@Override
	protected void onDestroy() {
//		Log.d(TAG, "Call onDestroy");
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
