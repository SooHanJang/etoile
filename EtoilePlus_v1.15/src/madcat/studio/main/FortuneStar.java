package madcat.studio.main;

import madcat.studio.dialog.FortuneStarDialog;
import madcat.studio.plus.R;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;

public class FortuneStar extends Activity implements SensorEventListener, OnDismissListener {
	
	private final String TAG										=	"FortuneStar";
	
	private static final int SHAKE_SENSOR_COUNT						=	5;
	private static final int SHAKE_THRESHOLD						=	800;
	private static final int DATA_X									=	SensorManager.DATA_X;
	private static final int DATA_Y									=	SensorManager.DATA_Y;
	private static final int DATA_Z									=	SensorManager.DATA_Z;
	
	
	private Context mContext;
	
	private SensorManager mSensorManager;
	private Sensor mAccelSensor;
	
	private long mLastTime;
	private float mSpeed, mLastX, mLastY, mLastZ;
	private float mX, mY, mZ;
	
	private int mCount;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_fortune_star);
		
		mContext = this;
		
		initSensor();
		
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		registeSensor(mSensorManager, this, mAccelSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		unregisteSensor(mSensorManager, this);
	}
	
	public void initSensor() {
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mAccelSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	}
	
	public void registeSensor(SensorManager sensorManager, SensorEventListener listener, Sensor sensor, int rate) {
		
		if(sensor != null) {
			sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}
	}
	
	public void unregisteSensor(SensorManager sensorManager, SensorEventListener listener) {
		
		if(sensorManager != null) {
			sensorManager.unregisterListener(listener);
		}
	}

	public void onSensorChanged(SensorEvent event) {
		if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			long currentTime = System.currentTimeMillis();
			long gabOfTime = (currentTime - mLastTime);
			
			if(gabOfTime > 100) {
				mLastTime = currentTime;
				
				mX = event.values[SensorManager.DATA_X];
				mY = event.values[SensorManager.DATA_Y];
				mZ = event.values[SensorManager.DATA_Z];
				
				mSpeed = Math.abs(mX + mY + mZ - mLastX - mLastY - mLastZ) / gabOfTime * 10000;
				
				if(mSpeed > SHAKE_THRESHOLD) {
//					Log.d(TAG, "이벤트 발생");
					mCount++;
				} else if(mSpeed < SHAKE_THRESHOLD - 1000) {
					mCount = 0;
				}
				
				if(mCount >= SHAKE_SENSOR_COUNT) {
					mCount = 0;
					unregisteSensor(mSensorManager, this);
					FortuneStarDialog fortuneStarDialog = new FortuneStarDialog(mContext);
					fortuneStarDialog.show();
					
					fortuneStarDialog.setOnDismissListener(this);
				}
				
				mLastX = event.values[SensorManager.DATA_X];
				mLastY = event.values[SensorManager.DATA_Y];
				mLastZ = event.values[SensorManager.DATA_Z];
			}
		}
	}
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}

	public void onDismiss(DialogInterface dialog) {
		registeSensor(mSensorManager, this, mAccelSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}















