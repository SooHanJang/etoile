package madcat.studio.main;

import java.util.Locale;

import madcat.studio.destiny.DestinyJob;
import madcat.studio.destiny.DestinyLife;
import madcat.studio.destiny.DestinyLove;
import madcat.studio.destiny.DestinyPersonality;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class Destiny extends Activity {

	private ImageButton mPersonality, mLife, mJob, mLove;
	
	private ImageView mShootingStar1;
	private ImageView mShootingStar2;
	private ImageView mShootingStar3;
	
	private String mLocale;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mLocale = getResources().getConfiguration().locale.getLanguage();
		if(mLocale.equals(Locale.KOREAN.toString())) {
			setContentView(R.layout.destiny_menu);
		} else {
			setContentView(R.layout.destiny_menu_en);
		}
		
		mPersonality = (ImageButton)findViewById(R.id.menu_destiny_personality);
		mLife = (ImageButton)findViewById(R.id.menu_destiny_life);
		mJob = (ImageButton)findViewById(R.id.menu_destiny_job);
		mLove = (ImageButton)findViewById(R.id.menu_destiny_love);
		
		mShootingStar1 = (ImageView)findViewById(R.id.shooting_star1);
        mShootingStar2 = (ImageView)findViewById(R.id.shooting_star2);
        mShootingStar3 = (ImageView)findViewById(R.id.shooting_star3);
        
        mShootingStar1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star1));
        mShootingStar2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star2));
        mShootingStar3.startAnimation(AnimationUtils.loadAnimation(this, R.anim.main_menu_shooting_star3));

		mPersonality.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Destiny.this, DestinyPersonality.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mLife.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Destiny.this, DestinyLife.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mJob.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Destiny.this, DestinyJob.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mLove.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Destiny.this, DestinyLove.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
