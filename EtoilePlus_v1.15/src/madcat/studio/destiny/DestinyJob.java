package madcat.studio.destiny;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class DestinyJob extends Activity {
	
	private final String TAG										=	"DestinyJob";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
//	private int DISPLAY_WIDTH, DISPLAY_HEIGHT;
	
	private Context mContext;
	
	private TextView mJobTextView;
	private ScrollView mScrollView;
	private TextView mJobTitle, mRecentPageText, mTotalPageText;
	private ViewFlipper mViewFlipper;
	
	private LinearLayout mBtnLayout;
	private ImageButton mPrevBtn;
	private ImageButton mNextBtn;
	
	private int mCount, mTempCount;
	private int mTotalPage;
	private String[] mJobResultText;
	
	private SharedPreferences mConfigPref, mIPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.destiny_job_result);
		
		// Context 초기화
		mContext = this;
		
		// 환경설정 초기화
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mIPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		float fontSize = (float)mConfigPref.getInt(Constants.CONFIG_FONT_SIZE, 3);
		
		// Resource 초기화
		mBtnLayout = (LinearLayout)findViewById(R.id.destiny_job_prev_next_button);
		mPrevBtn = (ImageButton)findViewById(R.id.destiny_job_prev_btn);
		mNextBtn = (ImageButton)findViewById(R.id.destiny_job_next_btn);
		mJobTextView = (TextView)findViewById(R.id.destiny_job_result_text);
		mScrollView = (ScrollView)findViewById(R.id.destiny_job_scroll_view);
		mJobTitle = (TextView)findViewById(R.id.destiny_job_result_title);
		mRecentPageText = (TextView)findViewById(R.id.destiny_job_recent_page_number);
		mTotalPageText = (TextView)findViewById(R.id.destiny_job_total_page_number);
		mViewFlipper = (ViewFlipper)findViewById(R.id.destiny_job_flipper);
		
		// ViewFlipper 초기화
		mJobTitle.setBackgroundColor(0);
		mJobTextView.setBackgroundColor(0);
		mJobTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE); 
		
//		DISPLAY_WIDTH = getResources().getDisplayMetrics().widthPixels;
//      DISPLAY_HEIGHT = getResources().getDisplayMetrics().heightPixels;
        
//      RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)(DISPLAY_WIDTH * 0.71 + 45), (int)(DISPLAY_HEIGHT * 0.78 - 5));
//      params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//		mViewFlipper.setLayoutParams(params);
		
		// 내 별자리에 따른 직업 텍스트 가져오기
		mJobResultText = Util.getJobText(mContext, 
				Util.getEastStarPosition(mIPref.getInt(Constants.USER_LUNAR_MONTH, 0), mIPref.getInt(Constants.USER_LUNAR_DAY, 0)));

		if(DEVELOPE_MODE) {
			Log.d(TAG, "lunar month : " + mIPref.getInt(Constants.USER_LUNAR_MONTH, 0));
			Log.d(TAG, "lunar day : " + mIPref.getInt(Constants.USER_LUNAR_DAY, 0));
			Log.d(TAG, "getEastStarPosition : " + Util.getEastStarPosition(mIPref.getInt(Constants.USER_LUNAR_MONTH, 0), mIPref.getInt(Constants.USER_LUNAR_DAY, 0)));
		}
		
		
		// 타이틀 설정
		mJobTitle.setText(mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_fortune_job));
		
		// 글꼴 설정
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		mJobTitle.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mJobTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mRecentPageText.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mTotalPageText.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		
		// 하단에 표시할 페이지 설정
		mCount = 0;
		mTotalPage = mJobResultText.length;
		
		WindowManager windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
		if (windowManager.getDefaultDisplay().getWidth() >= 720) {
			String jobText = mJobResultText[0];
			for (int i = 1; i < mTotalPage; i++) {
				jobText += "\n\n" + mJobResultText[i];
			}
			
			mJobTextView.setText(jobText);
			mJobTitle.setVisibility(View.VISIBLE);
			mBtnLayout.setVisibility(View.GONE);
		} else {
			if(mCount == 0) {		// 첫 페이지 표시 설정
				mJobTextView.setText(mJobResultText[mCount]);
				mJobTitle.setVisibility(View.VISIBLE);
				mPrevBtn.setVisibility(View.GONE);		// 이미 첫장이므로 전 장을 넘길 수 있는 버튼은 표시 안함
				mNextBtn.setVisibility(View.VISIBLE);		// 다음 장을 넘길 수 있는 버튼 만 표시
				mRecentPageText.setText(mCount+1 + "");		// 첫장이라고 표시
				mTotalPageText.setText(mTotalPage + "");		// 마지막 장이 몇페이지인지 표시
			} 
			
			mTempCount = mCount + 1;
			
			mNextBtn.setOnClickListener(new View.OnClickListener() {		// 다음 페이지 버튼을 눌렀다면
				public void onClick(View v) {
					mScrollView.scrollTo(0, 0);		//  페이지 전환시 스크롤을 처음 위치로 설정
					mViewFlipper.startFlipping();
					mTempCount++;
					
//					Log.d(TAG, "next count : " + mTempCount);
					
					if(mTempCount == mTotalPage) {		
						Toast.makeText(getApplicationContext(), getString(R.string.toast_last_page), Toast.LENGTH_SHORT).show();		// 마지막 장이라고 알려준다.
						mJobTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);		// 이전 버튼을 표시
						mNextBtn.setVisibility(View.GONE);		// 다음 장 버튼을  잠시 사라지게 한다.
						mJobTextView.setText(mJobResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					} else if (mTempCount < mTotalPage && mTempCount != mTotalPage) {		// 아직 최종 장에 도달하지 못했다면
						mJobTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);		// 이전 버튼을 표시
						mNextBtn.setVisibility(View.VISIBLE);		// 다음 버튼을 표시
						mJobTextView.setText(mJobResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					}
					mViewFlipper.stopFlipping();
				}
			});
			
			mPrevBtn.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mScrollView.scrollTo(0, 0);		//  페이지 전환시 스크롤을 처음 위치로 설정
					mViewFlipper.startFlipping();
					mTempCount--;
//					Log.d(TAG, "prev count : " + mTempCount);
					
					if(mTempCount == 1) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_first_page), Toast.LENGTH_SHORT).show();		// 처음 장이라고 알려준다.
						mJobTitle.setVisibility(View.VISIBLE);
						mPrevBtn.setVisibility(View.GONE);
						mNextBtn.setVisibility(View.VISIBLE);
						mJobTextView.setText(mJobResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					} else if (mTempCount < mTotalPage && mTempCount != 1) {		// 아직 최종 장에 도달하지 못했다면
						mJobTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);
						mNextBtn.setVisibility(View.VISIBLE);
						mJobTextView.setText(mJobResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					}
					mViewFlipper.stopFlipping();
				}
			});
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
