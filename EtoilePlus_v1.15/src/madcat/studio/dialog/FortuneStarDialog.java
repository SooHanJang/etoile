package madcat.studio.dialog;

import java.util.Random;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.plus.R.color;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class FortuneStarDialog extends Dialog {

	private final String TAG										=	"FortuneStarDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final int RANDOM_MAX_NUMBER								=	10;
	private final int RANDOM_MAX_LOTTO_NUMBER						=	45;
	
	private Context mContext;
	
	private TextView mTextFortuneNumber, mTextFortuneColor, mTextFortunePlace, mTextFortuneLotto;
	private SharedPreferences mConfigPref;
	
	
	public FortuneStarDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_fortune_star);
		
		// Init sharedPreference
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		// Init resource id
		mTextFortuneNumber = (TextView)findViewById(R.id.dialog_fortune_star_number);
		mTextFortuneColor = (TextView)findViewById(R.id.dialog_fortune_star_color);
		mTextFortunePlace = (TextView)findViewById(R.id.dialog_fortune_star_place);
		mTextFortuneLotto = (TextView)findViewById(R.id.dialog_fortune_star_lotto_number);
		
		String[] todayColor = mContext.getResources().getStringArray(R.array.fortune_color_name);
		String[] todayPlace = mContext.getResources().getStringArray(R.array.fortune_place_name);
		
		// 포춘 스타 변수
		int randFortuneNumber, randFortuneColorNumber, randFortunePlaceNumber;
		int[] fortuneLottoNumber;
		String lottoText;
		
		// 포춘 스타를 처음 실행
		if(!mConfigPref.getBoolean(Constants.CONFIG_CHECK_FORTUNE_STAR, false)) {
			// 랜덤 값 설정
			randFortuneNumber = getRandomNumber(RANDOM_MAX_NUMBER)+1;
			randFortuneColorNumber = getRandomNumber(todayColor.length);
			randFortunePlaceNumber = getRandomNumber(todayPlace.length);
			fortuneLottoNumber = getLottoNumber();
			
			lottoText = fortuneLottoNumber[0] + " " + fortuneLottoNumber[1] + " " + fortuneLottoNumber[2] + " " + fortuneLottoNumber[3] + " " +
						fortuneLottoNumber[4] + " " + fortuneLottoNumber[5] + " (" + mContext.getString(R.string.dialog_fortune_star_today_bonus) + fortuneLottoNumber[6] + ")";
			
			// 설정된 랜덤 값과 로또 번호 String 을 저장
			SharedPreferences.Editor configEditor = mConfigPref.edit();
			configEditor.putInt(Constants.CONFIG_FORTUNE_STAR_NUMBER, randFortuneNumber);
			configEditor.putInt(Constants.CONFIG_FORTUNE_STAR_COLOR, randFortuneColorNumber);
			configEditor.putInt(Constants.CONFIG_FORTUNE_STAR_PLACE, randFortunePlaceNumber);
			configEditor.putString(Constants.CONFIG_FORTUNE_STAR_LOTTO, lottoText); 
			configEditor.putBoolean(Constants.CONFIG_CHECK_FORTUNE_STAR, true);
			configEditor.commit();
			
		} else {
			randFortuneNumber = mConfigPref.getInt(Constants.CONFIG_FORTUNE_STAR_NUMBER, 0);
			randFortuneColorNumber = mConfigPref.getInt(Constants.CONFIG_FORTUNE_STAR_COLOR, 0);
			randFortunePlaceNumber = mConfigPref.getInt(Constants.CONFIG_FORTUNE_STAR_PLACE, 0);
			lottoText = mConfigPref.getString(Constants.CONFIG_FORTUNE_STAR_LOTTO, "");
			
		}
		
		
		// Text 값 설정
		mTextFortuneNumber.setText(String.valueOf(randFortuneColorNumber));
		mTextFortuneColor.setText(todayColor[randFortuneColorNumber]);
		
		int colorResId = mContext.getResources().getIdentifier(todayColor[randFortuneColorNumber], "color", mContext.getPackageName());
		mTextFortuneColor.setBackgroundResource(colorResId);
		
		mTextFortunePlace.setText(todayPlace[randFortunePlaceNumber]);
		mTextFortuneLotto.setText(lottoText);
		
	}
	
	private int getRandomNumber(int range) {
		Random random = new Random();
		int randomNumber = 0;
		random.setSeed(System.currentTimeMillis());		// 랜덤 시드를 현재 시간으로 설정
		
		randomNumber = random.nextInt(range);
		return randomNumber;			
	}
	
	private int[] getLottoNumber() {
		
		int[] lottoNumber = new int[7];
		Random random = new Random();
		random.setSeed(System.currentTimeMillis());
		
		int randomNumber = 0;
		int count = 0;
		
		return calculateLottoNumber(lottoNumber, random, count);
	}
	
	private int[] calculateLottoNumber(int[] lottoNumber, Random random, int count) {
		
		int randNum = 0;
		
		if(count == 0) {
			randNum = random.nextInt(RANDOM_MAX_LOTTO_NUMBER) + 1;
			lottoNumber[count] = randNum;
			count++;
			return calculateLottoNumber(lottoNumber, random, count);
		} else if(count > 0 && count <= 6) {
			randNum = random.nextInt(RANDOM_MAX_LOTTO_NUMBER) + 1;
			
			for(int i=0; i < count; i++) {
				if(randNum == lottoNumber[i]) {
					return calculateLottoNumber(lottoNumber, random, count);
				}
			}
			
			lottoNumber[count] = randNum;
			count++;
			return calculateLottoNumber(lottoNumber, random, count);
		} else {
			return lottoNumber;
		}
	}
}













