package madcat.studio.dialog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import madcat.studio.friends.FriendsTodayFortune;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class VoiceResultActivityDialog extends Activity {

	private final String TAG										=	"VoiceResultActivityDialog";
	private final int VOICE_REQUEST_CODE							=	150;
	
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mContext = this;
		
		showSpeechDialog();
	}
	
	private void showSpeechDialog() {
    	Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);    	
	   	intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
	   				RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
	   	intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "운세 음성 인식");		
	   	intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 20);
	   	intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
	   	startActivityForResult(intent, VOICE_REQUEST_CODE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(requestCode == VOICE_REQUEST_CODE) {
			if(data != null) {
	    		ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
	    		
	    		final ArrayList<String> voiceItems = new ArrayList<String>();
	    		
	    		for(int i=0; i < results.size(); i++) {
	    			
	    			if(results.get(i).length() >= 9) {
	    				String dateItem = Util.getSplitVoiceFortune(results.get(i).trim().toString().replaceAll("\\p{Space}", "")); 
	    				
	    				if(dateItem != null) {
	    					voiceItems.add(dateItem);
	    				}
	    			}
	    		}
	    		
	    		// 문자열 중복 제거
	    		HashSet<String> hs = new HashSet<String>(voiceItems);
	    		ArrayList<String> duplicateArray = new ArrayList<String>(hs);
	    		
	    		// 중복 제거한 값을 FortuneDate 로 변환 후 ArrayList 에 넣기
	    		final ArrayList<FortuneDate> voiceDateArray = new ArrayList<FortuneDate>();
	    		
	    		for(int i=0; i < duplicateArray.size(); i++) {
	    			String[] splitArray = duplicateArray.get(i).split(",");
	    			FortuneDate fortuneItem = new FortuneDate();
	    			fortuneItem.setYear(Integer.parseInt(splitArray[0]));
	    			fortuneItem.setMonth(Integer.parseInt(splitArray[1]));
	    			fortuneItem.setDay(Integer.parseInt(splitArray[2]));
	    			voiceDateArray.add(fortuneItem);
	    		}
	    		
	    		
	    		if(voiceDateArray.size() != 0) {
		    		final CharSequence[] items = new CharSequence[voiceDateArray.size()];
		
		    		for(int i=0; i < voiceDateArray.size(); i++) {
		    			String dateFormat = voiceDateArray.get(i).getYear() + "년 " + voiceDateArray.get(i).getMonth() + "월 " + voiceDateArray.get(i).getDay() + "일";
		    			items[i] = dateFormat;
		    		}
		    		
		    		
		    		AlertDialog.Builder builder = new Builder(mContext);
		    		builder.setTitle("생년월일을 선택해 주세요.").setItems(items, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// 사용자가 선택한 생년월일을 파라미터로 하여, 오늘의 운세를 실행한다.
							StartIntentAsync startIntentAsync = 
								new StartIntentAsync(voiceDateArray.get(which), items[which].toString());
							startIntentAsync.execute();
							
						}
					}).setOnCancelListener(new OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							finish();
						}
					}).show();
	    		} else {
	    			Toast.makeText(mContext, "다시 한번 말씀해 주세요.", Toast.LENGTH_SHORT).show();
	    			finish();
	    		}
			} else {
				finish();
			}
    	}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	class StartIntentAsync extends AsyncTask<Void, Void, Void> {
		
		ProgressDialog progressDialog;
		FortuneDate item, lunarDate;
		String name;
		
		public StartIntentAsync(FortuneDate item, String name) {
			this.item = item;
			this.name = name;
		}
		
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", "잠시만 기다려주세요...");
		}

		@Override
		protected Void doInBackground(Void... params) {
			lunarDate = Util.getLunarDate(item.getYear(), item.getMonth(), item.getDay());
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			Intent intent = new Intent(VoiceResultActivityDialog.this, FriendsTodayFortune.class);
			intent.putExtra("FRIENDS_NAME", name);
			intent.putExtra("FRIENDS_LUNAR_MONTH", lunarDate.getMonth());
			intent.putExtra("FRIENDS_LUNAR_DAY", lunarDate.getDay());
			startActivity(intent);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			finish();
		}
	}
}














