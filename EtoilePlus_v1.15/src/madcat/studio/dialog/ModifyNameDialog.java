package madcat.studio.dialog;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ModifyNameDialog extends Dialog implements OnClickListener {
	
	private Context mContext;
	
	private EditText mEditName;
	private Button mBtnOk, mBtnCancel;
	
	private SharedPreferences mUserPref;
	private boolean mFlag;
	
	public ModifyNameDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_name_modify_dialog);
		setTitle(mContext.getResources().getString(R.string.setting_modify_name));
		
		mUserPref = mContext.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFlag = false;
		
		mEditName = (EditText)findViewById(R.id.editText_name);
		mBtnOk = (Button) findViewById(R.id.okButton_name);
		mBtnCancel = (Button) findViewById(R.id.cancleButton_name);
		
		mEditName.setText(mUserPref.getString(Constants.USER_NAME, ""));
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.okButton_name:
				if (!mEditName.getText().toString().trim().equals("")) {
					mFlag = true;
					
					SharedPreferences.Editor prefEditor = mUserPref.edit();
					prefEditor.putString(Constants.USER_NAME, mEditName.getText().toString());
					prefEditor.commit();
	
					dismiss();
				}
				
				break;
			case R.id.cancleButton_name:
				mFlag = false;
				dismiss();
				break;
			default:
				break;
		}
	}
	
	public boolean getDialogFlag() {
		return mFlag;
	}

}
