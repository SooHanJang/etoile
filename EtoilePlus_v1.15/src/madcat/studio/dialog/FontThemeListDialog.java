package madcat.studio.dialog;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class FontThemeListDialog extends Dialog implements OnClickListener, OnItemClickListener {
	
	// 반드시 Constants 와 인덱스 순서를 맞출것. 변경 시 Util.getFontFileName() 인덱스 순서를 수정할 것.
	private boolean mDialogFlag										=	false;
	private Context mContext;

	private SharedPreferences mPref;
	private ListView mFontThemeList;
	private Button mBtnOk, mBtnCancel;
	private TextView mFontText;
	private int mThemePosition;
	
	public FontThemeListDialog(Context context) {
		super(context);
		mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_font_theme_list);
		setTitle(mContext.getString(R.string.dialog_title_select_font_theme));
		
		// 기본 변수 설정
		mPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontThemeList = (ListView)findViewById(android.R.id.list);
		mFontText = (TextView)findViewById(R.id.font_theme_text);
		mBtnOk = (Button)findViewById(R.id.font_theme_list_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.font_theme_list_btn_cancel);
		
		String[] fontItems = {mContext.getString(R.string.setting_font_default_font), mContext.getString(R.string.setting_font_tricycle_font),
				mContext.getString(R.string.setting_font_seoul_namsan_font), mContext.getString(R.string.setting_font_daum_font) };
		
		// 어댑터 설정
		mFontThemeList.setAdapter(new ArrayAdapter<String>(mContext, R.layout.theme_widget_list_item, android.R.id.text1, fontItems));
		mFontThemeList.setItemsCanFocus(false);
		mFontThemeList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mFontThemeList.setOnItemClickListener(this);
		
		// 리스너 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 기본 설정 불러오기
		mThemePosition = mPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0);
		mFontThemeList.setItemChecked(mThemePosition, true);
		
		// 폰트의 인덱스에 따른 파일 이름을 가져와서 경로를 설정한다.
		String fontPath = Constants.FONT_PREFIX_PATH + Util.getFontFileName(mThemePosition) + Constants.FONT_SUFFIX_TTF;

		// 경로 설정이 끝나면, 해당 폰트를 Assets 에서 직접 불러와 폰트를 적용할 View 에 적용한다.
		mFontText.setTypeface(Util.getFontType(mContext, fontPath, mThemePosition));
		
	}

	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		String fontPath = Constants.FONT_PREFIX_PATH + Util.getFontFileName(position) + Constants.FONT_SUFFIX_TTF;
		mFontText.setTypeface(Util.getFontType(mContext, fontPath, position));		
		
		mThemePosition = position;
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.font_theme_list_btn_ok:
				SharedPreferences.Editor editor = mPref.edit();
				editor.putInt(Constants.CONFIG_FONT_THEME_ITEM, mThemePosition);
				editor.commit();
				
				mDialogFlag = true;
				
				Util.sendBroadCasting(mContext, Constants.ACTION_WIDGET_UPDATE_THEME);
				dismiss();
				
				break;
			case R.id.font_theme_list_btn_cancel:
				mDialogFlag = false;
				dismiss();
				break;
			default:
				break;
		}
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}
}
