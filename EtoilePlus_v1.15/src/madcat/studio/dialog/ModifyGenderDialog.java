package madcat.studio.dialog;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import android.app.Activity;
import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;

public class ModifyGenderDialog extends Dialog implements OnClickListener{
	
	private final String TAG													=	"ModifyGenderDialog";
	
	public static int RADIO_MAN_SELECT											=	1;
	public static int RADIO_WOMAN_SELECT										=	2;
	
	private Context mContext;
	
	private SharedPreferences mUserPref;
	private RadioButton mRadioMan, mRadioWoman;
	private Button mBtnOk, mBtnCancel; 
	private boolean mFlag;
	
	public ModifyGenderDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_gender_modify_dialog);
		setTitle(mContext.getResources().getString(R.string.setting_choice_sex));
		
		mUserPref = mContext.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFlag = false;
		
		mRadioMan = (RadioButton) findViewById(R.id.manButton_gender);
		mRadioWoman = (RadioButton) findViewById(R.id.womanButton_gender);
		mBtnOk = (Button) findViewById(R.id.okButton_gender);
		mBtnCancel = (Button) findViewById(R.id.cancleButton_gender);
		
		if(mUserPref.getInt(Constants.USER_GENDER, RADIO_MAN_SELECT) == RADIO_MAN_SELECT) {
			mRadioMan.setChecked(true);
		} else {
			mRadioWoman.setChecked(true);
		}
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.okButton_gender:
				mFlag = true;
				
				SharedPreferences.Editor editor = mUserPref.edit();
				
				if(mRadioMan.isChecked()) {
					editor.putInt(Constants.USER_GENDER, RADIO_MAN_SELECT);
				} else {
					editor.putInt(Constants.USER_GENDER, RADIO_WOMAN_SELECT);
				}
				
				editor.commit();
				
				Intent intent = new Intent(AppWidgetManager.ACTION_APPWIDGET_UPDATE, Uri.parse(""));
				mContext.sendBroadcast(intent);
				
				dismiss();
				break;
			case R.id.cancleButton_gender:
				mFlag = false;
				
				dismiss();
				break;
			default:
				break;
		}
	}

	public boolean getDialogFlag() {
		return mFlag;
	}
}
