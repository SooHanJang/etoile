package madcat.studio.dialog;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;

public class ModifyBirthDayTypeDialog extends Dialog implements OnClickListener {

	public static final int RADIO_NOT_MODIFIED										=	0;
	public static final int RADIO_SOLAR_CHECK										=	1;
	public static final int RADIO_LUNAR_CHECK										=	2;
	
	private Context mContext;
	private SharedPreferences mUserPref;
	
	private RadioButton mRadioSolar, mRadioLunar;
	private Button mBtnOk, mBtnCancel;
	
	private int mBirthDayType;
	
	private int mModifyFlag;
	private boolean mDialogFlag;
	
	public ModifyBirthDayTypeDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_birthday_type_modify_dialog);
		setTitle(mContext.getString(R.string.setting_choice_solar_lunar));
		
		mModifyFlag = RADIO_NOT_MODIFIED;
		mDialogFlag = false;
		mUserPref = mContext.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mRadioSolar = (RadioButton)findViewById(R.id.solarButton_solarOrLunar);
		mRadioLunar = (RadioButton)findViewById(R.id.lunarButton_solarOrLunar);
		mBtnOk = (Button)findViewById(R.id.okButton_solarOrLunar);
		mBtnCancel = (Button)findViewById(R.id.cancleButton_solarOrLunar);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		mBirthDayType = mUserPref.getInt(Constants.USER_BIRTH_TYPE, 1);
		
		switch(mBirthDayType) {
			case RADIO_SOLAR_CHECK:
				mRadioSolar.setChecked(true);
				break;
			case RADIO_LUNAR_CHECK:
				mRadioLunar.setChecked(true);
				break;
			default:
				break;
		}
		
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.okButton_solarOrLunar:
				if(mRadioSolar.isChecked()) {
					if(mBirthDayType != RADIO_SOLAR_CHECK) {
						// 음력에서 양력으로 바뀜
						mBirthDayType = RADIO_SOLAR_CHECK;
						mModifyFlag = RADIO_SOLAR_CHECK;
					}
				} else if(mRadioLunar.isChecked()) {
					if(mBirthDayType != RADIO_LUNAR_CHECK) {
						// 양력에서 음력으로 바뀜
						mBirthDayType = RADIO_LUNAR_CHECK;
						mModifyFlag = RADIO_LUNAR_CHECK;
					}
				}
				
				// 바뀐 사용자의 양음력 값을 저장
				SharedPreferences.Editor editor = mUserPref.edit();
				editor.putInt(Constants.USER_BIRTH_TYPE, mBirthDayType);
				editor.commit();
				
				mDialogFlag = true;
				dismiss();
				break;
			case R.id.cancleButton_solarOrLunar:
				mDialogFlag = false;
				dismiss();
				break;
			default:
				break;
		}
	}
	
	public int getModifyFlag() {
		return mModifyFlag;
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}

}










