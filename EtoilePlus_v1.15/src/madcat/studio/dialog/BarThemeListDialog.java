package madcat.studio.dialog;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

public class BarThemeListDialog extends Dialog implements OnClickListener, OnItemClickListener {

	private final String TAG										=	"WidgetthemeListDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private Button mBtnOk, mBtnCancel;
	private ListView mWidgetThemeList;
	private SharedPreferences mPref;
	private ImageView mWidgetThemeImage;
	private int mThemePosition;
	
	private boolean mDialogFlag										=	false;
	
	public BarThemeListDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_bar_theme_list);
		setTitle(mContext.getString(R.string.dialog_title_select_bar_theme));
		
		// 테마 아이템 초기화
		String[] themeItems = {mContext.getString(R.string.pref_title_bar_theme_0), mContext.getString(R.string.pref_title_bar_theme_1),
				mContext.getString(R.string.pref_title_bar_theme_2), mContext.getString(R.string.pref_title_bar_theme_3),
				mContext.getString(R.string.pref_title_bar_theme_4), mContext.getString(R.string.pref_title_bar_theme_5),
				mContext.getString(R.string.pref_title_bar_theme_6), mContext.getString(R.string.pref_title_bar_theme_7),
				mContext.getString(R.string.pref_title_bar_theme_8), mContext.getString(R.string.pref_title_bar_theme_9),
				mContext.getString(R.string.pref_title_bar_theme_10), mContext.getString(R.string.pref_title_bar_theme_11),
				mContext.getString(R.string.pref_title_bar_theme_12), mContext.getString(R.string.pref_title_bar_theme_13),
				mContext.getString(R.string.pref_title_bar_theme_14)};
		

		// 기본 변수 초기화
		mPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mWidgetThemeList = (ListView)findViewById(android.R.id.list);
		mWidgetThemeImage = (ImageView)findViewById(R.id.widget_theme_list_image);
		mBtnOk = (Button)findViewById(R.id.widget_theme_list_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.widget_theme_list_btn_cancle);
		
		// 어댑터 설정
		mWidgetThemeList.setAdapter(new ArrayAdapter<String>(mContext, R.layout.theme_widget_list_item, android.R.id.text1, themeItems));
		mWidgetThemeList.setItemsCanFocus(false);
		mWidgetThemeList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mWidgetThemeList.setOnItemClickListener(this);

		// 리스너 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 기본 값 불러오기
		mThemePosition = mPref.getInt(Constants.CONFIG_WIDGET_BAR_THEME_ITEM, 0);
		
		int resId = mContext.getResources().getIdentifier("widget_background_" + mThemePosition, "drawable", mContext.getPackageName());
		mWidgetThemeList.setItemChecked(mThemePosition, true);
		mWidgetThemeImage.setImageResource(resId);
		
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.widget_theme_list_btn_ok:
				SharedPreferences.Editor editor = mPref.edit();
				editor.putInt(Constants.CONFIG_WIDGET_BAR_THEME_ITEM, mThemePosition);
				editor.commit();
				
				mDialogFlag = true;
				
				Util.sendBroadCasting(mContext, Constants.ACTION_WIDGET_UPDATE_THEME);
				dismiss();
				break;
			case R.id.widget_theme_list_btn_cancle:
				mDialogFlag = false;
				dismiss();
				break;
			default:
				break;
		}
	}

	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		int resId = mContext.getResources().getIdentifier("widget_background_" + position, "drawable", mContext.getPackageName());
		mWidgetThemeImage.setImageResource(resId);
		mThemePosition = position;
	}
}
