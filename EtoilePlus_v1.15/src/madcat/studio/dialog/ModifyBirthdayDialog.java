package madcat.studio.dialog;

import java.util.GregorianCalendar;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class ModifyBirthdayDialog extends Dialog implements OnClickListener, OnItemSelectedListener{
	
	private Context mContext;
	private SharedPreferences mPref;
	private FortuneDate mBirthDay;
	private int mSelectedYear;
	
	private Spinner mSpinYear, mSpinMonth, mSpinDay;
	private Button mBtnOk, mBtnCancel;
	private ArrayAdapter<CharSequence> mAdapterYear, mAdapterMonth, mAdapterDay;
	
	private boolean mDialogFlag, mCorrectFlag;
	
	public ModifyBirthdayDialog(Context context, FortuneDate birthDay) {
		super(context);
		mContext = context;
		mBirthDay = birthDay;
	}

	public boolean getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_birthday_modify_dialog);
		setTitle(mContext.getString(R.string.setting_choice_birthday));
		
		mPref = mContext.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mSelectedYear = mBirthDay.getYear();
		mCorrectFlag = false;
		
		mSpinYear = (Spinner)findViewById(R.id.setting_spin_year);
		mSpinMonth = (Spinner)findViewById(R.id.setting_spin_month);
		mSpinDay = (Spinner)findViewById(R.id.setting_spin_day);
		
		mBtnOk = (Button)findViewById(R.id.setting_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.setting_btn_cancle);
		
		mAdapterYear = ArrayAdapter.createFromResource(mContext, R.array.year, android.R.layout.simple_spinner_item);
        mAdapterMonth = ArrayAdapter.createFromResource(mContext, R.array.month, android.R.layout.simple_spinner_item);
	        
        mAdapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAdapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        mSpinYear.setAdapter(mAdapterYear);
        mSpinMonth.setAdapter(mAdapterMonth);
        
        if(mBirthDay.getMonth()==1 || mBirthDay.getMonth()==3 || mBirthDay.getMonth()==5 || mBirthDay.getMonth()==7 || mBirthDay.getMonth()==8 || mBirthDay.getMonth()==10 || mBirthDay.getMonth()==12) {
			mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_odd, android.R.layout.simple_spinner_item);
			mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinDay.setAdapter(mAdapterDay);
		} else if(mBirthDay.getMonth()==2) {
			if(new GregorianCalendar().isLeapYear(mBirthDay.getYear())) {	// 윤년이면 2월은 29일
				mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_leap_true, android.R.layout.simple_spinner_item);
				mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				mSpinDay.setAdapter(mAdapterDay);
			} else {		// 윤년이 아니면 2월은 28일
				mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_leap_false, android.R.layout.simple_spinner_item);
				 mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				mSpinDay.setAdapter(mAdapterDay);
			}
		} else {
			mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_even, android.R.layout.simple_spinner_item);
			mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mSpinDay.setAdapter(mAdapterDay);
		}
        
        mSpinYear.setSelection(mAdapterYear.getPosition(mBirthDay.getYear()+""));
        mSpinMonth.setSelection(mAdapterMonth.getPosition(mBirthDay.getMonth()+""));
        mSpinDay.setSelection(mAdapterDay.getPosition(mBirthDay.getDay()+""));
        
        mBtnOk.setOnClickListener(this);
        mBtnCancel.setOnClickListener(this);
        mSpinYear.setOnItemSelectedListener(this);
        mSpinMonth.setOnItemSelectedListener(this);
        mSpinDay.setOnItemSelectedListener(this);
		
		
	}

	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		switch(parent.getId()) {
		case R.id.setting_spin_year:		// 년도가 클릭 되면 수행
			mSelectedYear = Integer.parseInt(mSpinYear.getSelectedItem().toString());
			break;
		case R.id.setting_spin_month:		// 월이 클릭 되면 수행
			int select_month = Integer.parseInt(mSpinMonth.getSelectedItem().toString());
			if(select_month==1 || select_month==3 || select_month==5 || select_month==7 || select_month==8 || select_month==10 || select_month==12) {
				mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_odd, android.R.layout.simple_spinner_item);
				mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				mSpinDay.setAdapter(mAdapterDay);
			} else if(select_month==2) {
				if(new GregorianCalendar().isLeapYear(mSelectedYear)) {	// 윤년이면 2월은 29일
					mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_leap_true, android.R.layout.simple_spinner_item);
					mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mSpinDay.setAdapter(mAdapterDay);
				} else {		// 윤년이 아니면 2월은 28일
					mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_leap_false, android.R.layout.simple_spinner_item);
					mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					mSpinDay.setAdapter(mAdapterDay);
				}
			} else {
				mAdapterDay = ArrayAdapter.createFromResource(mContext, R.array.day_even, android.R.layout.simple_spinner_item);
				mAdapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				mSpinDay.setAdapter(mAdapterDay);
			}
			break;
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.setting_btn_ok:
				FortuneDate solarDate = null;
				FortuneDate lunarDate = null;
				int birthDayType = mPref.getInt(Constants.USER_BIRTH_TYPE, 1);
				
				SharedPreferences.Editor editor = mPref.edit();
				
				mBirthDay.setYear(Integer.parseInt(mSpinYear.getSelectedItem().toString()));
				mBirthDay.setMonth(Integer.parseInt(mSpinMonth.getSelectedItem().toString()));
				mBirthDay.setDay(Integer.parseInt(mSpinDay.getSelectedItem().toString()));
				
				switch (birthDayType) {
					case 1://양력
						solarDate = new FortuneDate(mBirthDay.getYear(), mBirthDay.getMonth(), mBirthDay.getDay());
						lunarDate = Util.getLunarDate(mBirthDay.getYear(), mBirthDay.getMonth(), mBirthDay.getDay());
						mCorrectFlag = true;
						break;
					case 2://음력
						if(!Util.emptyLunarDateCheck(mBirthDay.getYear(), mBirthDay.getMonth(), mBirthDay.getDay())) {
							mCorrectFlag = false;
							break;
						}
						
						solarDate = Util.getSolarDate(mBirthDay.getYear(), mBirthDay.getMonth(), mBirthDay.getDay());
						lunarDate = new FortuneDate(mBirthDay.getYear(), mBirthDay.getMonth(), mBirthDay.getDay());
						mCorrectFlag = true;
						break;
				}
				
				if(mCorrectFlag) {
					Util.setUserBirthDay(mContext, solarDate, lunarDate);
				} else {
					Toast.makeText(mContext, mContext.getString(R.string.toast_caution_not_lunar_day), Toast.LENGTH_SHORT).show();
				}
				
				mDialogFlag = true;
				dismiss();
				break;
			case R.id.setting_btn_cancle:
				mDialogFlag = false;
				dismiss();
			default:
				break;
		}
	}
	
	public FortuneDate getBirthDay() {
		return mBirthDay;
	}

}
