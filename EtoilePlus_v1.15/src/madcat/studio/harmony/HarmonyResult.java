package madcat.studio.harmony;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import madcat.studio.adapter.SNSAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.facebook.FaceBookUtil;
import madcat.studio.fortune.FaceBookLogin;
import madcat.studio.fortune.SNSWebView;
import madcat.studio.plus.R;
import madcat.studio.utils.KakaoLink;
import madcat.studio.utils.Util;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.http.AccessToken;
import twitter4j.http.OAuthAuthorization;
import twitter4j.http.RequestToken;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class HarmonyResult extends Activity{
//	private static final String TAG = "Harmony Result";
	
//	private int DISPLAY_WIDTH, DISPLAY_HEIGHT;
	
	private TextView mHarmonyResultTitle, mRecentPageText, mTotalPageText;
	private TextView mHarmonyResultTextView;
	private ScrollView mScrollView;
	private LinearLayout mBtnLayout;
	private ImageButton mPrevBtn, mNextBtn;
	private ViewFlipper mViewFlipper;
	
	private String[] mHarmonyResultText, mHarmonyResultWebString;
	private String mHarmonySummary;
	
	private int mFlag;
	private String mHarmonyResult;
	
	private int mCount, mTempCount;
	private int mTotalPage;
	
	private Context mContext;
	
	private SharedPreferences mConfigPref, mIPref, mUPref;
	
	// SNS 변수
	private Twitter mTwitter;
	private AccessToken mAccessToken;
	private RequestToken mRequestToken;
	private FaceBookLogin mFaceBookLogin;
	
	// Kakaotalk 변수
	private KakaoLink mLink;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.harmony_result);
		
		getIntenter();
		
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mIPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mUPref = getSharedPreferences(Constants.MATE_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		float fontSize = (float)mConfigPref.getInt(Constants.CONFIG_FONT_SIZE, 3);
		
		mHarmonyResultTitle = (TextView)findViewById(R.id.harmony_result_title);
		mHarmonyResultTextView = (TextView)findViewById(R.id.harmony_result_text);
		mScrollView = (ScrollView)findViewById(R.id.harmony_scroll_view);
		mBtnLayout = (LinearLayout)findViewById(R.id.prev_next_button);
		mPrevBtn = (ImageButton)findViewById(R.id.prev_btn);
		mNextBtn = (ImageButton)findViewById(R.id.next_btn);
		mRecentPageText = (TextView)findViewById(R.id.recent_page_number);
		mTotalPageText = (TextView)findViewById(R.id.total_page_number);
		mViewFlipper = (ViewFlipper)findViewById(R.id.harmony_flipper);
		
		mHarmonyResultTitle.setBackgroundColor(0);
		mHarmonyResultTextView.setBackgroundColor(0);
		mHarmonyResultTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE); 
		
		// 글꼴 설정
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
					Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		mHarmonyResultTitle.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mHarmonyResultTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mRecentPageText.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mTotalPageText.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		
//		DISPLAY_WIDTH = getResources().getDisplayMetrics().widthPixels;
//      DISPLAY_HEIGHT = getResources().getDisplayMetrics().heightPixels;
//        
//      RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)(DISPLAY_WIDTH * 0.71 + 45), (int)(DISPLAY_HEIGHT * 0.78 - 5));
//      params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
		
//		mViewFlipper.setLayoutParams(params);
		
		mContext = this;
		
		if(mFlag == 1) {		// 나에게 너는...
			mHarmonyResult = Util.getEastStarHarmony(mIPref.getInt(Constants.USER_EAST_STAR_POSITION, -1), mUPref.getInt(Constants.MATE_EAST_STAR_POSITION, -1));
			mHarmonyResultTitle.setText(mIPref.getString(Constants.USER_NAME, "") + 
					getString(R.string.suffix_me) + mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_you));
		} else if(mFlag == 2) {		// 너에게 나는...
			mHarmonyResult = Util.getEastStarHarmony(mUPref.getInt(Constants.MATE_EAST_STAR_POSITION, -1), mIPref.getInt(Constants.USER_EAST_STAR_POSITION, -1));
			mHarmonyResultTitle.setText(mUPref.getString(Constants.MATE_NAME, "") + 
					getString(R.string.suffix_me) + mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_you));
		}
		
//		FrameLayout.LayoutParams setScrollMargin = (FrameLayout.LayoutParams) mScrollView.getLayoutParams();
//		setScrollMargin.setMargins(Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN);
//		setScrollMargin.setMargins(0, 0, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN);
//		mScrollView.setLayoutParams(setScrollMargin);
		
//		Log.d(TAG, "FONT SIZE : " + fontSize);
		
		mHarmonyResultText = Util.getHarmonyResultText(mContext, mHarmonyResult);
		mHarmonySummary = Util.getHarmonySummaryText(mContext, mHarmonyResult);
		
//		Log.d(TAG, "결과 : " + mHarmonySummary);
		
		mCount = 0;
		mTotalPage = mHarmonyResultText.length;
		
		WindowManager windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
		if (windowManager.getDefaultDisplay().getWidth() >= 720) {
			String harmonyText = mHarmonyResultText[0];
			for (int i = 1; i < mTotalPage; i++) {
				harmonyText += "\n\n" + mHarmonyResultText[i];
			}
			
			mHarmonyResultTextView.setText(harmonyText);
			mHarmonyResultTitle.setVisibility(View.VISIBLE);
			mBtnLayout.setVisibility(View.GONE);
		} else {
			if(mCount == 0) {		// 첫 페이지 표시 설정
				mHarmonyResultTextView.setText(mHarmonyResultText[mCount]);
				mHarmonyResultTitle.setVisibility(View.VISIBLE);
				mPrevBtn.setVisibility(View.GONE);		// 이미 첫장이므로 전 장을 넘길 수 있는 버튼은 표시 안함
				mNextBtn.setVisibility(View.VISIBLE);		// 다음 장을 넘길 수 있는 버튼 만 표시
				mRecentPageText.setText(mCount+1 + "");		// 첫장이라고 표시
				mTotalPageText.setText(mTotalPage + "");		// 마지막 장이 몇페이지인지 표시
			} 
			
			mTempCount = mCount + 1;
			
//			Log.d(TAG, "mTempCount : " + mTempCount);
			
			mNextBtn.setOnClickListener(new View.OnClickListener() {		// 다음 페이지 버튼을 눌렀다면
				public void onClick(View v) {
					mScrollView.scrollTo(0, 0);		//  페이지 전환시 스크롤을 처음 위치로 설정
					mViewFlipper.startFlipping();
					mTempCount++;
					
//					Log.d(TAG, "next count : " + mTempCount);
					
					if(mTempCount == mTotalPage) {		
						Toast.makeText(getApplicationContext(), getString(R.string.toast_last_page), Toast.LENGTH_SHORT).show();		// 마지막 장이라고 알려준다.
						mHarmonyResultTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);		// 이전 버튼을 표시
						mNextBtn.setVisibility(View.GONE);		// 다음 장 버튼을  잠시 사라지게 한다.
						mHarmonyResultTextView.setText(mHarmonyResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					} else if (mTempCount < mTotalPage && mTempCount != mTotalPage) {		// 아직 최종 장에 도달하지 못했다면
						mHarmonyResultTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);		// 이전 버튼을 표시
						mNextBtn.setVisibility(View.VISIBLE);		// 다음 버튼을 표시
						mHarmonyResultTextView.setText(mHarmonyResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					}
					mViewFlipper.stopFlipping();
				}
			});
			
			mPrevBtn.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					mScrollView.scrollTo(0, 0);		//  페이지 전환시 스크롤을 처음 위치로 설정
					mViewFlipper.startFlipping();
					mTempCount--;
//					Log.d(TAG, "prev count : " + mTempCount);
					
					if(mTempCount == 1) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_first_page), Toast.LENGTH_SHORT).show();		// 처음 장이라고 알려준다.
						mHarmonyResultTitle.setVisibility(View.VISIBLE);
						mPrevBtn.setVisibility(View.GONE);
						mNextBtn.setVisibility(View.VISIBLE);
						mHarmonyResultTextView.setText(mHarmonyResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					} else if (mTempCount < mTotalPage && mTempCount != 1) {		// 아직 최종 장에 도달하지 못했다면
						mHarmonyResultTitle.setVisibility(View.GONE);
						mPrevBtn.setVisibility(View.VISIBLE);
						mNextBtn.setVisibility(View.VISIBLE);
						mHarmonyResultTextView.setText(mHarmonyResultText[mTempCount-1]);
						mRecentPageText.setText(mTempCount + "");
					}
					mViewFlipper.stopFlipping();
				}
			});
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		
		MenuItem item = menu.add(0, Constants.MENU_UPLOAD_SNS, 0, getString(R.string.menu_item_sns));
		item.setIcon(R.drawable.post_icon);
		MenuItem item_1 = menu.add(0, Constants.MENU_SEND_SMS, 0, getString(R.string.menu_item_sms));
		item_1.setIcon(R.drawable.mail_icon);
		MenuItem item_2 = menu.add(0, Constants.MENU_SEND_KAKAO, 0, getString(R.string.menu_item_kakaotalk));
		item_2.setIcon(R.drawable.kakaotalkicon_3535);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
		case Constants.MENU_UPLOAD_SNS:
			showUploadSNSDialog();
			break;
		case Constants.MENU_SEND_SMS:
			sendSMSFortune();
			break;
		case Constants.MENU_SEND_KAKAO:
			sendKakaoFortune();
			break;
		default:
			break;
		}
		
		return result;
	}
	
	private void sendKakaoFortune() {
		try {
			String my_name = mIPref.getString(Constants.USER_NAME, "");
			String other_name = mUPref.getString(Constants.MATE_NAME, "");
			String sendMsg;
			
			if(mFlag == 1) {
				sendMsg = my_name + getString(R.string.suffix_me) + other_name + getString(R.string.suffix_ellipsis_you) + mHarmonySummary;		// 나에게 너는을 상대방에게 전송
			} else {
				sendMsg = other_name + getString(R.string.suffix_me) + my_name + getString(R.string.suffix_ellipsis_you) + mHarmonySummary;		// 너에게 나는을 상대방에게 전송
			}
			
			 ArrayList<Map<String, String>> arrMetaInfo = new ArrayList<Map<String, String>>();

		        // If application is support Android platform. 
		        Map <String, String> metaInfoAndroid = new Hashtable <String, String>(1);
		        metaInfoAndroid.put("os", "android");
		        metaInfoAndroid.put("devicetype", "phone");
		        metaInfoAndroid.put("installurl", Constants.KAKAO_STR_INSTALL_URL);
		        metaInfoAndroid.put("executeurl", "example://example");
		        arrMetaInfo.add(metaInfoAndroid);

		        mLink = new KakaoLink(mContext, Constants.KAKAO_STR_URL, Constants.KAKAO_STR_APPID, 
						Constants.KAKAO_STR_APPVER, sendMsg, Constants.KAKAO_STR_APPNAME, arrMetaInfo, "UTF-8");
			if( mLink.isAvailable() ) {
	            startActivity(mLink.getIntent());
	        } else {
	        	Toast.makeText(mContext, getString(R.string.toast_caution_not_kakaotalk), Toast.LENGTH_SHORT).show();
	        }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private void sendSMSFortune() {
		String my_name = mIPref.getString(Constants.USER_NAME, "");
		String other_name = mUPref.getString(Constants.MATE_NAME, "");
		
//		Log.d(TAG, "결과 : " + mHarmonySummary);
		
		final Intent intent = new Intent(Intent.ACTION_VIEW);
		if(mFlag == 1) {
			intent.putExtra("sms_body", my_name + getString(R.string.suffix_me) + other_name + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);		// 나에게 너는을 상대방에게 전송
		} else {
			intent.putExtra("sms_body", other_name + getString(R.string.suffix_me) + my_name + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);		// 너에게 나는을 상대방에게 전송
		}
		
		intent.setType("vnd.android-dir/mms-sms");
		startActivity(intent);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
	public void showUploadSNSDialog() {
		final String items[] = {getString(R.string.sns_twitter), getString(R.string.sns_facebook), getString(R.string.sns_metoday)};
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.dialog_title_sns_list)).setCancelable(true).setAdapter(new SNSAdapter(getApplicationContext(), 
				R.layout.user_sns_display_row, items), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				switch(whichButton) {
				case 0:
//					Log.d(TAG, "트위터");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						
						if(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, "").equals("")
								|| userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, "").equals("") 
								|| userPref.getString(Constants.TWITTER_PREFERENCE_PIN_CODE, "").equals("")) {
							twitterLogin();	// 로그인
						} else {
							twitterPostDialog();
						}
					}
					break;
				case 1:
//					Log.d(TAG, "페이스북");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						if (Constants.FACEBOOK_APP_ID == null) {
				            FaceBookUtil.showAlert(getApplicationContext(), "Warning", "Facebook Applicaton ID must be " +
				                    "specified before running this example: see Example.java");
				        }
						
						if(mFlag == 1) {
							mFaceBookLogin = new FaceBookLogin(mContext, HarmonyResult.this, 
									mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_me) + 
									mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
						} else {
							mFaceBookLogin = new FaceBookLogin(mContext, HarmonyResult.this, 
									mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_me) + 
									mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
						}
						mFaceBookLogin.login();
					}
			        
					break;
				case 2:
//					Log.d(TAG, "미투데이");
					
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(HarmonyResult.this, SNSWebView.class);

						if(mFlag == 1) {
							intent.putExtra("SNS_BODY", 
									mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_me) + 
									mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
						} else {
							intent.putExtra("SNS_BODY", 
									mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_me) + 
									mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
						}
						
						intent.putExtra("LOGIN_FLAG", Constants.ME2DAY_LOGIN_FLAG);
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
					}
					break;
				}
			}
		});
		
		builder.create().show();
	}
	
	
	private void twitterLogin() {
		mTwitter = new TwitterFactory().getInstance();
    	mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
    	
    	mRequestToken = null;

		try {
	    	mRequestToken = mTwitter.getOAuthRequestToken();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	
		Intent intent = new Intent(HarmonyResult.this, SNSWebView.class);
		intent.putExtra("REQUEST_URL", mRequestToken.getAuthorizationURL());
		intent.putExtra("LOGIN_FLAG", Constants.TWITTER_LOGIN_FLAG);
		startActivityForResult(intent, Constants.TWITTER_LOGIN_FLAG);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
    
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	
	    	if(resultCode == RESULT_OK) {		// 서브 액티비티가 정상적으로 종료 되었다면
	    		if(requestCode == Constants.TWITTER_LOGIN_FLAG) {
	    			// 서브 액티비티에서 받아온 결과값을 이용해 처리
	    			
//	    			Log.d(TAG, "결과값 : " + data.getStringExtra("PIN_CODE"));
	    			
	    			try {
						mAccessToken = mTwitter.getOAuthAccessToken(mRequestToken, data.getStringExtra("PIN_CODE"));
						mTwitter.setOAuthAccessToken(mAccessToken);
						
						//로그인을 한번만 하기 위해, PINCODE, AccessToken, AccessSecretToken 저장
						
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = userPref.edit();
						
						editor.putString(Constants.TWITTER_PREFERENCE_PIN_CODE, data.getStringExtra("PIN_CODE"));
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, mAccessToken.getToken());
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, mAccessToken.getTokenSecret());
						
						editor.commit();
						
						twitterPostDialog();
					} catch (TwitterException e) {
						e.printStackTrace();
					}
	    		}
	    	}
	    }

    private void twitterPostDialog() {
    	final EditText fortune_text = new EditText(mContext);
    	
    	if(mFlag == 1) {
    		fortune_text.setText(mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_me) +
    				mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
		} else {
			fortune_text.setText(mUPref.getString(Constants.MATE_NAME, "") + getString(R.string.suffix_me) + 
					mIPref.getString(Constants.USER_NAME, "") + getString(R.string.suffix_ellipsis_you) + mHarmonySummary);
		}
		 
		AlertDialog.Builder builder =  new AlertDialog.Builder(mContext);
		builder.setTitle(getString(R.string.dialog_title_sns_registe_twitter)).setView(fortune_text)
		    .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	TwitterLoadingAsync twitterLoading = new TwitterLoadingAsync();
		    	twitterLoading.execute(fortune_text.getText().toString());
		    	
		    }
		    }).setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
			}
		}).create().show();
    }
    
    private class TwitterLoadingAsync extends AsyncTask<String, Void, Void> {
    	
    	SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
    	ProgressDialog loading;
    	int twitterErrorCheck = 1;
    	
    	@Override
    	protected Void doInBackground(String... strData) {
    		try {
	    		 mAccessToken = new AccessToken(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, ""), 
	    				 userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, ""));
	    		
	    		
	    		 ConfigurationBuilder cb = new ConfigurationBuilder();
	    	     cb.setOAuthAccessToken(mAccessToken.getToken());
	    	     cb.setOAuthAccessTokenSecret(mAccessToken.getTokenSecret());
	    	     cb.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
	    	     cb.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
	    	     Configuration config = cb.build();
	    	     OAuthAuthorization auth = new OAuthAuthorization(config);
	    	      
	    	     TwitterFactory tFactory = new TwitterFactory(config);
	    	     Twitter twitter = tFactory.getInstance();

	    	     twitter.updateStatus(strData[0].toString());
			} catch (TwitterException e) {
				twitterErrorCheck = 2;
			}
			
    		return null;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		loading = ProgressDialog.show(mContext, "", getString(R.string.progress_upload_post));
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		loading.dismiss();
    		
    		if(twitterErrorCheck == 2) {
    			Toast.makeText(mContext, getString(R.string.toast_caution_incorrect_post), Toast.LENGTH_SHORT).show();
    		} else {
    			Toast.makeText(mContext, getString(R.string.toast_succeed_registe_post), Toast.LENGTH_SHORT).show();
    		}
    	}
    }
	

	private void getIntenter() {
		Intent intent = getIntent();
		mFlag = intent.getExtras().getInt("YOUANDME");
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	 @Override
		public void onBackPressed() {
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
}
