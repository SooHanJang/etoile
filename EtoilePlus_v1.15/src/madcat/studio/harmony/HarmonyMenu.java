package madcat.studio.harmony;

import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

public class HarmonyMenu extends Activity {
	
//	private static final String TAG = "HarmonyMenu";
	private ImageButton mYouToMe, mMeToYou;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.harmony_submenu);
		
		mYouToMe = (ImageButton)findViewById(R.id.harmony_you_to_me);		// 나에게 너는
		mMeToYou = (ImageButton)findViewById(R.id.harmony_me_to_you);		// 너에게 나는
		
		mYouToMe.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(HarmonyMenu.this, HarmonyResult.class);
				intent.putExtra("YOUANDME", 1);		// 1 - 나에게 너는...
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
		
		mMeToYou.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(HarmonyMenu.this, HarmonyResult.class);
				intent.putExtra("YOUANDME", 2);		// 2 - 너에게 나는...
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
		});
	}
	
	@Override
	protected void onDestroy() {
//		Log.d(TAG, "Call onDestroy");
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
