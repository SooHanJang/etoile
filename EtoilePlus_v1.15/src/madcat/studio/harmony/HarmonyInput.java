package madcat.studio.harmony;

import java.util.ArrayList;
import java.util.Locale;

import madcat.studio.adapter.FriendsAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.friends.Friends;
import madcat.studio.friends.FriendsList;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class HarmonyInput extends Activity {
//	private static final String TAG = "Harmony Input";

	private Context mContext;
	
	private EditText mNameEditText, mYearEditText, mMonthEditText, mDayEditText;
	private RadioGroup mRadioGroupGender, mRadioGroupSolarOrLunar;
	private RadioButton mMaleRadioBtn, mFemaleRadioBtn, mSolarRadioBtn, mLunarRadioBtn;
	private ImageButton mOkImageBtn, mCancleImageBtn;
	
	private int mStatusCheck;
	private String mLocale;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mLocale = getResources().getConfiguration().locale.getLanguage();
		if(mLocale.equals(Locale.KOREAN.toString())) {
			setContentView(R.layout.harmony_input);
		} else {
			setContentView(R.layout.harmony_input_en);
		}
		
		mContext = this;
		
		mStatusCheck = 1;
		
		mRadioGroupGender = (RadioGroup)findViewById(R.id.harmony_radio_gender);
		mMaleRadioBtn = (RadioButton)findViewById(R.id.harmony_radio_male);
		mFemaleRadioBtn = (RadioButton)findViewById(R.id.harmony_radio_female);
		
		mRadioGroupSolarOrLunar = (RadioGroup)findViewById(R.id.harmony_radio_solar_or_lunar);
		mSolarRadioBtn = (RadioButton)findViewById(R.id.harmony_radio_solar);
		mLunarRadioBtn = (RadioButton)findViewById(R.id.harmony_radio_lunar);
		
		mOkImageBtn = (ImageButton)findViewById(R.id.harmony_btn_ok);
		mCancleImageBtn = (ImageButton)findViewById(R.id.harmony_btn_cancle);
		
		mNameEditText = (EditText)findViewById(R.id.harmony_input_name);
		mYearEditText = (EditText)findViewById(R.id.harmony_input_year);
		mMonthEditText = (EditText)findViewById(R.id.harmony_input_month);
		mDayEditText = (EditText)findViewById(R.id.harmony_input_day);
		
		// 글꼴 설정
		SharedPreferences configPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			mNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mYearEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mMonthEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mDayEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
		} else {
			mNameEditText.setTypeface(Typeface.DEFAULT);
			mYearEditText.setTypeface(Typeface.DEFAULT);
			mMonthEditText.setTypeface(Typeface.DEFAULT);
			mDayEditText.setTypeface(Typeface.DEFAULT);
		}
		
		//Radio Button Event Handling
		mRadioGroupGender.setOnCheckedChangeListener(new RadioGroupEventHandler());
		mRadioGroupSolarOrLunar.setOnCheckedChangeListener(new RadioGroupEventHandler());
		
		//Image Button Event Handling
		mOkImageBtn.setOnClickListener(new OkButtonEventHandler());
		mCancleImageBtn.setOnClickListener(new CancleButtonEventHandler());
		
		mNameEditText.requestFocus();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		finish();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.harmony_fortune_menu, menu);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
		case R.id.harmony_friends_list:
			showFriendsListDialog();
			break;
		case R.id.harmony_friends_manage:
			Intent intent_friendList = new Intent(HarmonyInput.this, FriendsList.class);
			startActivity(intent_friendList);
			overridePendingTransition(R.anim.fade, R.anim.hold);
			break;
		}
		
		return result;
	}
	
	private void showFriendsListDialog() {
		final ArrayList<Friends> friendsOrders = Util.getFriendsList();

		if(friendsOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_not_registe_friends), Toast.LENGTH_SHORT).show();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.dialog_title_frineds_list)).setCancelable(true).setAdapter(new FriendsAdapter(getApplicationContext(), 
					R.layout.setting_friends_row, friendsOrders, Constants.FLAG_FRIENDS_TODAYFORTUNE_SEARCH), new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
						mStatusCheck = 2;
						
						mNameEditText.setText(friendsOrders.get(which).getName().toString());
						
						if(friendsOrders.get(which).getBirthType() == 1) {		// 양력이면
							mYearEditText.setText(String.valueOf(friendsOrders.get(which).getSolarYear()));
							mMonthEditText.setText(String.valueOf(friendsOrders.get(which).getSolarMonth()));
							mDayEditText.setText(String.valueOf(friendsOrders.get(which).getSolarDay()));
							
							mSolarRadioBtn.setChecked(true);
							mLunarRadioBtn.setChecked(false);
						} else {		 // 음력이면
							mYearEditText.setText(String.valueOf(friendsOrders.get(which).getLunarYear()));
							mMonthEditText.setText(String.valueOf(friendsOrders.get(which).getLunarMonth()));
							mDayEditText.setText(String.valueOf(friendsOrders.get(which).getLunarDay()));
							
							mSolarRadioBtn.setChecked(false);
							mLunarRadioBtn.setChecked(true);
						}
						
						if(friendsOrders.get(which).getGender() == 1) {		// 남자라면
							mMaleRadioBtn.setChecked(true);
							mFemaleRadioBtn.setChecked(false);
						} else {		// 여자라면
							mMaleRadioBtn.setChecked(false);
							mFemaleRadioBtn.setChecked(true);
						}
					
						dialog.dismiss();
						mStatusCheck = 1;
					}
				});
			
			builder.create().show();
		}
	}
	
	private class RadioGroupEventHandler implements RadioGroup.OnCheckedChangeListener {
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);

			switch(checkedId) {
			case R.id.harmony_radio_male:
				if(mStatusCheck == 1) {
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_man), Toast.LENGTH_SHORT).show();
				}
				mFemaleRadioBtn.setChecked(false);
				mMaleRadioBtn.setChecked(true);
				break;
			case R.id.harmony_radio_female:
				if(mStatusCheck == 1) {
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_woman), Toast.LENGTH_SHORT).show();
				}
				mMaleRadioBtn.setChecked(false);
				mFemaleRadioBtn.setChecked(true);
				break;
			case R.id.harmony_radio_solar:
				if(mStatusCheck == 1) {
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_solar), Toast.LENGTH_SHORT).show();
				}
				mSolarRadioBtn.setChecked(true);
				mLunarRadioBtn.setChecked(false);
				break;
			case R.id.harmony_radio_lunar:
				if(mStatusCheck == 1) {
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_lunar), Toast.LENGTH_SHORT).show();
				}
				mSolarRadioBtn.setChecked(false);
				mLunarRadioBtn.setChecked(true);
				break;
			}
		}
	}
	
	private class OkButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			SharedPreferences mPref = getSharedPreferences(Constants.MATE_PREFERENCE_NAME, Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = mPref.edit();
			
			int correctCode = Util.isCorrect(mNameEditText.getText().toString(), mYearEditText.getText().toString(), mMonthEditText.getText().toString(), mDayEditText.getText().toString());
			FortuneDate solarDate, lunarDate;
			
			switch(correctCode) {
			case 0:
				if(mLunarRadioBtn.isChecked()) {
					if(!Util.emptyLunarDateCheck(Integer.parseInt(mYearEditText.getText().toString()), 
							Integer.parseInt(mMonthEditText.getText().toString()), 
							Integer.parseInt(mDayEditText.getText().toString()))) {
						Toast.makeText(mContext, getString(R.string.toast_caution_not_lunar_day), Toast.LENGTH_SHORT).show();
						break;
					}
				}
				
				editor.putString(Constants.MATE_NAME, mNameEditText.getText().toString());
				
				if(mRadioGroupGender.getCheckedRadioButtonId() == R.id.harmony_radio_male) {
					editor.putInt(Constants.MATE_GENDER, 1);
				} else {
					editor.putInt(Constants.MATE_GENDER, 2);
				}
				
				if(mRadioGroupSolarOrLunar.getCheckedRadioButtonId() == R.id.harmony_radio_solar) {
//					Log.d(TAG, "양력 선택");
					solarDate = new FortuneDate(Integer.parseInt(mYearEditText.getText().toString()), Integer.parseInt(mMonthEditText.getText().toString()), Integer.parseInt(mDayEditText.getText().toString()));
					lunarDate = Util.getLunarDate(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay());
					
//					Log.d(TAG, solarDate.getYear() + "/" + solarDate.getMonth() + "/" + solarDate.getDay());
//					Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
					
					editor.putInt(Constants.MATE_BIRTH_TYPE, 1);
				} else {
//					Log.d(TAG, "음력 선택");
					lunarDate = new FortuneDate(Integer.parseInt(mYearEditText.getText().toString()), Integer.parseInt(mMonthEditText.getText().toString()), Integer.parseInt(mDayEditText.getText().toString()));
					solarDate = Util.getSolarDate(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
					
//					Log.d(TAG, solarDate.getYear() + "/" + solarDate.getMonth() + "/" + solarDate.getDay());
//					Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
					
					editor.putInt(Constants.MATE_BIRTH_TYPE, 2);
				}
				
				editor.putInt(Constants.MATE_SOLAR_YEAR, solarDate.getYear());
				editor.putInt(Constants.MATE_SOLAR_MONTH, solarDate.getMonth());
				editor.putInt(Constants.MATE_SOLAR_DAY, solarDate.getDay());
				
				editor.putInt(Constants.MATE_LUNAR_YEAR, lunarDate.getYear());
				editor.putInt(Constants.MATE_LUNAR_MONTH, lunarDate.getMonth());
				editor.putInt(Constants.MATE_LUNAR_DAY, lunarDate.getDay());
				
				editor.putInt(Constants.MATE_WEST_STAR_POSITION, Util.getWestStarPosition(solarDate.getMonth(), solarDate.getDay()));
				editor.putInt(Constants.MATE_EAST_STAR_POSITION, Util.getEastStarPosition(lunarDate.getMonth(), lunarDate.getDay()));
				
				editor.commit();
//				Log.d(TAG, "상대방 자료 commit 완료");
				
				Intent intent = new Intent(HarmonyInput.this, HarmonyMenu.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
				break;
			case 1:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_name), Toast.LENGTH_SHORT).show();
				break;
			case 2:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_year), Toast.LENGTH_SHORT).show();
				break;
			case 3:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_year), Toast.LENGTH_SHORT).show();
				break;
			case 4:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_month), Toast.LENGTH_SHORT).show();
				break;
			case 5:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_day), Toast.LENGTH_SHORT).show();
				break;
			case 6:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_month), Toast.LENGTH_SHORT).show();
				break;
			case 7:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_day), Toast.LENGTH_SHORT).show();
				break;
			}
		}
	}
	
	private class CancleButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			finish();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
