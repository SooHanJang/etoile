package madcat.studio.calendar;

public class DayInfo {
	private String title;
	private String year;
	private String day;
	private String month;
	private String body;
    private int weather;
	
	private boolean inMonth;
    
	public String getBody() {	return body;	}
	public void setBody(String body) {	this.body = body;	}
	
    public String getTitle() {	return title;	}
	public void setTitle(String title) {	this.title = title;	}
    
    public String getYear() {	return year;	}
    public void setYear(String year) {	this.year = year;	}
    
    public String getMonth() {	return month;	}
    public void setMonth(String month) {	this.month = month;	}
    
    public String getDay() {	return day;	}
    public void setDay(String day) {	this.day = day;	}
    
    public int getWeather() {	return weather;	}
    public void setWeather(int weather)	{	this.weather = weather;	}
 
    /**
     * 이번달의 날짜인지 정보를 반환한다.
     * 
     * @return inMonth(true/false)
     */
    public boolean isInMonth() {
        return inMonth;
    }
 
    /**
     * 이번달의 날짜인지 정보를 저장한다.
     * 
     * @param inMonth(true/false)
     */
    public void setInMonth(boolean inMonth) {
        this.inMonth = inMonth;
    }
}
