package madcat.studio.calendar;

import java.util.ArrayList;
import java.util.Calendar;

import madcat.studio.adapter.CalendarFortuneAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneAllDate;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

public class CalendarFortune extends Activity implements OnItemClickListener {

	private final String TAG											=	"CalendarFortune";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	
	public static int SUNDAY									        =	1;
	public static int MONDAY       										=	2;
	public static int TUESDAY										    =	3;
	public static int WEDNSESDAY										=	4;
	public static int THURSDAY											=	5;
	public static int FRIDAY											=	6;
	public static int SATURDAY											=	7;
	
	private Context mContext;
	private SharedPreferences mUserPref, mConfigPref;
	
	private CalendarFortuneAdapter mCalendarAdapter;
	private ArrayList<DayInfo> mDayList;
	
	private Calendar mThisMonthCalendar;
	private TextView mTextCalendarTitle, mTextCalendarFortune;
    private GridView mGridCalendar;
    private Button mBtnCalendarSetting;
    private TextView[] mTextCalendarWeekOfDay;
    private TextView[] mTextNumeric;
    private TableLayout mTableNumeric;
    
    private String[][] mFortuneNumeric;
    
    private int mDayOfMonthStartGap;
    private int mLastYear, mLastMonth, mLastMaximDay;
    private int mNextYear, mNextMonth;

    private int mStartDayIndex;
    private String[] mFortuneText;
    private ArrayList<FortuneAllDate> mArrayAllDate;
    
//    private String[] mWeekOfDay;
    private int mLastMonthStartDay;
    private int mDayOfMonth;
    private int mThisMonthLastDay;
    
    // 달력 클릭 처리 관련 변수
    private View mCalendarTempView;
    private boolean mCalendarInitFlag										=	false;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_calendar_fortune);
		
		// 초기화 부분
		mContext = this;
		mUserPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mDayList = new ArrayList<DayInfo>();
		mTextCalendarWeekOfDay = new TextView[7];
		mTextNumeric = new TextView[4];
		
		mTextCalendarTitle = (TextView)findViewById(R.id.calendar_fortune_title);
		mTextCalendarFortune = (TextView)findViewById(R.id.calendar_fortune_text);
		mGridCalendar = (GridView)findViewById(R.id.calendar_fortune_gridview);
		mTableNumeric = (TableLayout)findViewById(R.id.calendar_fortune_numeric_table);
		
		for(int i=1; i <= mTextCalendarWeekOfDay.length; i++) {
			int resId = getResources().getIdentifier("calendar_fortune_week_of_day_" + i, "id", getPackageName());
			mTextCalendarWeekOfDay[i-1] = (TextView)findViewById(resId);
		}
		
		for(int i=0; i < mTextNumeric.length; i++) {
			int resId = getResources().getIdentifier("calendar_fortune_numeric_" + (i+1), "id", getPackageName());
			mTextNumeric[i] = (TextView)findViewById(resId);
		}
		
		// 글꼴 설정
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			mTextCalendarTitle.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mTextCalendarFortune.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			
			for(int i=0; i < mTextCalendarWeekOfDay.length; i++) {
				mTextCalendarWeekOfDay[i].setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			}
			
			for(int i=0; i < mTextNumeric.length; i++) {
				mTextNumeric[i].setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			}
			
		} else {
			mTextCalendarTitle.setTypeface(Typeface.DEFAULT);
			mTextCalendarFortune.setTypeface(Typeface.DEFAULT);
			
			for(int i=0; i < mTextCalendarWeekOfDay.length; i++) {
				mTextCalendarWeekOfDay[i].setTypeface(Typeface.DEFAULT);
			}
			
			for(int i=0; i < mTextNumeric.length; i++) {
				mTextNumeric[i].setTypeface(Typeface.DEFAULT);
			}
		}
		
		// 리스너 초기화
		mGridCalendar.setOnItemClickListener(this);
		
		// GridLayout 사이즈 설정
		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)mGridCalendar.getLayoutParams();
		lp.height = mContext.getResources().getDisplayMetrics().widthPixels;
		mGridCalendar.setLayoutParams(lp);
		
		// Adpater 호출
		mThisMonthCalendar = Calendar.getInstance();
		mThisMonthCalendar.set(Calendar.DAY_OF_MONTH, 1);
		
		InitCalendarFortuneAsync initCalendarAsync = new InitCalendarFortuneAsync(mThisMonthCalendar);
		initCalendarAsync.execute();
		
	}
	
	class InitCalendarFortuneAsync extends AsyncTask<Void, Void, Void> {
		
		Calendar calendar;
		ProgressDialog progressDialog;
		int todayIndex;
		
		
		public InitCalendarFortuneAsync(Calendar calendar) {
			this.calendar = calendar;
		}
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", "불러오는 중입니다...");
		}
		
		
		// DB 로 처리해야 될 부분을 이곳에서 처리하도록 한다.
		@Override
		protected Void doInBackground(Void... arg0) {
			
			// Calendar 를 설정하는 부분 (이미 날짜 값은 전부 설정 되어 있다.)
			getCalendar(calendar);
			
			// lunarDate ArrayList 에는 저번달, 이번달, 다음달의 모든 음력일이 기록된다.
			mArrayAllDate = Util.getRangeAllDate(mContext, (Integer.parseInt(Util.getYear())), (Integer.parseInt(Util.getMonthOfYear())));
			mFortuneText = new String[mArrayAllDate.size()];
			
			mFortuneNumeric = new String[mTextNumeric.length][mArrayAllDate.size()];
			
			// 오늘의 음력 날짜를 가져온다.
			FortuneDate todayLunarDate = Util.getLunarDate(Util.TODAY);
			
			// 운세 타입에 따른 운세 내용을 설정한다.
			for(int i=0; i < mArrayAllDate.size(); i++) {
				String fortuneType = Util.getEastTodayFortune(mUserPref.getInt(Constants.USER_EAST_STAR_POSITION, -1), 
						Util.getEastStarPosition(mArrayAllDate.get(i).getLunarMonth(), mArrayAllDate.get(i).getLunarDay()));
				mFortuneText[i] = Util.getSendText(mContext, fortuneType)[0];
				
				for(int j=0; j < mTextNumeric.length; j++) {
					mFortuneNumeric[j][i] = Util.getFortuneNumeric(mContext, fortuneType)[j];
				}
				
				// for 문을 돌면서, 오늘 음력 날짜에 해당 하는 index 가 몇번째인지를 미리 체크해둔다.
				if((mArrayAllDate.get(i).getLunarYear() == todayLunarDate.getYear()) &&
						(mArrayAllDate.get(i).getLunarMonth() == todayLunarDate.getMonth()) &&
						(mArrayAllDate.get(i).getLunarDay() == todayLunarDate.getDay())) {
					todayIndex = i;
					mStartDayIndex = (todayIndex - Integer.parseInt(Util.getTodayOfMonth()) - mDayOfMonth) + 2;
				}
				
				// mDayList 와 비교를 하면서 이번 달력에 표시되는 날짜들에만 운세 날씨 값을 넣어준다.
				for(int j=0; j < mDayList.size(); j++) {
					if(mArrayAllDate.get(i).getSolarYear() == (Integer.parseInt(mDayList.get(j).getYear()))
							&& mArrayAllDate.get(i).getSolarMonth() == (Integer.parseInt(mDayList.get(j).getMonth()))
							&& mArrayAllDate.get(i).getSolarDay() == (Integer.parseInt(mDayList.get(j).getDay()))) {
						mDayList.get(j).setWeather(Util.getWidgetFortuneWeather(mContext, fortuneType));
					}
				}
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}

			// Adapter 를 호출하여 달력을 구성해준다.
			initCalendarAdapter();

			// 캘린더 타이틀(년월 표시)을 세팅한다. 
//	        mTextCalendarTitle.setText(Util.getYear() + mContext.getString(R.string.calendar_title_year) + 
//	        		Util.getMonthOfYear() + mContext.getString(R.string.calendar_title_month));
			
			mTextCalendarTitle.setText(Util.getYear() + ". " +  Util.getMonthOfYear() + " " + getString(R.string.suffix_fortune));
	        
	        // 요일을 설정해준다.
	        String[] weekOfDay = {getString(R.string.calendar_title_sunday), getString(R.string.calendar_title_monday), 
		    		getString(R.string.calendar_title_tuesday), getString(R.string.calendar_title_wednesday), 
		    		getString(R.string.calendar_title_thursday), getString(R.string.calendar_title_friday), 
		    		getString(R.string.calendar_title_saturday) };
	        
	        for(int i=0; i < mTextCalendarWeekOfDay.length; i++) {
	        	mTextCalendarWeekOfDay[i].setText(weekOfDay[i]);
	        }
	        
	        // 운세 지수를 표시하는 테이블 레이아웃을 화면에 표시한다.
	        mTableNumeric.setVisibility(View.VISIBLE);

	        // 오늘 날짜에 해당하는 운세 지수를 설정한다.
//	        for(int i=0; i < mTextNumeric.length; i++) {
//	        	mTextNumeric[i].setText(mFortuneNumeric[i][todayIndex]);
//	        }
	        
	        setTextNumeric(todayIndex);
	        
			// 오늘 날짜에 해당하는 운세를 설정해준다.
			mTextCalendarFortune.setText(mFortuneText[todayIndex]);
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "onPostExecute 완료");
			}
		}
	}
	
	private void setTextNumeric(int index) {
		mTextNumeric[0].setText(getString(R.string.calendar_title_feel_amount) + "(" + mFortuneNumeric[0][index] + ")");
        mTextNumeric[1].setText(getString(R.string.calendar_title_health_amount) + "(" + mFortuneNumeric[1][index] + ")");
        mTextNumeric[2].setText(getString(R.string.calendar_title_money_amount) + "(" + mFortuneNumeric[2][index] + ")");
        mTextNumeric[3].setText(getString(R.string.calendar_title_love_amount) + "(" + mFortuneNumeric[3][index] + ")");
	}
	
	private void getCalendar(Calendar calendar) {
        mDayList.clear();
        
        // 이번달 시작일의 요일을 구한다. 시작일이 일요일인 경우 인덱스를 1(일요일)에서 8(다음주 일요일)로 바꾼다.)
        mDayOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
        mThisMonthLastDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        
        calendar.add(Calendar.MONTH, -1);
        
        // 지난달의 마지막 일자를 구한다.
        mLastMonthStartDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
 
        // 지난달에 해당하는 년도와 날짜 그리고 마지막 날을 전역변수로 설정한다.
        mLastYear = calendar.get(Calendar.YEAR);
        mLastMonth = calendar.get(Calendar.MONTH)+1;
        mLastMaximDay = mLastMonthStartDay;
        
        calendar.add(Calendar.MONTH, 1);
        
        // 다음 달에 해당하는 년도와 날짜를 전역변수로 설정한다. (반드시 calendar 에 add 한 이 지점에서 전역 설정을 해야함.)
        mNextMonth = calendar.get(Calendar.MONTH)+2;
        mNextYear = calendar.get(Calendar.YEAR);
        
        if(mNextMonth > 12) {
        	mNextYear++;
        	mNextMonth = 1;
        }
        
        if(mDayOfMonth == SUNDAY) {
            mDayOfMonth += 7;
        }
         
        mLastMonthStartDay -= (mDayOfMonth-1)-1;
 
        DayInfo day;
         
        setDayOfMonthStartGap(mDayOfMonth-2);
         
        // 지난 달
        for(int i=0; i<mDayOfMonth-1; i++) {
            int date = mLastMonthStartDay+i;
            day = new DayInfo();
            
            day.setYear(Integer.toString(mLastYear));
            day.setMonth(Integer.toString(mLastMonth));
            day.setDay(Integer.toString(date));
            day.setInMonth(false);
             
            mDayList.add(day);
        }
        
        // 이번 달
        for(int i=1; i <= mThisMonthLastDay; i++) {
            day = new DayInfo();
            
            day.setYear(Integer.toString(mThisMonthCalendar.get(Calendar.YEAR)));
            day.setMonth(Integer.toString(mThisMonthCalendar.get(Calendar.MONTH) + 1));
            day.setDay(Integer.toString(i));
            day.setInMonth(true);
             
            mDayList.add(day);
        }
        
        // 다음 달
        for(int i=1; i<42-(mThisMonthLastDay+mDayOfMonth-1)+1; i++) {
            day = new DayInfo();
            
            day.setYear(Integer.toString(mNextYear));
            day.setMonth(Integer.toString(mNextMonth));
            day.setDay(Integer.toString(i));
            day.setInMonth(false);
            mDayList.add(day);
        }
	}
	
	private void initCalendarAdapter() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "initCalendarAdapter 호출");
		}
		
		
//        mCalendarAdapter = new CalendarFortuneAdapter(this, R.layout.user_calendar_fortune_day, mDayList, 
//        		mThisMonthCalendar.get(Calendar.YEAR), (mThisMonthCalendar.get(Calendar.MONTH)+1));
		mCalendarAdapter = new CalendarFortuneAdapter(this, R.layout.user_calendar_fortune_day, mDayList); 
        mGridCalendar.setAdapter(mCalendarAdapter);
    }
	
	/**
     * 해당 달과 GridView Position 위치의 차이를 저장하기 위한 setter, getter 함수
     * 
     */
    
    public int getDayOfMonthStartGap() { return mDayOfMonthStartGap; }
	public void setDayOfMonthStartGap(int dayOfMonthStartPosition) { this.mDayOfMonthStartGap = dayOfMonthStartPosition; }
	
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		int dayPositionIndex = position - getDayOfMonthStartGap();
		
		int clickYear = 0, clickMonth = 0, clickDay = 0;
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "dayPositionIndex : " + dayPositionIndex);
		}
		
		if(dayPositionIndex > 0 && dayPositionIndex <= mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
    		clickYear = mThisMonthCalendar.get(Calendar.YEAR);
    		clickMonth = mThisMonthCalendar.get(Calendar.MONTH)+1;
    		clickDay = position - getDayOfMonthStartGap();
    	} else if(dayPositionIndex <= 0) {
    		clickYear = mLastYear;
    		clickMonth = mLastMonth;
    		clickDay = mLastMaximDay + dayPositionIndex;
    	} else if(dayPositionIndex > mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)) {
    		clickYear = mNextYear;
    		clickMonth = mNextMonth;
    		clickDay = dayPositionIndex - mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    	}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "클릭된 날짜 : " + clickYear + ", " + clickMonth + ", " + clickDay);
			Log.d(TAG, "클릭한 뷰 : " + v);
		}
		
		if(!mCalendarInitFlag) {
			mCalendarTempView = mCalendarAdapter.getCurrentTodayView();
			mCalendarInitFlag = true;
			
		}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "mCalendarTempView : " + mCalendarTempView);
		}
		
		// 이번달에 해당하는 운세만 클릭과 운세 내용을 처리한다.
		if(clickYear == mThisMonthCalendar.get(Calendar.YEAR) &&
				clickMonth == (mThisMonthCalendar.get(Calendar.MONTH) + 1)) {

			if(!v.equals(mCalendarTempView)) {
				mCalendarTempView.setBackgroundResource(R.drawable.calendar_bg_day);
			}

			mCalendarTempView = v;
			v.setBackgroundResource(R.drawable.calendar_bg_fill_day);
			
			// 날짜에 해당하는 운세 지수를 설정한다.
			setTextNumeric(mStartDayIndex + position);
			
	        // 날짜에 해당하는 운세를 설정한다.
			mTextCalendarFortune.setText(mFortuneText[mStartDayIndex + position]);
			
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
