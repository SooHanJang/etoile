package madcat.studio.widget;

import java.util.Random;

import madcat.studio.constant.Constants;
import madcat.studio.dialog.VoiceResultActivityDialog;
import madcat.studio.fortune.TodayFortune;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class UserWidget extends AppWidgetProvider {

//	private static final String TAG										=	"WIDGET";
	
	private static final int FONT_COLOR_BLACK							=	0;
	private static final int FONT_COLOR_WHITE							=	1;
	
	private static final int WIDGET_UPDATE_INTERVAL						=	10 * 60 * 1000;		// 위젯이 바뀌는 간격 설정
	private static AlarmManager mAlarmManager;
	private static PendingIntent mPendingIntent;

	private static boolean mRandomIndexArrayFlag[]						=	new boolean[7];
	private static int mRandomIndexCounter								=	0;
	private static String mReceiveAction;
	
	public void onReceive(Context context, Intent intent) {		// 위젯과 관련된 인텐트를 Broadcast 받았을 때 실행되는 call back 메소드
		super.onReceive(context, intent);
		
		mReceiveAction = intent.getAction();
//		Log.d(TAG, "수신한 액션 : " + mReceiveAction);
		
		if(AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(mReceiveAction)) {
			removePreviousAlarm();		// 일단 설정된 알람을 취소하고

			long firstTime = System.currentTimeMillis() + WIDGET_UPDATE_INTERVAL;
			mPendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			mAlarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			mAlarmManager.set(AlarmManager.RTC, firstTime, mPendingIntent);
			
			updateWidget(context);
		} else if(Constants.ACTION_WIDGET_UPDATE_THEME.equals(mReceiveAction)) {					//	위젯 테마 업데이트 수신
			updateWidget(context);
		} else if(AppWidgetManager.ACTION_APPWIDGET_DISABLED.equals(mReceiveAction)) {
			removePreviousAlarm();		// 설정된 알람을 취소
		} else if(mReceiveAction.equals(Constants.ACTION_CHANGE_LANGUAGE)) {
			updateWidget(context);
		}
	}
	
	private void updateWidget(Context context) {
		ComponentName thisAppWidget = new ComponentName(context.getPackageName(), UserWidget.class.getName());
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);
		
		onUpdate(context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {		// 위젯을 설정된 interval 에 따라 업데이트 한다.
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		for(int i=0; i < appWidgetIds.length; i++) {
			int appwidgetId = appWidgetIds[i];
			updateAppWidget(context, appWidgetManager, appwidgetId);		// 위젯을 업데이트 하는 메소드
		}
	}
	
	// 위젯을 설정된 알람 시간 가격에 맞춰 업데이트 하는 메소드
	public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
//		Log.d(TAG, "updateAppWidget 호출");

		SharedPreferences pref = context.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		SharedPreferences configPref = context.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);

		String[] textDisplay = null;
		Random random = new Random();		// 랜덤 객체 생성
		int randomNumber = 0;
		random.setSeed(System.currentTimeMillis());		// 랜덤 시드를 현재 시간으로 설정
		
		// 0~4 사이의 랜덤값을 얻어오기 위한 방법
		while (true) {
			randomNumber = random.nextInt(7);
			if (!mRandomIndexArrayFlag[randomNumber]) {
				mRandomIndexArrayFlag[randomNumber] = true;
				mRandomIndexCounter++;
				if (mRandomIndexCounter == 7) {
					mRandomIndexCounter = 0;
					for (int i = 0; i < 7; i++) {
						mRandomIndexArrayFlag[i] = false;
					}
				}
				break;
			}
		}
		
		// 위젯이 업데이트 될 때마다 오늘의 운세를 가져오는 DB 실행
		FortuneDate todayLunarDate = Util.getLunarDate(Util.TODAY);
		String todayFortune = Util.getEastTodayFortune(pref.getInt(Constants.USER_EAST_STAR_POSITION, -1), Util.getEastStarPosition(todayLunarDate.getMonth(), todayLunarDate.getDay()));
		textDisplay = Util.getWidgetFortuneText(context, todayFortune);
		
		// 위젯을 업데이트 해주기 위한 위젯 레이아웃을 설정 & 텍스트 설정
		RemoteViews updateViews = new RemoteViews(context.getPackageName(), R.layout.user_widget);
		
		// 위젯 테마를 설정
		int barThemePosition = configPref.getInt(Constants.CONFIG_WIDGET_BAR_THEME_ITEM, 0);
		int bgResId = context.getResources().getIdentifier("widget_background_" + barThemePosition, "drawable", context.getPackageName());
		updateViews.setInt(R.id.widget_background, "setBackgroundResource", bgResId);
		
		// 폰트 칼라 설정
		int fontColor = FONT_COLOR_BLACK;
		
		// 위젯 '바' 테마 설정
		switch(barThemePosition) {
			case 0:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_0);
				break;
			case 1:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_1);
				break;
			case 2:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_2);
				break;
			case 3:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_3);
				break;
			case 4:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_4);
				break;
			case 5:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_5);
				fontColor = FONT_COLOR_WHITE;
				break;
			case 6:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_6);
				fontColor = FONT_COLOR_WHITE;
				break;
			case 7:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_7);
				break;
			case 8:
			case 9:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_0);
				fontColor = FONT_COLOR_WHITE;
			case 10:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_10);
				fontColor = FONT_COLOR_WHITE;
				break;
			case 11:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_10);
				break;
			case 12:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_12);
				break;
			case 13:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_12);
				break;
			case 14:
				updateViews.setInt(R.id.widget_today_bar, "setBackgroundResource", R.drawable.widget_bar_14);
				break;
		}
		
		if(barThemePosition == 8 || barThemePosition == 9) {
			updateViews.setViewVisibility(R.id.widget_today_bar, View.INVISIBLE);
		} else {
			updateViews.setViewVisibility(R.id.widget_today_bar, View.VISIBLE);
		}
		
		// 지정된 폰트 인덱스와 경로 불러오기
		int fontTypeIndex = configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0);
		String fontPath = Constants.FONT_PREFIX_PATH + Util.getFontFileName(fontTypeIndex) + Constants.FONT_SUFFIX_TTF;
		Typeface fontTypeFace = Util.getFontType(context, fontPath, fontTypeIndex);
		
		// 운세를 표시할 텍스트 및 아이콘 설정
		switch(Util.getWidgetFortuneWeather(context, todayFortune)) {
			case Constants.WIDGET_WEATHER_BEST:
				switch(barThemePosition) {
					case 0:
					case 1:
					case 2:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_0);
						break;
					case 3:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_3);
						break;
					case 4:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_4);
						break;
					case 5:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_5);
						break;
					case 6:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_6);
						break;
					case 7:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_best_7);
						break;
				}
				
				break;
			case Constants.WIDGET_WEATHER_GOOD:
				switch(barThemePosition) {
					case 0:
					case 1:
					case 2:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_0);
						break;
					case 3:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_3);
						break;
					case 4:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_4);
						break;
					case 5:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_5);
						break;
					case 6:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_6);
						break;
					case 7:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_good_7);
						break;
				}
				break;
			case Constants.WIDGET_WEATHER_SOSO:
				switch(barThemePosition) {
					case 0:
					case 1:
					case 2:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_0);
						break;
					case 3:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_3);
						break;
					case 4:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_4);
						break;
					case 5:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_5);
						break;
					case 6:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_6);
						break;
					case 7:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_soso_7);
						break;
				}
				break;
			case Constants.WIDGET_WEATHER_BAD:
				switch(barThemePosition) {
					case 0:
					case 1:
					case 2:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_0);
						break;
					case 3:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_3);
						break;
					case 4:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_4);
						break;
					case 5:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_5);
						break;
					case 6:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_6);
						break;
					case 7:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_bad_7);
						break;
				}
				break;
			case Constants.WIDGET_WEATHER_WORST:
				switch(barThemePosition) {
					case 0:
					case 1:
					case 2:
					case 8:
					case 9:
					case 10:
					case 11:
					case 12:
					case 13:
					case 14:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_0);
						break;
					case 3:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_3);
						break;
					case 4:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_4);
						break;
					case 5:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_5);
						break;
					case 6:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_6);
						break;
					case 7:
						updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_worst_7);
						break;
				}
				break;
			default:
				updateViews.setImageViewResource(R.id.widget_today_state, R.drawable.widget_error);
				break;
		}
	
	    updateViews.setImageViewBitmap(R.id.widget_today_text, 
	    		Util.getFontBitmapToSetting(context, fontTypeFace, textDisplay[randomNumber], Constants.FONT_WIDGET_DEFAULT_SIZE,
	    				Constants.WIDGET_WIDTH_DIP, Constants.WIDGET_HEIGHT_DIP, fontColor));
	    
	    if(configPref.getInt(Constants.CONFIG_SELECT_LANGUAGE, 0) == 0) {
		    updateViews.setOnClickPendingIntent(R.id.widget_today_state, 			// 위젯에 클릭이 발생되었을 경우 음성인식 처리 다이얼로그 호출
					PendingIntent.getActivity(context, 0, new Intent(context, VoiceResultActivityDialog.class), 0));
	    } else {
	    	updateViews.setOnClickPendingIntent(R.id.widget_today_state, 			// 위젯에 클릭이 발생되었을 경우 음성인식 처리 다이얼로그 호출
	    			PendingIntent.getActivity(context, 0, new Intent(context, TodayFortune.class), 0));
	    }
	    
		updateViews.setOnClickPendingIntent(R.id.widget_today_text, 			// 위젯에 클릭이 발생되었을 경우 오늘의 운세 액티비티 호출
				PendingIntent.getActivity(context, 0, new Intent(context, TodayFortune.class), 0));
		
		
		appWidgetManager.updateAppWidget(appWidgetId, updateViews);		// 위젯 업데이트 매니저 호출
	}
	
	// 예약 되어 있는 알람을 취소하는 메소드
	public void removePreviousAlarm() {
		if(mAlarmManager != null && mPendingIntent != null) {
			mPendingIntent.cancel();
			mAlarmManager.cancel(mPendingIntent);
		}
	}

}
