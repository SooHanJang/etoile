package madcat.studio.fortune;

import java.util.ArrayList;
import java.util.Calendar;

import madcat.studio.adapter.MonthlyFortuneListAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MonthlyFortuneList extends ListActivity {

	private final String TAG										=	"MonthlyFortuneList";
	
	private Context mContext;
	private ArrayList<MonthlyFortuneData> mOrders;
	private MonthlyFortuneListAdapter mAdapter;
	private Calendar mThisMonthCalendar;
	
	private SharedPreferences mUserPref, mConfigPref;
	private ListView mListView;
	private TextView mTextTitle;
	private LinearLayout mLinearLayout;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_monthly_fortune_list);

		// 초기화 부분
		mContext = this;
		mOrders = new ArrayList<MonthlyFortuneData>();
		mUserPref = mContext.getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mListView = (ListView)findViewById(android.R.id.list);
		mTextTitle = (TextView)findViewById(R.id.monthly_list_title);
		mLinearLayout = (LinearLayout)findViewById(R.id.monthly_list_linear);
		
		// 글꼴 설정
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			mTextTitle.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
		} else {
			mTextTitle.setTypeface(Typeface.DEFAULT);
		}

		// 편지지 모양에 맞춰 설정하는 부분
//		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mLinearLayout.getLayoutParams();
//		
//		lp.topMargin = Constants.SCROLL_VIEW_MARGIN;
//		lp.leftMargin = Constants.SCROLL_VIEW_MARGIN;
//		lp.rightMargin = Constants.SCROLL_VIEW_MARGIN;
//		lp.bottomMargin = Constants.SCROLL_VIEW_MARGIN;
//		
//		mLinearLayout.setLayoutParams(lp);
		
		// 어댑터 설정 부분 (Async 로 처리)
		MonthlyFortuneListAsync monthlyListAsync = new MonthlyFortuneListAsync();
		monthlyListAsync.execute();
		
	}
	
	class MonthlyFortuneListAsync extends AsyncTask<Void, Void, Void> {

		ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", "잠시만 기다려주세요...");
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			getMonthlyListAdapter();
			return null;
		}
		
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
			
			mTextTitle.setText(Util.getYear() + ". " + Util.getMonthOfYear() + " " + getString(R.string.suffix_fortune) );
			
			mAdapter = new MonthlyFortuneListAdapter(mContext, R.layout.user_monthly_fortune_row, mOrders);
			setListAdapter(mAdapter);
			mListView.setSelectionFromTop(Integer.parseInt(Util.getTodayOfMonth()), Util.getDisplayCenterPixelHeight(MonthlyFortuneList.this));
		};
	}

	// 날짜와 요일을 넣어주는 과정
	private void getMonthlyListAdapter() {
		// 이번 달 날짜 설정
		mThisMonthCalendar = Calendar.getInstance();
		mThisMonthCalendar.set(Calendar.DAY_OF_MONTH, 1);
 
		int dayOfMonth;
		int thisMonthLastDay;
		
		dayOfMonth = mThisMonthCalendar.get(Calendar.DAY_OF_WEEK);							//	이번달의 시작일 요일
		thisMonthLastDay = mThisMonthCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);		//	이번달의 마지막 날짜
		
		String[] monthlyFortuneType = Util.getMonthlyFortuneType(mContext, mThisMonthCalendar.get(Calendar.YEAR), (mThisMonthCalendar.get(Calendar.MONTH) + 1));
		
		String[] monthlyFortuneText = new String[monthlyFortuneType.length];
		int[] monthlyFortuneWeather = new int[monthlyFortuneType.length];
		
		for(int i=0; i < monthlyFortuneType.length; i++) {
			monthlyFortuneText[i] = Util.getSendText(mContext, monthlyFortuneType[i])[0];
			monthlyFortuneWeather[i] = Util.getWidgetFortuneWeather(mContext, monthlyFortuneType[i]);
		}
		
		// 요일, 날짜를 설정하여 ArrayList 에 추가
		for(int i=1; i <= thisMonthLastDay; i++) {
			String textDayOfMonth = null;
	    	int tempDayOfMonth = ((i + dayOfMonth) - 1) % 7;
	    	
	    	if(tempDayOfMonth == 0) {
	    		tempDayOfMonth = 7;
	    	}
	    	
	    	switch(tempDayOfMonth) {
		    	case 1:
		    		textDayOfMonth = "일";
		    		break;
		    	case 2:
		    		textDayOfMonth = "월";
		    		break;
		    	case 3:
		    		textDayOfMonth = "화";
		    		break;
		    	case 4:
		    		textDayOfMonth = "수";
		    		break;
		    	case 5:
		    		textDayOfMonth = "목";
		    		break;
		    	case 6:
		    		textDayOfMonth = "금";
		    		break;
		    	case 7:
		    		textDayOfMonth = "토";
		    		break;
	    	}
	    	
	    	
	    	MonthlyFortuneData monthlyListData = new MonthlyFortuneData();
	    	monthlyListData.setDay(i);
	    	monthlyListData.setDayOfWeek(textDayOfMonth);
	    	monthlyListData.setFortuneText(monthlyFortuneText[i-1]);
	    	monthlyListData.setFortuneWeather(monthlyFortuneWeather[i-1]);
	    	mOrders.add(monthlyListData);
        }
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
