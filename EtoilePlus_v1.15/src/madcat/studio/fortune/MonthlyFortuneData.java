package madcat.studio.fortune;

public class MonthlyFortuneData {

	private int mDay;					//	朝楼
	private String mDayOfWeek;			//	夸老
	private String mFortuneText;		//	款技
	private int mFortuneWeather;		//	款技 朝揪
	
	public int getDay() {	return mDay;	}
	public void setDay(int day) {	this.mDay = day;	}
	
	public String getDayOfWeek() {	return mDayOfWeek;	}
	public void setDayOfWeek(String dayOfWeek) {	this.mDayOfWeek = dayOfWeek;	}
	
	public String getFortuneText() {	return mFortuneText;	}
	public void setFortuneText(String fortuneText) {	this.mFortuneText = fortuneText;	}
	
	public int getFortuneWeather() {	return mFortuneWeather;	}
	public void setFortuneWeather(int fortuneWeather)	{	this.mFortuneWeather = fortuneWeather;	}
}
