package madcat.studio.fortune;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class SNSWebView extends Activity {
	
//	private static final String TAG = "UploadSNS";
	
	private WebView mWebView;
	private String mUrl;
	private String mBody;
	
	private Context mContext;
	private Intent mIntent;
	
	private int mLoginFlag;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_upload_sns);
		
		mIntent = getIntent();
		mLoginFlag = mIntent.getExtras().getInt("LOGIN_FLAG");
		mContext = this;
		
		mWebView = (WebView)findViewById(R.id.upload_webView);
		mWebView.getSettings().setJavaScriptEnabled(true);
		
		if(mLoginFlag == Constants.ME2DAY_LOGIN_FLAG) {
			this.getMe2DayIntenter();
			
			mUrl = Constants.ME2DAY_URL + Constants.ME2DAY_BODY_PARAM + mBody + Constants.ME2DAY_SIGN + Constants.ME2DAY_TAG_PARAM + Constants.ME2DAY_TAG;
			
			mWebView.loadUrl(mUrl);
		    mWebView.setWebViewClient(new ConnWebViewClient());
		} else if(mLoginFlag == Constants.TWITTER_LOGIN_FLAG) {
			this.getTwitterIntenter();
			
			mWebView.addJavascriptInterface(new JavaScriptInterface(), "PINCODE");
			
			mWebView.loadUrl(mUrl);
			mWebView.setWebViewClient(new ConnWebViewClient());
		}
	}
	
	private class ConnWebViewClient extends WebViewClient {
		private ProgressDialog dialog;

    	public void onPageStarted(WebView view, String url, Bitmap favicon) {
    		if(dialog == null) {
	    		dialog = new ProgressDialog(mContext);
	    		dialog.setMessage(getString(R.string.progress_loading));
	    		dialog.show();
    		}
    	}
    	
    	public void onReceivedError(WebView view, int errorCode,
    			String description, String failingUrl) {
    		Toast.makeText(mContext, getString(R.string.toast_error_loading_page), Toast.LENGTH_SHORT).show();
    		if(dialog.isShowing()) {
    			dialog.dismiss();
    		}
    	}
    	
    	public void onPageFinished(WebView view, String url) {
    		if(mLoginFlag == Constants.TWITTER_LOGIN_FLAG) {
    			view.loadUrl("javascript:window.PINCODE.getPinCode(document.getElementById('oauth_pin').innerHTML);");
    		} 
    		
    		if(dialog.isShowing()) {
    			dialog.dismiss();
    		}
    	}
    }
	
	private void getMe2DayIntenter() {
		Intent intent = getIntent();
		mBody = intent.getExtras().getString("SNS_BODY");
	}
	
	private void getTwitterIntenter() {
		Intent intent = getIntent();
		mUrl = intent.getExtras().getString("REQUEST_URL");
	}

	@Override
	protected void onDestroy() {
//		Log.d(TAG, "Call onDestroy");
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
	
	class JavaScriptInterface {
		public void getPinCode(String pin) {
			if(pin.length() > 0) {
				
				String result_pin = pin.split("<code>")[1].trim().split("</code>")[0];
				
				Intent intent = getIntent();
				intent.putExtra("PIN_CODE", result_pin);
				setResult(RESULT_OK, intent);
				finish();
			} else {
				// pin code 를 실패했을 경우를 출력
			}
		}
	}
}
