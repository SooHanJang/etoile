package madcat.studio.fortune;

import madcat.studio.adapter.SNSAdapter;
import madcat.studio.calendar.CalendarFortune;
import madcat.studio.constant.Constants;
import madcat.studio.facebook.FaceBookUtil;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.http.AccessToken;
import twitter4j.http.OAuthAuthorization;
import twitter4j.http.RequestToken;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class TodayFortune extends Activity {
//	private static final String TAG = "Today Fortune";
	
	private ScrollView mScrollView;
	private TextView mTodayTextView, mFeelTextView, mHealthTextView, mMoneyTextView, mLoveTextView;
	private String mTodayLuck;
	private String mTodayText[], mTodaySendText[];
	private ImageView image_next_fortune;
	
	private Context mContext;
	
	private Twitter mTwitter;
	private AccessToken mAccessToken;
	private RequestToken mRequestToken;
	
	private SharedPreferences mUserPref, mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.user_today_fortune);
		
		mContext = this;

		mUserPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		mTodayTextView = (TextView)findViewById(R.id.today_fortune_text);
		mFeelTextView = (TextView)findViewById(R.id.today_feel_text);
		mHealthTextView = (TextView)findViewById(R.id.today_health_text);
		mMoneyTextView = (TextView)findViewById(R.id.today_money_text);
		mLoveTextView = (TextView)findViewById(R.id.today_love_text);
		mScrollView = (ScrollView)findViewById(R.id.today_fortune_scroll);
		image_next_fortune = (ImageView)findViewById(R.id.go_tomorrow_fortune);

		mTodayTextView.setBackgroundColor(0);
		mFeelTextView.setBackgroundColor(0);
		mHealthTextView.setBackgroundColor(0);
		mMoneyTextView.setBackgroundColor(0);
		mLoveTextView.setBackgroundColor(0);
		
		FortuneDate todayLunarDate = Util.getLunarDate(Util.TODAY);
		
//		RelativeLayout.LayoutParams setScrollMargin = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
//		setScrollMargin.setMargins(Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN);
//		mScrollView.setLayoutParams(setScrollMargin);
		
		try {
			mTodayLuck = Util.getEastTodayFortune(Util.getEastStarPosition(mUserPref.getInt(Constants.USER_LUNAR_MONTH, 0), mUserPref.getInt(Constants.USER_LUNAR_DAY, 0)), 
						Util.getEastStarPosition(todayLunarDate.getMonth(), todayLunarDate.getDay()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		// 글꼴 설정
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		mTodayTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mFeelTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mHealthTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mMoneyTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mLoveTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		mTodayText = Util.getFortuneText(mContext, mTodayLuck);
		mTodaySendText = Util.getSendText(mContext, mTodayLuck);
		
		float fontSize = (float)mConfigPref.getInt(Constants.CONFIG_FONT_SIZE, 3);
		
		mTodayTextView.setText(mTodayText[0]);
		mFeelTextView.setText(mTodayText[1]);
		mHealthTextView.setText(mTodayText[2]);
		mMoneyTextView.setText(mTodayText[3]);
		mLoveTextView.setText(mTodayText[4]);
		
		mTodayTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mFeelTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mHealthTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mMoneyTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mLoveTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		
		image_next_fortune.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(TodayFortune.this, TomorrowFortune.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		
		MenuItem item_1 = menu.add(0, Constants.MENU_UPLOAD_SNS, 0, getString(R.string.menu_item_sns));
		item_1.setIcon(R.drawable.context_menu_sns);
		
		MenuItem item_2 = menu.add(0, Constants.MENU_VIEW_LIST, 0, getString(R.string.menu_item_view_list));
		item_2.setIcon(R.drawable.context_menu_list);
		
		MenuItem item_3 = menu.add(0, Constants.MENU_VIEW_CALENDAR, 0 , getString(R.string.menu_item_view_calendar));
		item_3.setIcon(R.drawable.context_menu_calendar);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case Constants.MENU_UPLOAD_SNS:
//				Log.d(TAG, "sns에 올리기 클릭");
				showUploadSNSDialog();
				break;
			case Constants.MENU_VIEW_LIST:
				Intent list_intent = new Intent(TodayFortune.this, MonthlyFortuneList.class);
				startActivity(list_intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			case Constants.MENU_VIEW_CALENDAR:
				Intent calendar_intent = new Intent(TodayFortune.this, CalendarFortune.class);
				startActivity(calendar_intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				break;
			default :
				break;
		}
		return false;
	}
	
	public void showUploadSNSDialog() {
		final String items[] = {getString(R.string.sns_twitter), getString(R.string.sns_facebook), getString(R.string.sns_metoday)};
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.dialog_title_sns_list)).setCancelable(true).setAdapter(new SNSAdapter(getApplicationContext(), 
				R.layout.user_sns_display_row, items), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				switch(whichButton) {
				case 0:
//					Log.d(TAG, "트위터");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						
						if(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, "").equals("")
								|| userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, "").equals("") 
								|| userPref.getString(Constants.TWITTER_PREFERENCE_PIN_CODE, "").equals("")) {
							twitterLogin();	// 로그인
						} else {
							twitterPostDialog();
						}
					}
					break;
				case 1:
//					Log.d(TAG, "페이스북");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						if (Constants.FACEBOOK_APP_ID == null) {
				            FaceBookUtil.showAlert(getApplicationContext(), "Warning", "Facebook Applicaton ID must be " +
				                    "specified before running this example: see Example.java");
				        }
						
						FaceBookLogin faceBookLogin = new FaceBookLogin(mContext, TodayFortune.this, getString(R.string.title_today_fortune) + mTodaySendText[0]);
						faceBookLogin.login();
					}
			        
					break;
				case 2:
//					Log.d(TAG, "미투데이");
					
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(TodayFortune.this, SNSWebView.class);
						intent.putExtra("SNS_BODY", getString(R.string.title_today_fortune) + mTodaySendText[0]);
						intent.putExtra("LOGIN_FLAG", Constants.ME2DAY_LOGIN_FLAG);
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
					}
					break;
				}
			}
		});
		
		builder.create().show();
	}
	
	
	private void twitterLogin() {
		mTwitter = new TwitterFactory().getInstance();
    	mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
    	
    	mRequestToken = null;

		try {
	    	mRequestToken = mTwitter.getOAuthRequestToken();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	
		Intent intent = new Intent(TodayFortune.this, SNSWebView.class);
		intent.putExtra("REQUEST_URL", mRequestToken.getAuthorizationURL());
		intent.putExtra("LOGIN_FLAG", Constants.TWITTER_LOGIN_FLAG);
		startActivityForResult(intent, Constants.TWITTER_LOGIN_FLAG);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
	
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	
	    	if(resultCode == RESULT_OK) {		// 서브 액티비티가 정상적으로 종료 되었다면
	    		if(requestCode == Constants.TWITTER_LOGIN_FLAG) {
	    			// 서브 액티비티에서 받아온 결과값을 이용해 처리
	    			
//	    			Log.d(TAG, "결과값 : " + data.getStringExtra("PIN_CODE"));
	    			
	    			try {
						mAccessToken = mTwitter.getOAuthAccessToken(mRequestToken, data.getStringExtra("PIN_CODE"));
						mTwitter.setOAuthAccessToken(mAccessToken);
						
						//로그인을 한번만 하기 위해, PINCODE, AccessToken, AccessSecretToken 저장
						
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = userPref.edit();
						
						editor.putString(Constants.TWITTER_PREFERENCE_PIN_CODE, data.getStringExtra("PIN_CODE"));
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, mAccessToken.getToken());
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, mAccessToken.getTokenSecret());
						
						editor.commit();
						
						twitterPostDialog();
					} catch (TwitterException e) {
						e.printStackTrace();
					}
	    		}
	    	}
	    }

    private void twitterPostDialog() {
    	final EditText fortune_text = new EditText(mContext);
		fortune_text.setText(getString(R.string.title_today_fortune) + mTodaySendText[0].toString());
		 
		AlertDialog.Builder builder =  new AlertDialog.Builder(mContext);
		builder.setTitle(getString(R.string.dialog_title_sns_registe_twitter)).setView(fortune_text)
		    .setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	TwitterLoadingAsync twitterLoading = new TwitterLoadingAsync();
		    	twitterLoading.execute(fortune_text.getText().toString());
		    	
		    }
		    }).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
			}
		}).create().show();
    }
    
    private class TwitterLoadingAsync extends AsyncTask<String, Void, Void> {
    	
    	SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
    	ProgressDialog loading;
    	int twitterErrorCheck = 1;
    	
    	@Override
    	protected Void doInBackground(String... strData) {
    		try {
	    		 mAccessToken = new AccessToken(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, ""), 
	    				 userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, ""));
	    		
	    		
	    		 ConfigurationBuilder cb = new ConfigurationBuilder();
	    	     cb.setOAuthAccessToken(mAccessToken.getToken());
	    	     cb.setOAuthAccessTokenSecret(mAccessToken.getTokenSecret());
	    	     cb.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
	    	     cb.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
	    	     Configuration config = cb.build();
	    	     OAuthAuthorization auth = new OAuthAuthorization(config);
	    	      
	    	     TwitterFactory tFactory = new TwitterFactory(config);
	    	     Twitter twitter = tFactory.getInstance();

	    	     twitter.updateStatus(strData[0].toString());
			} catch (TwitterException e) {
				twitterErrorCheck = 2;
			}
			
    		return null;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		loading = ProgressDialog.show(mContext, "", getString(R.string.progress_upload_post));
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		loading.dismiss();
    		
    		if(twitterErrorCheck == 2) {
    			Toast.makeText(mContext, getString(R.string.toast_caution_incorrect_post), Toast.LENGTH_SHORT).show();
    		} else {
    			Toast.makeText(mContext, getString(R.string.toast_succeed_registe_post), Toast.LENGTH_SHORT).show();
    		}
    	}
    }
	
	@Override
	protected void onDestroy() {
//		Log.d(TAG, "Call onDestroy");
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
}
