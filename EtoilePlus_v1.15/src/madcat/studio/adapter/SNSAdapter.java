package madcat.studio.adapter;

import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SNSAdapter extends ArrayAdapter<String> {
	
//	private static final String TAG = "SNSAdapter";
	
	private String[] mItems;
	private Context mContext;
	private SharedPreferences mConfigPref;
	private String mFontAssetUrl;

	public SNSAdapter(Context context, int textViewResourceId, String[] items) {
		super(context, textViewResourceId, items);
		this.mItems = items;
		this.mContext = context;
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontAssetUrl = Constants.FONT_PREFIX_PATH + 
			Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.user_sns_display_row, null);
		} 
		
		String stringOrder = mItems[position];
		
		if(stringOrder != null) {
			
			ImageView imageView = (ImageView)v.findViewById(R.id.sns_image);
			TextView textView = (TextView)v.findViewById(R.id.sns_title);
			
			if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
				textView.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
			} else {
				textView.setTypeface(Typeface.DEFAULT);
			}
			
			if(stringOrder.equals(mContext.getResources().getString(R.string.sns_twitter))) {
				imageView.setBackgroundResource(R.drawable.twitter_icon);
				textView.setText(mContext.getResources().getString(R.string.sns_twitter));
			} else if(stringOrder.equals(mContext.getResources().getString(R.string.sns_facebook))) {
				imageView.setBackgroundResource(R.drawable.facebook_icon);
				textView.setText(mContext.getResources().getString(R.string.sns_facebook));
			} else if(stringOrder.equals(mContext.getResources().getString(R.string.sns_metoday))) {
				imageView.setBackgroundResource(R.drawable.me2day_icon);
				textView.setText(mContext.getResources().getString(R.string.sns_metoday));
			}
		}
		
		return v;
	}
}
