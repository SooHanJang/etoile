package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constant.Constants;
import madcat.studio.friends.Friends;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FriendsAdapter extends ArrayAdapter<Friends> {
	
//	private static final String TAG = "FriendsAdapter";
	
	private ArrayList<Friends> mItems;
	private Context mContext;
	private View mConvertView;
	private int mFlag;
	private SharedPreferences mConfigPref;
	private String mFontAssetUrl;

	public FriendsAdapter(Context context, int textViewResourceId, ArrayList<Friends> items, int flag) {
		super(context, textViewResourceId, items);
		this.mItems = items;
		this.mContext = context;
		this.mFlag = flag;
		
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontAssetUrl = Constants.FONT_PREFIX_PATH + 
			Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.setting_friends_row, null);
			v.setBackgroundResource(R.drawable.com_list_selector);
		} 
		
		Friends friends = mItems.get(position);
		
		if(friends != null) {
			
			TextView tt = (TextView)v.findViewById(R.id.toptext);
			TextView bt = (TextView)v.findViewById(R.id.bottomtext);
			ImageView iv = (ImageView)v.findViewById(R.id.friend_image);

			if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
				tt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
				bt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
			} else {
				tt.setTypeface(Typeface.DEFAULT);
				bt.setTypeface(Typeface.DEFAULT);
			}
			
			if(mFlag == Constants.FLAG_FRIENDS_LIST_SEARCH) {		// 친구 목록을 호출 할 때 사용되는 ListView 설정
			
				if(tt != null) {
					if(friends.getGender() == 1) {		// 남자
						tt.setText(friends.getName());
						iv.setBackgroundResource(R.drawable.boy_icon);
					} else {		// 여자
						tt.setText(friends.getName());
						iv.setBackgroundResource(R.drawable.girl_icon);
					}
					
				}
					
				if(bt != null) {
					if(friends.getBirthType() == 1) {		// 양력이면
						bt.setText(friends.getSolarYear() + "/" + friends.getSolarMonth() + "/" + 
								friends.getSolarDay() + " (" + mContext.getResources().getString(R.string.setting_solar) + ")");
					} else {		//음력이면
						bt.setText(friends.getLunarYear() + "/" + friends.getLunarMonth() + "/" + 
								friends.getLunarDay() + " (" + mContext.getResources().getString(R.string.setting_lunar) + ")");
					}
				}
			} else if(mFlag == Constants.FLAG_FRIENDS_TODAYFORTUNE_SEARCH) {
				v.setBackgroundColor(Color.WHITE);
				v.setBackgroundResource(R.drawable.com_list_selector);
				
				if(tt != null) {
					tt.setTextColor(Color.BLACK);
					
					if(friends.getGender() == 1) {		// 남자
						tt.setText(friends.getName());
						iv.setBackgroundResource(R.drawable.boy_icon);
					} else {		// 여자
						tt.setText(friends.getName());
						iv.setBackgroundResource(R.drawable.girl_icon);
					}
					
				}
					
				if(bt != null) {
					bt.setTextColor(Color.BLACK);
					
					if(friends.getBirthType() == 1) {		// 양력이면
						bt.setText(friends.getSolarYear() + "/" + friends.getSolarMonth() + "/" + 
								friends.getSolarDay() + " (" + mContext.getResources().getString(R.string.setting_solar) + ")");
					} else {		//음력이면
						bt.setText(friends.getLunarYear() + "/" + friends.getLunarMonth() + "/" + 
								friends.getLunarDay() + " (" + mContext.getResources().getString(R.string.setting_lunar) + ")");
					}
				}
			} else if(mFlag == Constants.FLAG_FRIENDS_PHONE_SEARCH) {	// 친구 전화번호를 호출 할 때 사용되는 ListView 설명
				v.setBackgroundColor(Color.WHITE);
				v.setBackgroundResource(R.drawable.com_list_selector);
				
				if(tt != null) {
					tt.setText(friends.getName());
					tt.setTextColor(Color.BLACK);
				}
				
//				Log.d(TAG, "음? : " + friends.getGender());
				
				iv.setBackgroundResource(R.drawable.people_icon);
				
				if(bt != null) {
					bt.setText("Tel : " + friends.getPhoneNumber());
					bt.setTextColor(Color.BLACK);
				}
			}
		}
		
		return v;
	}
}
