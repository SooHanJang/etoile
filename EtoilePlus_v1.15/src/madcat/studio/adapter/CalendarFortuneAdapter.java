package madcat.studio.adapter;

import java.util.ArrayList;
import java.util.Calendar;

import madcat.studio.calendar.DayInfo;
import madcat.studio.constant.Constants;
import madcat.studio.main.*;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CalendarFortuneAdapter extends ArrayAdapter<DayInfo> {
	
	private final String TAG										=	"CalendarAdapter";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE; 
	
	private Context mContext;
	private ArrayList<DayInfo> mItems;
	private LayoutInflater mLayoutInflater;
	private int mResourceId;
	private SharedPreferences mConfigPref;
	private String mFontAssetUrl;
	private Calendar mCalendar;
	
	// 오늘날짜 View 관련 변수
	private View mTodayView;
	
	public CalendarFortuneAdapter(Context context, int textViewResourceId, ArrayList<DayInfo> items) {
		super(context, textViewResourceId, items);
		
		mContext = context;
		mItems = items;
		mResourceId = textViewResourceId;
		
		mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontAssetUrl = Constants.FONT_PREFIX_PATH + 
			Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		mCalendar = Calendar.getInstance();
	}
	
	public class DayViewHolder {
		RelativeLayout relativeBackground;
		TextView dayText;
		ImageView imageWeather;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		DayInfo day = mItems.get(position);
		DayViewHolder holder;
		
		if(v == null) {
			v = mLayoutInflater.inflate(mResourceId, null);
			
			if(position % 7 == 6) {
                v.setLayoutParams(new GridView.LayoutParams(getCellWidthDP() + getRestCellWidthDP(), getCellHeightDP()));
            } else {
                v.setLayoutParams(new GridView.LayoutParams(getCellWidthDP(), getCellHeightDP()));    
            }
			
			holder = new DayViewHolder();
			 
			holder.relativeBackground = (RelativeLayout)v.findViewById(R.id.day_cell_relative_background);
			holder.dayText = (TextView)v.findViewById(R.id.day_cell_tv_day);
			holder.imageWeather = (ImageView)v.findViewById(R.id.day_cell_image);
			
			if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
				holder.dayText.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
			} else {
				holder.dayText.setTypeface(Typeface.DEFAULT);
			}
			
			v.setTag(holder);
		} else {
			holder = (DayViewHolder)v.getTag();
		}
		
		if(day != null) {
			if((Integer.parseInt(day.getYear()) == mCalendar.get(Calendar.YEAR)) &&
					(Integer.parseInt(day.getMonth()) == (mCalendar.get(Calendar.MONTH) + 1)) &&
					(Integer.parseInt(day.getDay()) == mCalendar.get(Calendar.DAY_OF_MONTH))) {
				holder.relativeBackground.setBackgroundResource(R.drawable.calendar_bg_fill_day);

				mTodayView = holder.relativeBackground;
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "TodayView : " + mTodayView);
				}
				
			} else {
				holder.relativeBackground.setBackgroundResource(R.drawable.calendar_bg_day);
			}
			
            holder.dayText.setText(day.getDay());
 
            if(day.isInMonth()) {
                if(position % 7 == 0) {
                	holder.dayText.setTextColor(Color.RED);
                } else if(position % 7 == 6) {
                	holder.dayText.setTextColor(Color.BLUE);
                } else {
                	holder.dayText.setTextColor(Color.BLACK);
                }
                
                switch(day.getWeather()) {
	                case Constants.WIDGET_WEATHER_BEST:
	                	holder.imageWeather.setBackgroundResource(R.drawable.widget_best_0);
						break;
					case Constants.WIDGET_WEATHER_GOOD:
						holder.imageWeather.setBackgroundResource(R.drawable.widget_good_0);
						break;
					case Constants.WIDGET_WEATHER_SOSO:
						holder.imageWeather.setBackgroundResource(R.drawable.widget_soso_0);
						break;
					case Constants.WIDGET_WEATHER_BAD:
						holder.imageWeather.setBackgroundResource(R.drawable.widget_bad_0);
						break;
					case Constants.WIDGET_WEATHER_WORST:
						holder.imageWeather.setBackgroundResource(R.drawable.widget_worst_0);
						break;
					default:
						holder.imageWeather.setBackgroundResource(R.drawable.widget_error);
						break;
                }
                
            }else {
            	holder.dayText.setTextColor(Color.GRAY);
            }
        }
        
        return v;
	}
	
	private int getCellWidthDP() {
		int width = mContext.getResources().getDisplayMetrics().widthPixels;
//        int cellWidth = 480/7;
		int cellWidth = width / 7;
        
//        Log.d(TAG, "getCellWidthDp width : " + width + ", 고정 값 : " + cellWidth);
         
        return cellWidth;
    }
     
    private int getRestCellWidthDP() {
    	int width = mContext.getResources().getDisplayMetrics().widthPixels;
//        int cellWidth = 480%7;
    	
    	int cellWidth = width % 7;
        
//        Log.d(TAG, "getResetCellWidthDp width : " + width + ", 고정 값 : " + cellWidth);
         
        return cellWidth;
    }
     
    private int getCellHeightDP() {
    	int height = mContext.getResources().getDisplayMetrics().widthPixels;
//        int cellHeight = 480/6;
    	
    	int cellHeight = height / 6;
        
//        Log.d(TAG, "getCellHeightDP height : " + height + ", 고정 값 : " + cellHeight);
         
        return cellHeight;
    }
    
    public View getCurrentTodayView() {
    	return mTodayView;
    }
    
    

}
