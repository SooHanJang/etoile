package madcat.studio.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import madcat.studio.constant.Constants;
import madcat.studio.fortune.MonthlyFortuneData;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MonthlyFortuneListAdapter extends ArrayAdapter<MonthlyFortuneData> {

//	private final String TAG										=	"MonthlyFortuneListAdapter";
	
	private Context mContext;
	private ArrayList<MonthlyFortuneData> mItems;
	private SharedPreferences mConfigPref;
	private String mFontAssetUrl;
	
	public MonthlyFortuneListAdapter(Context context, int textViewResourceId, ArrayList<MonthlyFortuneData> items) {
		super(context, textViewResourceId, items);
		mContext = context;
		mItems = items;
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontAssetUrl = Constants.FONT_PREFIX_PATH + 
			Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
	}

	public class ViewHolder {
		LinearLayout rootLinear;
		TextView dayText;
		TextView fortuneText;
		ImageView fortuneWeahter;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder = null;
		
		if(v == null) {
			holder = new ViewHolder();
			LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.user_monthly_fortune_row, null);
			
			holder.rootLinear = (LinearLayout)v.findViewById(R.id.monthly_list_root_linear);
			holder.dayText = (TextView)v.findViewById(R.id.monthly_list_day);
			holder.fortuneText = (TextView)v.findViewById(R.id.monthly_list_fortune);
			holder.fortuneWeahter = (ImageView)v.findViewById(R.id.monthly_list_image_weather);
			
			if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
				holder.dayText.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
				holder.fortuneText.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
			} else {
				holder.dayText.setTypeface(Typeface.DEFAULT);
				holder.fortuneText.setTypeface(Typeface.DEFAULT);
			}
			
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		MonthlyFortuneData mData = mItems.get(position);
		
		if(mData != null) {
			
			if(mData.getDay() == Integer.parseInt(Util.getTodayOfMonth())) {
				holder.rootLinear.setBackgroundColor(Color.rgb(255, 136, 52));
			} else {
				holder.rootLinear.setBackgroundColor(Color.argb(0, 0, 0, 0));
			}
			
			switch(mData.getFortuneWeather()) {
	            case Constants.WIDGET_WEATHER_BEST:
	            	holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_best_0);
					break;
				case Constants.WIDGET_WEATHER_GOOD:
					holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_good_0);
					break;
				case Constants.WIDGET_WEATHER_SOSO:
					holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_soso_0);
					break;
				case Constants.WIDGET_WEATHER_BAD:
					holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_bad_0);
					break;
				case Constants.WIDGET_WEATHER_WORST:
					holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_worst_0);
					break;
				default:
					holder.fortuneWeahter.setBackgroundResource(R.drawable.widget_error);
					break;
			}
			
			holder.dayText.setText(mData.getDay() + "(" + mData.getDayOfWeek() + ")");
			holder.fortuneText.setText(mData.getFortuneText());
		}
		
		return v;
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}
	
	@Override
	public boolean isEnabled(int position) {
		return false;
	}
}
