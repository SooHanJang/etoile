package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constant.Constants;
import madcat.studio.friends.Friends;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class FriendsDelAdapter extends ArrayAdapter<Friends> {
	
//	private static final String TAG = "FriendsDelAdapter";
	
	private ArrayList<Friends> mItems;
	
	private ArrayList<Friends> mDelItems;
	private ArrayList<Integer> mListItems;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	private String mFontAssetUrl;
	

	public FriendsDelAdapter(Context context, int textViewResourceId, ArrayList<Friends> items) {
		super(context, textViewResourceId, items);
		this.mItems = items;
		this.mContext = context;
		this.mDelItems = new ArrayList<Friends>();
		this.mListItems = new ArrayList<Integer>();
		
		mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		mFontAssetUrl = Constants.FONT_PREFIX_PATH + 
			Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		final int checkPosition = position;
		
		if(v == null) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.setting_friends_del_row, null);
		} 
		
		Friends friends = mItems.get(position);
		
		if(friends != null) {
			TextView del_tt = (TextView)v.findViewById(R.id.del_toptext);
			TextView del_bt = (TextView)v.findViewById(R.id.del_bottomtext);
			CheckBox del_check = (CheckBox)v.findViewById(R.id.del_check);
			ImageView del_image = (ImageView)v.findViewById(R.id.del_friend_image);
			
			if(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
				del_tt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
				del_bt.setTypeface(Typeface.createFromAsset(mContext.getAssets(), mFontAssetUrl));
			} else {
				del_tt.setTypeface(Typeface.DEFAULT);
				del_bt.setTypeface(Typeface.DEFAULT);
			}
			
			if(del_tt != null) {
				if(friends.getGender() == 1) {		// 남자
					del_tt.setText(friends.getName());
					del_image.setBackgroundResource(R.drawable.boy_icon);
				} else {		// 여자
					del_tt.setText(friends.getName());
					del_image.setBackgroundResource(R.drawable.girl_icon);
				}
				
			}
			
			if(del_bt != null) {
				if(friends.getBirthType() == 1) {		// 양력이면
					del_bt.setText(friends.getSolarYear() + "/" + friends.getSolarMonth() + "/" + 
							friends.getSolarDay() + " (" + mContext.getResources().getString(R.string.setting_solar) + ")");
				} else {		//음력이면
					del_bt.setText(friends.getLunarYear() + "/" + friends.getLunarMonth() + "/" + 
							friends.getLunarDay() + " (" + mContext.getResources().getString(R.string.setting_lunar) + ")");
				}
			}
			
			del_check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked) {
						for(int i=0; i < mListItems.size(); i++) {
							if(mListItems.get(i) == checkPosition) {
								return;
							}
						}
//						Log.d(TAG, "checkPosition : " + checkPosition + ", position : " + position);
						mListItems.add(checkPosition);
						mDelItems.add(mItems.get(checkPosition));
//						mDelItems.add(mItems.get(position));
					} else {
						for(int i=0; i < mListItems.size(); i++) {
							if(mListItems.get(i) == checkPosition) {
								mListItems.remove(i);
								mDelItems.remove(mItems.get(position));
								break;
							}
						}
//						mDelItems.remove(mItems.get(position));
					}
				}
			});
			
			boolean reChecked = false;
			for(int i=0; i < mListItems.size(); i++) {
				if(mListItems.get(i) == checkPosition) {
					del_check.setChecked(true);
					reChecked = true;
					break;
				}
			}
			
			if(!reChecked) {
				del_check.setChecked(false);
			}
		}
		
		return v;
	}
	
	public ArrayList<Friends> getCheckedList() {
		return mDelItems;
	}
}
