package madcat.studio.friends;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import madcat.studio.adapter.SNSAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.facebook.FaceBookUtil;
import madcat.studio.fortune.FaceBookLogin;
import madcat.studio.fortune.SNSWebView;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.KakaoLink;
import madcat.studio.utils.Util;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.http.AccessToken;
import twitter4j.http.OAuthAuthorization;
import twitter4j.http.RequestToken;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class FriendsTodayFortune extends Activity {
	
	private final String TAG										=	"FriendsTodayFortune";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private ScrollView mScrollView;
	private TextView mTodayTextView, mFeelTextView, mHealthTextView, mMoneyTextView, mLoveTextView;
	private TextView mTextView;
	private String mTodayLuck;
	
	private String mTodayText[], mTodaySendText[];
	
	private ImageView image_next_fortune;
	
	private String mName;
	private int mLunarMonth, mLunarDay;
	
	private Context mContext;
	
	private Twitter mTwitter;
	private AccessToken mAccessToken;
	private RequestToken mRequestToken;
	
	private KakaoLink mLink;
	
	private SharedPreferences mConfigPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.friends_today_fortune);
		this.mContext = this;
		
		mConfigPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);

		mTodayTextView = (TextView)findViewById(R.id.friends_today_fortune_text);
		mFeelTextView = (TextView)findViewById(R.id.friends_today_feel_text);
		mHealthTextView = (TextView)findViewById(R.id.friends_today_health_text);
		mMoneyTextView = (TextView)findViewById(R.id.friends_today_money_text);
		mLoveTextView = (TextView)findViewById(R.id.friends_today_love_text);
		mScrollView = (ScrollView)findViewById(R.id.friends_today_fortune_scroll);
		image_next_fortune = (ImageView)findViewById(R.id.go_tomorrow_fortune);

		mTodayTextView.setBackgroundColor(0);
		mFeelTextView.setBackgroundColor(0);
		mHealthTextView.setBackgroundColor(0);
		mMoneyTextView.setBackgroundColor(0);
		mLoveTextView.setBackgroundColor(0);

		// 글꼴 설정
		if(DEVELOPE_MODE) {
			Log.d(TAG, "글꼴 설정");
		}
		
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		mTextView = (TextView)findViewById(R.id.friends_today_fortune_name);
		mTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		
		FortuneDate todayLunarDate = Util.getLunarDate(Util.TODAY);
		getIntenter();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "친구의 오늘 운세 음력 월, 일 : " + mLunarMonth + ", " + mLunarDay);
		}
		
//		RelativeLayout.LayoutParams setScrollMargin = (RelativeLayout.LayoutParams) mScrollView.getLayoutParams();
//		setScrollMargin.setMargins(Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN, Constants.SCROLL_VIEW_MARGIN);
//		mScrollView.setLayoutParams(setScrollMargin);
		
		try {
			mTodayLuck = Util.getEastTodayFortune(Util.getEastStarPosition(mLunarMonth, mLunarDay), 
						Util.getEastStarPosition(todayLunarDate.getMonth(), todayLunarDate.getDay()));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		// 글꼴 설정
		mTodayTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mFeelTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mHealthTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mMoneyTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
		mLoveTextView.setTypeface(Util.getFontType(mContext, fontAssetUrl, mConfigPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
//		Log.d(TAG, "친구의 오늘 운세 : " + mTodayLuck);
		
		mTodayText = Util.getFortuneText(mContext, mTodayLuck);
		mTodaySendText = Util.getSendText(mContext, mTodayLuck);
		
		mTextView.setText(mName + getString(R.string.suffix_today_fortune));
		
		float fontSize = (float)mConfigPref.getInt(Constants.CONFIG_FONT_SIZE, 3);
		
		mTodayTextView.setText(mTodayText[0]);
		mFeelTextView.setText(mTodayText[1]);
		mHealthTextView.setText(mTodayText[2]);
		mMoneyTextView.setText(mTodayText[3]);
		mLoveTextView.setText(mTodayText[4]);
		
		mTodayTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mFeelTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mHealthTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mMoneyTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		mLoveTextView.setTextSize(fontSize + Constants.FONT_PLUS_SIZE);
		
		image_next_fortune.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
//				Log.d(TAG, mName + ", " + mLunarMonth + ", " + mLunarDay);
				Intent intent = new Intent(FriendsTodayFortune.this, FriendsTomorrowFortune.class);
				intent.putExtra("FRIENDS_NAME", mName);
				intent.putExtra("FRIENDS_LUNAR_MONTH", mLunarMonth);
				intent.putExtra("FRIENDS_LUNAR_DAY", mLunarDay);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
				finish();
			}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		
		MenuItem item = menu.add(0, Constants.MENU_UPLOAD_SNS, 0, getString(R.string.menu_item_sns));
		item.setIcon(R.drawable.post_icon);
		MenuItem item_1 = menu.add(0, Constants.MENU_SEND_SMS, 0, getString(R.string.menu_item_sms));
		item_1.setIcon(R.drawable.mail_icon);
		MenuItem item_2 = menu.add(0, Constants.MENU_SEND_KAKAO, 0, getString(R.string.menu_item_kakaotalk));
		item_2.setIcon(R.drawable.kakaotalkicon_3535);
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case Constants.MENU_UPLOAD_SNS:
				showUploadSNSDialog();
				break;
			case Constants.MENU_SEND_SMS:
				sendSMSFortune();
				break;
			case Constants.MENU_SEND_KAKAO:
				sendKakaoFortune();
				break;
			default :
				break;
		}
		return false;
	}
	
	private void sendKakaoFortune() {
		try {
			String sendMsg = mName + getString(R.string.suffix_today_fortune_s) + mTodaySendText[0].toString();
			
			 ArrayList<Map<String, String>> arrMetaInfo = new ArrayList<Map<String, String>>();

		        // If application is support Android platform. 
		        Map <String, String> metaInfoAndroid = new Hashtable <String, String>(1);
		        metaInfoAndroid.put("os", "android");
		        metaInfoAndroid.put("devicetype", "phone");
		        metaInfoAndroid.put("installurl", Constants.KAKAO_STR_INSTALL_URL);
		        metaInfoAndroid.put("executeurl", "etoile://");
		        arrMetaInfo.add(metaInfoAndroid);

		        mLink = new KakaoLink(mContext, Constants.KAKAO_STR_URL, Constants.KAKAO_STR_APPID, 
						Constants.KAKAO_STR_APPVER, sendMsg, Constants.KAKAO_STR_APPNAME, arrMetaInfo, "UTF-8");
			if( mLink.isAvailable() ) {
	            startActivity(mLink.getIntent());
	        } else {
	        	Toast.makeText(mContext, getString(R.string.toast_caution_not_kakaotalk), Toast.LENGTH_SHORT).show();
	        }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	private void sendSMSFortune() {

		final Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.putExtra("sms_body", mName + getString(R.string.suffix_today_fortune_s) + mTodaySendText[0].toString());

		intent.setType("vnd.android-dir/mms-sms");
		startActivity(intent);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
	public void showUploadSNSDialog() {
		final String items[] = {getString(R.string.sns_twitter), getString(R.string.sns_facebook), getString(R.string.sns_metoday)};
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.dialog_title_sns_list)).setCancelable(true).setAdapter(new SNSAdapter(getApplicationContext(), 
				R.layout.user_sns_display_row, items), new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int whichButton) {
				switch(whichButton) {
				case 0:
//					Log.d(TAG, "트위터");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						
						if(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, "").equals("")
								|| userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, "").equals("") 
								|| userPref.getString(Constants.TWITTER_PREFERENCE_PIN_CODE, "").equals("")) {
							twitterLogin();	// 로그인
						} else {
							twitterPostDialog();
						}
					}
					break;
				case 1:
//					Log.d(TAG, "페이스북");
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						if (Constants.FACEBOOK_APP_ID == null) {
				            FaceBookUtil.showAlert(getApplicationContext(), "Warning", "Facebook Applicaton ID must be " +
				                    "specified before running this example: see Example.java");
				        }
						
						FaceBookLogin faceBookLogin = new FaceBookLogin(mContext, 
								FriendsTodayFortune.this, mName + getString(R.string.suffix_today_fortune_s) + mTodaySendText[0].toString());
						faceBookLogin.login();
					}
			        
					break;
				case 2:
//					Log.d(TAG, "미투데이");
					
					if(!Util.isWifiAvailable(getApplicationContext()) && !Util.is3GAvailable(getApplicationContext())) {
						Toast.makeText(getApplicationContext(), getString(R.string.toast_caution_check_wifi_3g), Toast.LENGTH_SHORT).show();
					} else {
						Intent intent = new Intent(FriendsTodayFortune.this, SNSWebView.class);
						intent.putExtra("SNS_BODY", mName + getString(R.string.suffix_today_fortune_s) + mTodaySendText[0].toString());
						intent.putExtra("LOGIN_FLAG", Constants.ME2DAY_LOGIN_FLAG);
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
					}
					break;
				}
			}
		});
		
		builder.create().show();
	}
	
	
	private void twitterLogin() {
		mTwitter = new TwitterFactory().getInstance();
    	mTwitter.setOAuthConsumer(Constants.TWITTER_CONSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET);
    	
    	mRequestToken = null;

		try {
	    	mRequestToken = mTwitter.getOAuthRequestToken();
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	
		Intent intent = new Intent(FriendsTodayFortune.this, SNSWebView.class);
		intent.putExtra("REQUEST_URL", mRequestToken.getAuthorizationURL());
		intent.putExtra("LOGIN_FLAG", Constants.TWITTER_LOGIN_FLAG);
		startActivityForResult(intent, Constants.TWITTER_LOGIN_FLAG);
		overridePendingTransition(R.anim.fade, R.anim.hold);
	}
	
    
	 @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	
	    	if(resultCode == RESULT_OK) {		// 서브 액티비티가 정상적으로 종료 되었다면
	    		if(requestCode == Constants.TWITTER_LOGIN_FLAG) {
	    			// 서브 액티비티에서 받아온 결과값을 이용해 처리
	    			
//	    			Log.d(TAG, "결과값 : " + data.getStringExtra("PIN_CODE"));
	    			
	    			try {
						mAccessToken = mTwitter.getOAuthAccessToken(mRequestToken, data.getStringExtra("PIN_CODE"));
						mTwitter.setOAuthAccessToken(mAccessToken);
						
						//로그인을 한번만 하기 위해, PINCODE, AccessToken, AccessSecretToken 저장
						
						SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = userPref.edit();
						
						editor.putString(Constants.TWITTER_PREFERENCE_PIN_CODE, data.getStringExtra("PIN_CODE"));
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, mAccessToken.getToken());
						editor.putString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, mAccessToken.getTokenSecret());
						
						editor.commit();
						
						twitterPostDialog();
					} catch (TwitterException e) {
						e.printStackTrace();
					}
	    		}
	    	}
	    }

    private void twitterPostDialog() {
    	final EditText fortune_text = new EditText(mContext);
		fortune_text.setText(mName + getString(R.string.suffix_today_fortune_s) + mTodaySendText[0].toString());
		 
		AlertDialog.Builder builder =  new AlertDialog.Builder(mContext);
		builder.setTitle(getString(R.string.dialog_title_sns_registe_twitter)).setView(fortune_text)
		    .setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int whichButton) {
		    	
		    	TwitterLoadingAsync twitterLoading = new TwitterLoadingAsync();
		    	twitterLoading.execute(fortune_text.getText().toString());
		    	
		    }
		    }).setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
			}
		}).create().show();
    }
    
    private class TwitterLoadingAsync extends AsyncTask<String, Void, Void> {
    	
    	SharedPreferences userPref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
    	ProgressDialog loading;
    	int twitterErrorCheck = 1;
    	
    	@Override
    	protected Void doInBackground(String... strData) {
    		try {
	    		 mAccessToken = new AccessToken(userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_TOKEN, ""), 
	    				 userPref.getString(Constants.TWITTER_PREFERENCE_ACCESS_SECRET, ""));
	    		
	    		
	    		 ConfigurationBuilder cb = new ConfigurationBuilder();
	    	     cb.setOAuthAccessToken(mAccessToken.getToken());
	    	     cb.setOAuthAccessTokenSecret(mAccessToken.getTokenSecret());
	    	     cb.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
	    	     cb.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
	    	     Configuration config = cb.build();
	    	     OAuthAuthorization auth = new OAuthAuthorization(config);
	    	      
	    	     TwitterFactory tFactory = new TwitterFactory(config);
	    	     Twitter twitter = tFactory.getInstance();

	    	     twitter.updateStatus(strData[0].toString());
			} catch (TwitterException e) {
				twitterErrorCheck = 2;
			}
			
    		return null;
    	}
    	
    	@Override
    	protected void onPreExecute() {
    		loading = ProgressDialog.show(mContext, "", getString(R.string.progress_upload_post));
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		loading.dismiss();
    		
    		if(twitterErrorCheck == 2) {
    			Toast.makeText(mContext, getString(R.string.toast_caution_incorrect_post), Toast.LENGTH_SHORT).show();
    		} else {
    			Toast.makeText(mContext, getString(R.string.toast_succeed_registe_post), Toast.LENGTH_SHORT).show();
    		}
    	}
    }
	
	@Override
	protected void onDestroy() {
//		Log.d(TAG, "Call onDestroy");
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
	
	private void getIntenter() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "getIntent 호출");
		}
		
		Intent intent = getIntent();
		mName = intent.getExtras().getString("FRIENDS_NAME");
		mLunarMonth = intent.getExtras().getInt("FRIENDS_LUNAR_MONTH");
		mLunarDay = intent.getExtras().getInt("FRIENDS_LUNAR_DAY");
		
//		Log.d(TAG, "getIntenter 로 받은 값 : " + mName + ", " + mLunarMonth + ", " + mLunarDay);
	}
}
