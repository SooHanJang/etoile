package madcat.studio.friends;

import java.util.ArrayList;

import madcat.studio.adapter.FriendsAdapter;
import madcat.studio.adapter.FriendsDelAdapter;
import madcat.studio.constant.Constants;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

public class FriendsList extends ListActivity {
	
//	private static final String TAG = "FriendsManage";
	private final int DYNAMIC_VIEW_ID = 0x80000;
	
	private static final int MODIFY_FRIENDS = 1;
	private static final int DEL_FRIENDS = 2;
	
	private ArrayList<Friends> mOrders;
	private FriendsAdapter mAdapter;
	private FriendsDelAdapter mDelAdapter;
	
	private int mCheckedGender, mCheckedSolarOrLunar;
	
	private int mFlag;
	
	private LinearLayout mDynamicLayout;
	private Button mDynamicDelOkButton;
	private Button mDynamicDelCancleButton;
	
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setting_friends_list);
		
		mOrders = new ArrayList<Friends>();
		
		mOrders = Util.getFriendsList();
		
		mFlag = 1;
		mContext = this;
		
//		Log.d(TAG, "처음 morders : " + mOrders.size());
		
		if(mOrders.size() == 0) {
			Toast.makeText(getApplicationContext(), getString(R.string.toast_click_menu_add_friends), Toast.LENGTH_SHORT).show();
		}
		
		mAdapter = new FriendsAdapter(getApplicationContext(), R.layout.setting_friends_row, mOrders, Constants.FLAG_FRIENDS_LIST_SEARCH);
		setListAdapter(mAdapter);
		
		registerForContextMenu(getListView());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		menu.setHeaderTitle(getString(R.string.dialog_title_management_friends));
		menu.add(0, MODIFY_FRIENDS, Menu.NONE, getString(R.string.menu_item_modify_friends));
		menu.add(0, DEL_FRIENDS, Menu.NONE, getString(R.string.menu_item_delete_friends));
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		boolean result = super.onContextItemSelected(item);
		
		AdapterView.AdapterContextMenuInfo menuInfo;
		int index;
		
		switch(item.getItemId()) {
		case MODIFY_FRIENDS:
			menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
			index = menuInfo.position;
			showModifyFriendsDialog(index);
			
//			Log.d(TAG, mAdapter.getItem(index).getName().toString() + " 이 클릭됨");
			return true;
			
		case DEL_FRIENDS:
			menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
			index = menuInfo.position;
			
//			Log.d(TAG, "현재 선택된 position : " + index);
			
			Util.deleteDbFriends(mAdapter.getItem(index).getName(),
					mAdapter.getItem(index).getGender(),
					mAdapter.getItem(index).getBirthType(),
					mAdapter.getItem(index).getSolarYear(),
					mAdapter.getItem(index).getSolarMonth(),
					mAdapter.getItem(index).getSolarDay());
			
			RefreshFriendsList();
			return true;
		}
		
		return false;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
//		Log.d(TAG, "onPrepare 호출");
		
		boolean result = super.onPrepareOptionsMenu(menu);
		
		if(mFlag == 1) {
			menu.removeItem(R.id.menu_add_friends);
			menu.removeItem(R.id.menu_del_friends);
			
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.friends_list_menu, menu);
		} else {
			result = false;
		}
		
		return result;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		
		switch(item.getItemId()) {
			case R.id.menu_add_friends:
				showAddFriendsDialog();
				break;
			case R.id.menu_del_friends:
//				Log.d(TAG, "DEL_Friends 호출");
				mFlag = 2;
				mDelAdapter = new FriendsDelAdapter(getApplicationContext(), R.layout.setting_friends_del_row, mOrders);
				setListAdapter(mDelAdapter);
				
				mDynamicLayout = (LinearLayout)findViewById(R.id.dynamic_del_area);
				mDynamicDelOkButton = new Button(this);
				mDynamicDelCancleButton = new Button(this);
				
				mDynamicDelOkButton.setId(DYNAMIC_VIEW_ID + 1);
				mDynamicDelOkButton.setText(getString(R.string.dialog_delete));
				mDynamicDelOkButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						if(mDelAdapter.getCheckedList().size() != 0) {
							for(int i=0; i < mDelAdapter.getCheckedList().size(); i++) {
//								Log.d(TAG, "선택된 것들 : " + mDelAdapter.getCheckedList().get(i).getName());
								Util.deleteDbFriends(mDelAdapter.getCheckedList().get(i).getName(),
										mDelAdapter.getCheckedList().get(i).getGender(),
										mDelAdapter.getCheckedList().get(i).getBirthType(),
										mDelAdapter.getCheckedList().get(i).getSolarYear(),
										mDelAdapter.getCheckedList().get(i).getSolarMonth(),
										mDelAdapter.getCheckedList().get(i).getSolarDay());
							}
						} else {
							Toast.makeText(getApplicationContext(), getString(R.string.toast_click_menu_del_friends), Toast.LENGTH_SHORT).show();
						}

						
						RefreshFriendsList();
						
						mDynamicLayout.removeView(mDynamicDelOkButton);
						mDynamicLayout.removeView(mDynamicDelCancleButton);
						
						mFlag = 1;
					}
				});
				
				mDynamicDelCancleButton.setId(DYNAMIC_VIEW_ID + 2);
				mDynamicDelCancleButton.setText(getString(R.string.dialog_cancel));
				mDynamicDelCancleButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						mDynamicLayout.removeView(mDynamicDelOkButton);
						mDynamicLayout.removeView(mDynamicDelCancleButton);
						setListAdapter(mAdapter);
						
						mFlag = 1;
					}
				});

				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
																				 ViewGroup.LayoutParams.FILL_PARENT,
																				 1.0F);
				
				mDynamicLayout.addView(mDynamicDelOkButton, params);
				mDynamicLayout.addView(mDynamicDelCancleButton, params);
				
				break;
			default:
				
				break;
		}
		
		return result;
	}
	
	private void RefreshFriendsList() {
		
// 이 주석 부분 삭제 금지
//		mOrders = Util.getFriendsList();
//		mAdapter.add(mOrders.get(mOrders.size()-1));
//		mAdapter.notifyDataSetChanged();
		
//		Log.d(TAG, "친구 어댑터 재호출");
		
		mAdapter.clear();
		mOrders = Util.getFriendsList();
		mAdapter = new FriendsAdapter(getApplicationContext(), R.layout.setting_friends_row, mOrders,  Constants.FLAG_FRIENDS_LIST_SEARCH);
		setListAdapter(mAdapter);
	}
	
	private void showModifyFriendsDialog(final int index) {
//		Log.d(TAG, "사용자 정보 변경");
		
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.setting_friends_input);
		
		final EditText input_name, input_year, input_month, input_day;
		final RadioGroup radioGroupGender, radioGroupSolarOrLunar;
		RadioButton maleRadioBtn, femaleRadioBtn, solarRadioBtn, lunarRadioBtn;
		ImageButton okImageBtn, cancleImageBtn;
		
		input_name = (EditText)dialog.findViewById(R.id.friends_input_name);
		input_year = (EditText)dialog.findViewById(R.id.friends_input_year);
		input_month = (EditText)dialog.findViewById(R.id.friends_input_month);
		input_day = (EditText)dialog.findViewById(R.id.friends_input_day);
		
		// 글꼴 설정
		SharedPreferences configPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, 0);
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			input_name.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_year.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_month.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_day.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
		} else {
			input_name.setTypeface(Typeface.DEFAULT);
			input_year.setTypeface(Typeface.DEFAULT);
			input_month.setTypeface(Typeface.DEFAULT);
			input_day.setTypeface(Typeface.DEFAULT);
		}
		
		radioGroupGender = (RadioGroup)dialog.findViewById(R.id.friends_radio_gender);
		radioGroupSolarOrLunar = (RadioGroup)dialog.findViewById(R.id.friends_radio_solar_or_lunar);
		
		maleRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_male);
		femaleRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_female);
		solarRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_solar);
		lunarRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_lunar);
		
		okImageBtn = (ImageButton)dialog.findViewById(R.id.friends_btn_ok);
		cancleImageBtn = (ImageButton)dialog.findViewById(R.id.friends_btn_cancle);
		
		input_name.setText(mAdapter.getItem(index).getName().toString());
		
		if(mAdapter.getItem(index).getGender() == 1) {
			maleRadioBtn.setChecked(true);
			femaleRadioBtn.setChecked(false);
			
			mCheckedGender = 1;
		} else if(mAdapter.getItem(index).getGender() == 2){
			maleRadioBtn.setChecked(false);
			femaleRadioBtn.setChecked(true);
			
			mCheckedGender = 2;
		}
		
		if(mAdapter.getItem(index).getBirthType() == 1) {		// 양력이면
			input_year.setText(String.valueOf(mAdapter.getItem(index).getSolarYear()));
			input_month.setText(String.valueOf(mAdapter.getItem(index).getSolarMonth()));
			input_day.setText(String.valueOf(mAdapter.getItem(index).getSolarDay()));
			
			solarRadioBtn.setChecked(true);
			lunarRadioBtn.setChecked(false);
			
			mCheckedSolarOrLunar = 1;
		} else if(mAdapter.getItem(index).getBirthType() == 2) {		// 음력이면
			input_year.setText(String.valueOf(mAdapter.getItem(index).getLunarYear()));
			input_month.setText(String.valueOf(mAdapter.getItem(index).getLunarMonth()));
			input_day.setText(String.valueOf(mAdapter.getItem(index).getLunarDay()));
			
			solarRadioBtn.setChecked(false);
			lunarRadioBtn.setChecked(true);
			
			mCheckedSolarOrLunar = 2;
		}
		
		radioGroupGender.setOnCheckedChangeListener(new OnCheckedChangeListener() {		// 성별 라디오 그룹
			public void onCheckedChanged(RadioGroup group, int checkId) {
				Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);
				switch(checkId) {
				case R.id.friends_radio_male :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_man), Toast.LENGTH_SHORT).show();
					mCheckedGender = 1;
					break;
				case R.id.friends_radio_female :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_woman), Toast.LENGTH_SHORT).show();
					mCheckedGender = 2;
					break;
				}
			}
		});
		
		radioGroupSolarOrLunar.setOnCheckedChangeListener(new OnCheckedChangeListener() {		// 양,음력 라디오 그룹
			public void onCheckedChanged(RadioGroup group, int checkId) {
				Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);
				switch(checkId) {
				case R.id.friends_radio_solar :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_solar), Toast.LENGTH_SHORT).show();
					mCheckedSolarOrLunar = 1;
					break;
				case R.id.friends_radio_lunar :
					mCheckedSolarOrLunar = 2;
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_lunar), Toast.LENGTH_SHORT).show();
					break;
				}
			}
		});
		
		okImageBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				int correctCode = Util.isCorrect(input_name.getText().toString(), input_year.getText().toString(), input_month.getText().toString(), input_day.getText().toString());
				
				switch(correctCode) {
				case 0:
					if(mCheckedSolarOrLunar == 1) {
						FortuneDate solarDate = new FortuneDate(Integer.parseInt(input_year.getText().toString()), Integer.parseInt(input_month.getText().toString()),
								Integer.parseInt(input_day.getText().toString()));
						FortuneDate lunarDate = Util.getLunarDate(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay());
						
						Util.updateDbFriends(mAdapter.getItem(index), input_name.getText().toString(), mCheckedGender, mCheckedSolarOrLunar, 
								solarDate.getYear(), solarDate.getMonth(), solarDate.getDay(), lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
					} else {
						if(!Util.emptyLunarDateCheck(Integer.parseInt(input_year.getText().toString()), 
								Integer.parseInt(input_month.getText().toString()), 
								Integer.parseInt(input_day.getText().toString()))) {
							Toast.makeText(mContext, getString(R.string.toast_caution_not_lunar_day), Toast.LENGTH_SHORT).show();
							break;
						}
						
						FortuneDate lunarDate = new FortuneDate(Integer.parseInt(input_year.getText().toString()), Integer.parseInt(input_month.getText().toString()),
								Integer.parseInt(input_day.getText().toString()));
						FortuneDate solarDate = Util.getLunarDate(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
						
						Util.updateDbFriends(mAdapter.getItem(index), input_name.getText().toString(), mCheckedGender, mCheckedSolarOrLunar, 
								solarDate.getYear(), solarDate.getMonth(), solarDate.getDay(), lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
					}
					
					Toast.makeText(mContext, getString(R.string.toast_succeed_change_friends_info), Toast.LENGTH_SHORT).show();
					RefreshFriendsList();
					dialog.dismiss();
					break;
				case 1:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_name), Toast.LENGTH_SHORT).show();
					break;
				case 2:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_year), Toast.LENGTH_SHORT).show();
					break;
				case 3:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_year), Toast.LENGTH_SHORT).show();
					break;
				case 4:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_month), Toast.LENGTH_SHORT).show();
					break;
				case 5:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_day), Toast.LENGTH_SHORT).show();
					break;
				case 6:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_month), Toast.LENGTH_SHORT).show();
					break;
				case 7:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_day), Toast.LENGTH_SHORT).show();
					break;
				}
			}
		});
		
		cancleImageBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				input_name.setText("");
				input_year.setText("");
				input_month.setText("");
				input_day.setText("");
				dialog.dismiss();
			}
		});
		
		dialog.show();
	}
		
	private void showAddFriendsDialog() {
//		Log.d(TAG, "사용자 이름 변경");
		
		final Dialog dialog = new Dialog(this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.setting_friends_input);
		
		final EditText input_name, input_year, input_month, input_day;
		final RadioGroup radioGroupGender, radioGroupSolarOrLunar;
		RadioButton maleRadioBtn, femaleRadioBtn, solarRadioBtn, lunarRadioBtn;
		ImageButton okImageBtn, cancleImageBtn;
		
		mCheckedGender = 1;
		mCheckedSolarOrLunar = 1;
		
		input_name = (EditText)dialog.findViewById(R.id.friends_input_name);
		input_year = (EditText)dialog.findViewById(R.id.friends_input_year);
		input_month = (EditText)dialog.findViewById(R.id.friends_input_month);
		input_day = (EditText)dialog.findViewById(R.id.friends_input_day);
		
		// 글꼴 설정
		SharedPreferences configPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, 0);
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			input_name.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_year.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_month.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			input_day.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
		} else {
			input_name.setTypeface(Typeface.DEFAULT);
			input_year.setTypeface(Typeface.DEFAULT);
			input_month.setTypeface(Typeface.DEFAULT);
			input_day.setTypeface(Typeface.DEFAULT);
		}
		
		radioGroupGender = (RadioGroup)dialog.findViewById(R.id.friends_radio_gender);
		radioGroupSolarOrLunar = (RadioGroup)dialog.findViewById(R.id.friends_radio_solar_or_lunar);
		
		maleRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_male);
		femaleRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_female);
		solarRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_solar);
		lunarRadioBtn = (RadioButton)dialog.findViewById(R.id.friends_radio_lunar);
		
		okImageBtn = (ImageButton)dialog.findViewById(R.id.friends_btn_ok);
		cancleImageBtn = (ImageButton)dialog.findViewById(R.id.friends_btn_cancle);
		
		radioGroupGender.setOnCheckedChangeListener(new OnCheckedChangeListener() {		// 성별 라디오 그룹
			public void onCheckedChanged(RadioGroup group, int checkId) {
				Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);
				switch(checkId) {
				case R.id.friends_radio_male :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_man), Toast.LENGTH_SHORT).show();
					mCheckedGender = 1;
					break;
				case R.id.friends_radio_female :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_woman), Toast.LENGTH_SHORT).show();
					mCheckedGender = 2;
					break;
				}
			}
		});
		
		radioGroupSolarOrLunar.setOnCheckedChangeListener(new OnCheckedChangeListener() {		// 양,음력 라디오 그룹
			public void onCheckedChanged(RadioGroup group, int checkId) {
				Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);
				switch(checkId) {
				case R.id.friends_radio_solar :
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_solar), Toast.LENGTH_SHORT).show();
					mCheckedSolarOrLunar = 1;
					break;
				case R.id.friends_radio_lunar :
					mCheckedSolarOrLunar = 2;
					Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_lunar), Toast.LENGTH_SHORT).show();
					break;
				}
			}
		});
		
		okImageBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				int correctCode = Util.isCorrect(input_name.getText().toString(), input_year.getText().toString(), input_month.getText().toString(), input_day.getText().toString());
				
				switch(correctCode) {
				case 0:
					if(mCheckedSolarOrLunar == 1) {
						FortuneDate solarDate = new FortuneDate(Integer.parseInt(input_year.getText().toString()), Integer.parseInt(input_month.getText().toString()),
								Integer.parseInt(input_day.getText().toString()));
						FortuneDate lunarDate = Util.getLunarDate(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay());
						
						Util.insertDbFriends(input_name.getText().toString(), mCheckedGender, mCheckedSolarOrLunar, 
								solarDate.getYear(), solarDate.getMonth(), solarDate.getDay(), lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
					} else {
						if(!Util.emptyLunarDateCheck(Integer.parseInt(input_year.getText().toString()), 
								Integer.parseInt(input_month.getText().toString()), 
								Integer.parseInt(input_day.getText().toString()))) {
							Toast.makeText(mContext, getString(R.string.toast_caution_not_lunar_day), Toast.LENGTH_SHORT).show();
							break;
						}
						
						FortuneDate lunarDate = new FortuneDate(Integer.parseInt(input_year.getText().toString()), Integer.parseInt(input_month.getText().toString()),
								Integer.parseInt(input_day.getText().toString()));
						FortuneDate solarDate = Util.getLunarDate(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
						
						Util.insertDbFriends(input_name.getText().toString(), mCheckedGender, mCheckedSolarOrLunar, 
								solarDate.getYear(), solarDate.getMonth(), solarDate.getDay(), lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
					}
					
					RefreshFriendsList();
					dialog.dismiss();
					break;
				case 1:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_name), Toast.LENGTH_SHORT).show();
					break;
				case 2:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_year), Toast.LENGTH_SHORT).show();
					break;
				case 3:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_year), Toast.LENGTH_SHORT).show();
					break;
				case 4:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_month), Toast.LENGTH_SHORT).show();
					break;
				case 5:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_day), Toast.LENGTH_SHORT).show();
					break;
				case 6:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_month), Toast.LENGTH_SHORT).show();
					break;
				case 7:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_day), Toast.LENGTH_SHORT).show();
					break;
				}
			}
		});
		
		cancleImageBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				input_name.setText("");
				input_year.setText("");
				input_month.setText("");
				input_day.setText("");
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		if(mFlag == 1) {
			super.onBackPressed();
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
		} else if(mFlag == 2) {
			mDynamicLayout.removeView(mDynamicDelOkButton);
			mDynamicLayout.removeView(mDynamicDelCancleButton);
			setListAdapter(mAdapter);
			RefreshFriendsList();
			
			mFlag = 1;
		}
	}
}
