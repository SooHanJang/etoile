package madcat.studio.friends;

public class Friends {

	private String mName, mPhoneNumber;
	private int mYear, mMonth, mDay, mBirthType, mGender;
	private int mSolarYear, mSolarMonth, mSolarDay, mLunarYear, mLunarMonth, mLunarDay;
	
	public Friends(String name, int gender, int birthType, int sy, int sm, int sd, int ly, int lm, int ld, String phoneNumber) {
		this.mName = name;
		this.mGender = gender;
		this.mBirthType = birthType;
		this.mSolarYear = sy;
		this.mSolarMonth = sm;
		this.mSolarDay = sd;
		this.mLunarYear = ly;
		this.mLunarMonth = lm;
		this.mLunarDay = ld;
		this.mPhoneNumber = phoneNumber;
	}

	public String getPhoneNumber() {
		return mPhoneNumber;
	}

	public String getName() {
		return mName;
	}

	public int getYear() {
		return mYear;
	}

	public int getMonth() {
		return mMonth;
	}

	public int getDay() {
		return mDay;
	}

	public int getBirthType() {
		return mBirthType;
	}

	public int getGender() {
		return mGender;
	}

	public int getSolarYear() {
		return mSolarYear;
	}

	public int getSolarMonth() {
		return mSolarMonth;
	}

	public int getSolarDay() {
		return mSolarDay;
	}

	public int getLunarYear() {
		return mLunarYear;
	}

	public int getLunarMonth() {
		return mLunarMonth;
	}

	public int getLunarDay() {
		return mLunarDay;
	}
	
	
}
