package madcat.studio.load;

import java.util.Locale;

import madcat.studio.constant.Constants;
import madcat.studio.main.MainMenu;
import madcat.studio.plus.R;
import madcat.studio.utils.FortuneDate;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * 
 * @author Dane 2011.05.26
 * 
 * 
 *
 */
public class LoadUserData extends Activity {
//	private static final String TAG = "LOAD USER DATA";
	
//	private ImageView mEggImageView;
	private EditText mNameEditText, mYearEditText, mMonthEditText, mDayEditText;
	private RadioGroup mRadioGroupGender, mRadioGroupSolarOrLunar;
	private RadioButton mMaleRadioBtn, mFemaleRadioBtn, mSolarRadioBtn, mLunarRadioBtn;
	private ImageButton mOkImageBtn, mCancleImageBtn;
	private Context mContext;
	private String mLocale;
	
	private MakeDatabaseTask mMakeDataBaseTask = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		mLocale = getResources().getConfiguration().locale.getLanguage();
		if(mLocale.equals(Locale.KOREAN.toString())) {
			setContentView(R.layout.user_birth_input);
		} else {
			setContentView(R.layout.user_birth_input_en);
		}
		
		mContext = this;
		
		mRadioGroupGender = (RadioGroup)findViewById(R.id.radio_gender);
		mMaleRadioBtn = (RadioButton)findViewById(R.id.radio_male);
		mFemaleRadioBtn = (RadioButton)findViewById(R.id.radio_female);
		
		mRadioGroupSolarOrLunar = (RadioGroup)findViewById(R.id.radio_solar_or_lunar);
		mSolarRadioBtn = (RadioButton)findViewById(R.id.radio_solar);
		mLunarRadioBtn = (RadioButton)findViewById(R.id.radio_lunar);
		
		mOkImageBtn = (ImageButton)findViewById(R.id.btn_ok);
		mCancleImageBtn = (ImageButton)findViewById(R.id.btn_cancle);
		
		mNameEditText = (EditText)findViewById(R.id.input_name);
		mYearEditText = (EditText)findViewById(R.id.input_year);
		mMonthEditText = (EditText)findViewById(R.id.input_month);
		mDayEditText = (EditText)findViewById(R.id.input_day);
		
		// 글꼴 설정
		SharedPreferences configPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, 0);
		String fontAssetUrl = Constants.FONT_PREFIX_PATH + 
				Util.getFontFileName(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0)) + Constants.FONT_SUFFIX_TTF;
		
		if(configPref.getInt(Constants.CONFIG_FONT_THEME_ITEM, 0) != 0) {
			mNameEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mYearEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mMonthEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
			mDayEditText.setTypeface(Typeface.createFromAsset(getAssets(), fontAssetUrl));
		} else {
			mNameEditText.setTypeface(Typeface.DEFAULT);
			mYearEditText.setTypeface(Typeface.DEFAULT);
			mMonthEditText.setTypeface(Typeface.DEFAULT);
			mDayEditText.setTypeface(Typeface.DEFAULT);
		}
		
		//Radio Button Event Handling
		mRadioGroupGender.setOnCheckedChangeListener(new RadioGroupEventHandler());
		mRadioGroupSolarOrLunar.setOnCheckedChangeListener(new RadioGroupEventHandler());
		
		//Image Button Event Handling
		mOkImageBtn.setOnClickListener(new OkButtonEventHandler());
		mCancleImageBtn.setOnClickListener(new CancleButtonEventHandler());
		
		mNameEditText.requestFocus();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mMakeDataBaseTask = new MakeDatabaseTask();
		mMakeDataBaseTask.execute();
	}
	
	private class RadioGroupEventHandler implements RadioGroup.OnCheckedChangeListener {
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			Util.getVibratorService(getApplicationContext(), Constants.VIBRATE_MILLSECONDS);
			switch(checkedId) {
			case R.id.radio_male:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_man), Toast.LENGTH_SHORT).show();
				mFemaleRadioBtn.setChecked(false);
				mMaleRadioBtn.setChecked(true);
				break;
			case R.id.radio_female:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_woman), Toast.LENGTH_SHORT).show();
				mMaleRadioBtn.setChecked(false);
				mFemaleRadioBtn.setChecked(true);
				break;
			case R.id.radio_solar:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_solar), Toast.LENGTH_SHORT).show();
				mSolarRadioBtn.setChecked(true);
				mLunarRadioBtn.setChecked(false);
				break;
			case R.id.radio_lunar:
				Toast.makeText(getApplicationContext(), getString(R.string.toast_choice_lunar), Toast.LENGTH_SHORT).show();
				mSolarRadioBtn.setChecked(false);
				mLunarRadioBtn.setChecked(true);
				break;
			}
		}
	}
	
	private class OkButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			SharedPreferences pref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
			SharedPreferences.Editor editor = pref.edit();
			
			int correctCode = Util.isCorrect(mNameEditText.getText().toString(), mYearEditText.getText().toString(), 
					mMonthEditText.getText().toString(), mDayEditText.getText().toString());
			
			FortuneDate solarDate, lunarDate;
			
//			Log.d(TAG, "음력 체크 : " + mLunarRadioBtn.isChecked());
//			Log.d(TAG, "양력 체크 : " + mSolarRadioBtn.isChecked());

			switch(correctCode) {
				case 0:
					if(mLunarRadioBtn.isChecked()) {
						if(!Util.emptyLunarDateCheck(Integer.parseInt(mYearEditText.getText().toString()), 
								Integer.parseInt(mMonthEditText.getText().toString()), 
								Integer.parseInt(mDayEditText.getText().toString()))) {
							Toast.makeText(mContext, getString(R.string.toast_caution_not_lunar_day), Toast.LENGTH_SHORT).show();
							break;
						}
					}
					editor.putString(Constants.USER_NAME, mNameEditText.getText().toString());
					
					if(mRadioGroupGender.getCheckedRadioButtonId() == R.id.radio_male) {
						editor.putInt(Constants.USER_GENDER, 1);
					} else {
						editor.putInt(Constants.USER_GENDER, 2);
					}
					
					if(mRadioGroupSolarOrLunar.getCheckedRadioButtonId() == R.id.radio_solar) {
	//					Log.d(TAG, "양력 선택");
						solarDate = new FortuneDate(Integer.parseInt(mYearEditText.getText().toString()), Integer.parseInt(mMonthEditText.getText().toString()), Integer.parseInt(mDayEditText.getText().toString()));
						lunarDate = Util.getLunarDate(solarDate.getYear(), solarDate.getMonth(), solarDate.getDay());
						
	//					Log.d(TAG, solarDate.getYear() + "/" + solarDate.getMonth() + "/" + solarDate.getDay());
	//					Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
						
						editor.putInt(Constants.USER_BIRTH_TYPE, 1);
					} else {
	//					Log.d(TAG, "음력 선택");
						lunarDate = new FortuneDate(Integer.parseInt(mYearEditText.getText().toString()), Integer.parseInt(mMonthEditText.getText().toString()), Integer.parseInt(mDayEditText.getText().toString()));
						solarDate = Util.getSolarDate(lunarDate.getYear(), lunarDate.getMonth(), lunarDate.getDay());
						
	//					Log.d(TAG, solarDate.getYear() + "/" + solarDate.getMonth() + "/" + solarDate.getDay());
	//					Log.d(TAG, lunarDate.getYear() + "/" + lunarDate.getMonth() + "/" + lunarDate.getDay());
						
						editor.putInt(Constants.USER_BIRTH_TYPE, 2);
					}
					
					editor.putInt(Constants.USER_SOLAR_YEAR, solarDate.getYear());
					editor.putInt(Constants.USER_SOLAR_MONTH, solarDate.getMonth());
					editor.putInt(Constants.USER_SOLAR_DAY, solarDate.getDay());
					
					editor.putInt(Constants.USER_LUNAR_YEAR, lunarDate.getYear());
					editor.putInt(Constants.USER_LUNAR_MONTH, lunarDate.getMonth());
					editor.putInt(Constants.USER_LUNAR_DAY, lunarDate.getDay());
					
					editor.putInt(Constants.USER_WEST_STAR_POSITION, Util.getWestStarPosition(solarDate.getMonth(), solarDate.getDay()));
					editor.putInt(Constants.USER_EAST_STAR_POSITION, Util.getEastStarPosition(lunarDate.getMonth(), lunarDate.getDay()));
					
					editor.commit();
//					Log.d(TAG, "사용자 자료 commit 완료");
					
	//				Intent intent = new Intent(LoadUserData.this, LoadCharacter.class);
					Intent intent = new Intent(LoadUserData.this, MainMenu.class);
					startActivity(intent);
					overridePendingTransition(R.anim.fade, R.anim.hold);
					finish();
					break;
				case 1:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_name), Toast.LENGTH_SHORT).show();
					break;
				case 2:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_year), Toast.LENGTH_SHORT).show();
					break;
				case 3:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_year), Toast.LENGTH_SHORT).show();
					break;
				case 4:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_month), Toast.LENGTH_SHORT).show();
					break;
				case 5:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_day), Toast.LENGTH_SHORT).show();
					break;
				case 6:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_month), Toast.LENGTH_SHORT).show();
					break;
				case 7:
					Toast.makeText(getApplicationContext(), getString(R.string.toast_input_correct_day), Toast.LENGTH_SHORT).show();
					break;
			}
		}
	}
	
	private class CancleButtonEventHandler implements View.OnClickListener {
		public void onClick(View v) {
			overridePendingTransition(R.anim.fade, R.anim.fade_out);
			finish();
		}
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.fade, R.anim.fade_out);
	}
	
	private class MakeDatabaseTask extends AsyncTask<Void, Void, Void> {
		ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			progressDialog = ProgressDialog.show(mContext, "", getString(R.string.progress_update_app));
		}
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
//			Log.d(TAG, "DB 생성 시작 ");
			if (LoadDatabase.loadDataBase(getResources().getAssets())) {
				dbUpdateCheck();
//				Log.d(TAG, "DB 생성 성공");
			} else {
//				Log.d(TAG, "DB 생성 실패");
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(progressDialog.isShowing()) {
				progressDialog.dismiss();
			}
		}
	}
	
	private void dbUpdateCheck() {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String updateCheck = "select * from " + Constants.TABLE_CALENDAR + 
		   " where " + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + 1900 +
		   " and " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + 1 +
		   " and " + Constants.ATTRIBUTE_SOLAR_DAY + "=" + 1;
		
		Cursor cursor = sdb.rawQuery(updateCheck, null);

		if(cursor.getCount() == 0) {
			ContentValues values = new ContentValues();
			
			values.put(Constants.ATTRIBUTE_SOLAR_YEAR, 1900);
			values.put(Constants.ATTRIBUTE_SOLAR_MONTH, 1);
			values.put(Constants.ATTRIBUTE_SOLAR_DAY, 1);
			values.put(Constants.ATTRIBUTE_LUNAR_YEAR, 1899);
			values.put(Constants.ATTRIBUTE_LUNAR_MONTH, 12);
			values.put(Constants.ATTRIBUTE_LUNAR_DAY, 1);
			
			sdb.insert(Constants.TABLE_CALENDAR, null, values);
			
//			Log.d(TAG, "update 성공");
		} else {
//			Log.d(TAG, "이미 update 가 되어있습니다.");
		}
		
		cursor.close();
		sdb.close();
			
	}
}
