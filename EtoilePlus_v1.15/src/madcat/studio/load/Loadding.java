package madcat.studio.load;

import madcat.studio.constant.Constants;
import madcat.studio.main.MainMenu;
import madcat.studio.plus.R;
import madcat.studio.utils.Util;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

/**
 * 
 * @author Dane 2011.05.26
 * 
 * SharedPreference 참조 후 값이 있으면 MainMenu를 호출하고,
 * SharedPreference에 값이 존재하지 않으면 사용자 정보를 생성하기 위한 과정을 진행합니다.
 * 
 */
public class Loadding extends Activity {
	private final String TAG											=	"LOADDING";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;

	private ImageView mLogoImage										=	null;
	private ImageView mShootingStar1									=	null;
	private ImageView mShootingStar2									=	null;
	private ImageView mShootingStar3									=	null;
	private ImageView mShootingStar4									=	null;
	private ImageView mShootingStar5 									=	null;
	private ImageView mShootingStar6 									=	null;
	
	private LoaddingTask mLoaddingTask 									=	null;
	
	private boolean mFirstFlag 											=	false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	
		SharedPreferences pref = getSharedPreferences(Constants.USER_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		SharedPreferences configPref = getSharedPreferences(Constants.CONFIG_PREFERENCE_NAME, Activity.MODE_PRIVATE);
		
		if (pref.getString(Constants.USER_NAME, null) == null) {
//			Log.d(TAG, "사용자 정보가 존재하지 않습니다.");
			mFirstFlag = true;
			
			SharedPreferences.Editor editor = configPref.edit();
			if(getResources().getConfiguration().locale.getLanguage().equals("ko")) {
				editor.putInt(Constants.CONFIG_SELECT_LANGUAGE, 0);
			} else {
				editor.putInt(Constants.CONFIG_SELECT_LANGUAGE, 1);
			}
			
			editor.commit();
			
		} else {
//			Log.d(TAG, "사용자 정보가 존재합니다.");
			mFirstFlag = false;
			
			String currentLanguage = null;
			
			switch(configPref.getInt(Constants.CONFIG_SELECT_LANGUAGE, 0)) {
				case 0:
					currentLanguage = "ko";
					break;
				case 1:
					currentLanguage = "en";
					break;
			}
			
//			Log.d("운세", "currentLanguage : " + currentLanguage);
			
			if(!getResources().getConfiguration().locale.getLanguage().equals(currentLanguage)) {
				Util.setLocale(this, currentLanguage);
			} else {
//				Log.d("운세", "설정과 시스템 언어 값이 같다.");
			}
		}

		// 오늘 날짜를 검사하고, 다르면 저장한다.
		String todayDate = Util.getYear() + Util.getMonthOfYear() + Util.getTodayOfMonth();
		
		if(!configPref.getString(Constants.CONFIG_TODAY_DATE, "").equals(todayDate)) {	
			SharedPreferences.Editor configEditor = configPref.edit();
			configEditor.putString(Constants.CONFIG_TODAY_DATE, todayDate);
			configEditor.putBoolean(Constants.CONFIG_CHECK_FORTUNE_STAR, false);
			configEditor.commit();
		}
		
		setContentView(R.layout.loading);
		
		mLogoImage = (ImageView)findViewById(R.id.loaddingLogoImageView);
		mShootingStar1 = (ImageView)findViewById(R.id.loaddingShootingStar1);
		mShootingStar2 = (ImageView)findViewById(R.id.loaddingShootingStar2);
		mShootingStar3 = (ImageView)findViewById(R.id.loaddingShootingStar3);
		mShootingStar4 = (ImageView)findViewById(R.id.loaddingShootingStar4);
		mShootingStar5 = (ImageView)findViewById(R.id.loaddingShootingStar5);
		mShootingStar6 = (ImageView)findViewById(R.id.loaddingShootingStar6);
		
		mLogoImage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mLoaddingTask != null) {
					mLoaddingTask.cancel(true);
					if (mFirstFlag) {
						Intent intent = new Intent(Loadding.this, LoadUserData.class);
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
					} else {
						Intent intent = new Intent(Loadding.this, MainMenu.class);
						startActivity(intent);
						overridePendingTransition(R.anim.fade, R.anim.hold);
					}
					finish();
				}
			}
		});
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		mLogoImage.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_logo));
		mShootingStar1.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star1));
		mShootingStar2.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star2));
		mShootingStar3.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star3));
		mShootingStar4.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star4));
		mShootingStar5.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star5));
		mShootingStar6.startAnimation(AnimationUtils.loadAnimation(this, R.anim.loadding_shooting_star6));
		
		mLoaddingTask = new LoaddingTask();
		mLoaddingTask.execute();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
//		Log.d(TAG, "onStop 호출");
		super.onStop();
		mLoaddingTask.cancel(true);
		finish();
	}
	
	private class LoaddingTask extends AsyncTask<Void, Void, Void> {
		private static final int LOADDING_TIME = 3000;
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try {
				if(!mFirstFlag) {
					dbUpdateCheck();
				}
				Thread.sleep(LOADDING_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if (mFirstFlag) {
				Intent intent = new Intent(Loadding.this, LoadUserData.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			} else {
				Intent intent = new Intent(Loadding.this, MainMenu.class);
				startActivity(intent);
				overridePendingTransition(R.anim.fade, R.anim.hold);
			}
			finish();
		}
	}
	
	private void dbUpdateCheck() {
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(Constants.ROOT_DIR + Constants.FILE_NAME, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String updateCheck = "select * from " + Constants.TABLE_CALENDAR + 
		   " where " + Constants.ATTRIBUTE_SOLAR_YEAR + "=" + 1900 +
		   " and " + Constants.ATTRIBUTE_SOLAR_MONTH + "=" + 1 +
		   " and " + Constants.ATTRIBUTE_SOLAR_DAY + "=" + 1;
		
		Cursor cursor = sdb.rawQuery(updateCheck, null);

		if(cursor.getCount() == 0) {
			ContentValues values = new ContentValues();
			
			values.put(Constants.ATTRIBUTE_SOLAR_YEAR, 1900);
			values.put(Constants.ATTRIBUTE_SOLAR_MONTH, 1);
			values.put(Constants.ATTRIBUTE_SOLAR_DAY, 1);
			values.put(Constants.ATTRIBUTE_LUNAR_YEAR, 1899);
			values.put(Constants.ATTRIBUTE_LUNAR_MONTH, 12);
			values.put(Constants.ATTRIBUTE_LUNAR_DAY, 1);
			
			sdb.insert(Constants.TABLE_CALENDAR, null, values);
			
//			Log.d(TAG, "update 성공");
		} else {
//			Log.d(TAG, "이미 update 가 되어있습니다.");
		}
		
		cursor.close();
		sdb.close();
			
	}
	
	@Override
	protected void onDestroy() {
		Util.recursiveRecycle(getWindow().getDecorView());
		System.gc();
		
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {}
}
