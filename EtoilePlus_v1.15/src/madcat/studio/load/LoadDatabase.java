package madcat.studio.load;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import madcat.studio.constant.Constants;
import android.content.res.AssetManager;

/**
 * 
 * @author Dane 2011.05.26
 * 
 * 
 *
 */
public class LoadDatabase {
//	private static final String TAG = "LOAD DATABASE";
	
	public static boolean loadDataBase(AssetManager assetManager) {
		boolean loadFlag = true;
		
		InputStream[] arrIs = new InputStream[2];
		BufferedInputStream[] arrBis = new BufferedInputStream[2];
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		
		try {
			File location = new File(Constants.ROOT_DIR);
			location.mkdirs();
			File f = new File(Constants.ROOT_DIR + Constants.FILE_NAME);
			if(f.exists()) {
				f.delete();
				f.createNewFile();
			}
			
//			Log.d(TAG, Constants.ROOT_DIR + Constants.FILE_NAME);
			
			for(int i=0; i < arrIs.length; i++) {
				arrIs[i] = assetManager.open("calDB" + (i+1) + ".db");
				arrBis[i] = new BufferedInputStream(arrIs[i]);
			}

			fos = new FileOutputStream(f);
			bos = new BufferedOutputStream(fos);
			
			int read = -1;
			byte[] buffer = new byte[1024];
			
			for(int i=0; i < arrIs.length; i++) {
				while((read = arrBis[i].read(buffer, 0, 1024)) != -1) {
					bos.write(buffer, 0, read);
				}
				bos.flush();
			}
		} catch (Exception e) {
			loadFlag = false;
		} finally {
			for(int i=0; i < arrIs.length; i++) {
				try {
					if(arrIs[i] != null) { 
						arrIs[i].close();
					}
				} catch(Exception e) {
				} try {
					if(arrBis[i] != null) {
						arrBis[i].close();
					}
				} catch(Exception e) {
				}
			}
			try {
				if(fos != null) {
					fos.close();
				}
			} catch(Exception e) {}
			try {
				if(bos != null) {
					bos.close();
				}
			} catch(Exception e) {}
			
			arrIs = null;
			arrBis = null;
		}
		
		return loadFlag;
    }
}
